package org.mathpiper.ui.gui.worksheets;

import java.awt.Color;
import org.mathpiper.ui.gui.worksheets.symbolboxes.ScaledGraphics;

public class GraphicObject
{

    private String label;
    private boolean isShowLabel = true;
    private double xCoordinate;
    private double yCoordinate;
    private double diameter = 6.0;
    private double highlightDiameter = diameter * 1.6;
    private Color highlightColor = new Color(243, 128, 43);
    
    private boolean isSelected = false;
    
    
    private Color color = Color.BLUE;

    public GraphicObject(String label, double x, double y)
    {
        this.label = label;
        this.xCoordinate = x;
        this.yCoordinate = y;
    }
    
    public void paint(ScaledGraphics sg, PlotPanel plotPanel)
    {
        double xPixels = plotPanel.toPixelsX(xCoordinate);
        double yPixels = plotPanel.toPixelsY(yCoordinate);
        
        if(isSelected)
        {
          sg.setColor(highlightColor);
          sg.fillOval(xPixels-highlightDiameter/2.0, yPixels-highlightDiameter/2.0, highlightDiameter, highlightDiameter);  
        }    
        
        
        sg.setColor(color);
        sg.fillOval(xPixels-diameter/2.0, yPixels-diameter/2.0, diameter, diameter);
        
        
        if(isShowLabel)
        {
            sg.drawScaledString(label, xPixels + 3, yPixels - 5, 1.5);
        }
    }

    public String getLabel()
    {
        return label;
    }
    
    public String setLabel(String label)
    {
        return this.label = label;
    }

    public double getXCoordinate()
    {
        return xCoordinate;
    }

    public double getYCoordinate()
    {
        return yCoordinate;
    }
    
    public void select(boolean isSelected)
    {
        this.isSelected = isSelected;
    }
    
    public void toggleSelected()
    {
        if(isSelected)
        {
            isSelected = false;
        }
        else
        {
            isSelected = true;
        }
    }

    public void setXCoordinate(double xCoordinate)
    {
        this.xCoordinate = xCoordinate;
    }

    public void setYCoordinate(double yCoordinate)
    {
        this.yCoordinate = yCoordinate;
    }
    
    public void move(double xDifference, double yDifference)
    {
        xCoordinate += xDifference;
        yCoordinate += yDifference;
    }
    
    public void snapToInteger()
    {
        xCoordinate = Math.round(xCoordinate);
        yCoordinate = Math.round(yCoordinate);
    }

    public double getDiameter()
    {
        return diameter;
    }

    public void setDiameter(double diameter)
    {
        this.diameter = diameter;
    }

    public Color getColor()
    {
        return color;
    }

    public void setColor(Color color)
    {
        this.color = color;
    }
    
    public boolean isSelected()
    {
        return this.isSelected;
    }

    public boolean isShowLabel()
    {
        return isShowLabel;
    }

    public void showLabel(boolean isShowLabel)
    {
        this.isShowLabel = isShowLabel;
    }
    
    
    
}
