package org.mathpiper.ui.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.util.Map;
import javax.swing.BorderFactory;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import org.mathpiper.interpreters.Interpreter;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;
import org.scilab.forge.mp.jlatexmath.TeXConstants;
import org.scilab.forge.mp.jlatexmath.TeXFormula;

public class RulesPanel extends JPanel {

    private Interpreter interpreter;
    private Environment environment;
    private JTextArea textArea = new JTextArea();
    private JTable table;
    private JScrollPane scrollPane;
    private String column1Name = "Name";
    private String column2Name = "Rule";
    private TableColumnAdjuster tableColumnAdjuster;
    private JPanel buttonsBox = new JPanel(new VerticalLayout(0, VerticalLayout.LEFT, VerticalLayout.TOP));
    private JPanel buttonsPanel1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    private JPanel buttonsPanel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));


    public RulesPanel(Environment aEnvironment, Map userOptions) throws Throwable {
        // todo:tk:consider using GroupLayout.
        buttonsPanel1.setBorder(javax.swing.BorderFactory.createEmptyBorder(-1,-1,-1,-1));
        buttonsPanel2.setBorder(javax.swing.BorderFactory.createEmptyBorder(-1,-1,-1,-1));
       
        this.setLayout(new BorderLayout());

        table = makeTable(aEnvironment, userOptions);

        scrollPane = new JScrollPane(new JPanel().add(table), JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.getVerticalScrollBar().setUnitIncrement(16);

        this.add(scrollPane);

        table.setPreferredScrollableViewportSize(table.getPreferredSize());
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        add(scrollPane);
        
        JPanel zoomButtonsPanel = new JPanel();
        zoomButtonsPanel.setBorder(BorderFactory.createTitledBorder("Rules Zoom"));
       
        buttonsPanel2.add(zoomButtonsPanel);

        JButton decrease = new JButton("-");
        zoomButtonsPanel.add(decrease);
        decrease.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                zoomFont(.90);
            }
        });
        
        zoomButtonsPanel.add(decrease);

        JButton increase = new JButton("+");
        zoomButtonsPanel.add(increase);
        increase.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                zoomFont(1.10);
            }
        });
        
        zoomButtonsPanel.add(increase);
        
        buttonsBox.add(buttonsPanel1);
        buttonsBox.add(buttonsPanel2);
        add(buttonsBox, BorderLayout.NORTH);

        
        table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        tableColumnAdjuster = new TableColumnAdjuster(table);
        tableColumnAdjuster.adjustColumns();
        
        table.setRowHeight(25);
    }

    public int getSelectedRowCount() {
        return this.table.getSelectedRowCount();
    }

    public int getSelectedRow() {
        return this.table.getSelectedRow();
    }
    
    public void selectRow(int row) {
        this.table.setRowSelectionInterval(row-1, row-1);
    }
    
    public void addButtonRow1(Component component, int index)
    {
        this.buttonsPanel1.add(component, index);
    }
    
    public void addButtonRow2(Component component, int index)
    {
        this.buttonsPanel2.add(component, index);
    }

    private JTable makeTable(Environment environment, Map m) throws Throwable {
        Cons cons = (Cons) m.get("RewriteRulesTable");
        
        if(cons == null)
        {
            throw new Exception("No rewrite rules table was submitted.");
        }

        final JTable table = new JTable(new RulesTableModel());
        
        DefaultTableCellRenderer iconRenderer = new IconCellRenderer();
        table.getColumnModel().getColumn(2).setCellRenderer(iconRenderer);

        table.setFillsViewportHeight(true);
        
        // table.setBorder(new EmptyBorder(new Insets(1,4,1,4)));

        RulesTableModel model = (RulesTableModel) table.getModel();

        Cons cons2 = null;

        try {
            int length = Utility.listLength(environment, -1, (Cons) ((Cons) cons.car()).cdr());

            for (int index = 1; index <= length; index++) {
                cons2 = Utility.nth(environment, -1, cons, index, true);

                Cons ruleName = Utility.nth(environment, -1, cons2, 1, true);
                String ruleNameString = Utility.toNormalString(environment, -1, ruleName.toString());
                Cons pattern = Utility.nth(environment, -1, cons2, 2, true);
                Cons patternTex = Utility.nth(environment, -1, cons2, 3, true);
                String patternTexString = Utility.toNormalString(environment, -1, patternTex.toString());
                Cons replacement = Utility.nth(environment, -1, cons2, 4, true);
                replacement = ((Cons)replacement.car()).cdr();
                Cons replacementTex = Utility.nth(environment, -1, cons2, 5, true);
                String replacementTexString = Utility.toNormalString(environment, -1, replacementTex.toString());

                model.addRow(new Object[]{" " + index, " " + ruleNameString, new LatexIcon(patternTexString, replacementTexString, 18)});
            }

        } catch (Throwable t) {
            t.printStackTrace();
        }

        /*
         TeXFormula texFormula = new TeXFormula("a\\_ + b\\_ \\leftarrow b + a");
         Icon rule = texFormula.createTeXIcon(TeXConstants.STYLE_DISPLAY, (float) 12);
         model.addRow(new Object[]{"Commutative +", rule});
         */
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    int index = table.getSelectedRow();
                }
            }
        });

        return table;
    }
    
    
    public class IconCellRenderer extends DefaultTableCellRenderer {
 
        public IconCellRenderer() {

            this.setHorizontalAlignment(JLabel.LEFT);
            this.setText(null);
        }

        @Override
        protected void setValue(Object value) {

            this.setIcon((Icon) value);
        }
    }

    public class RulesTableModel extends DefaultTableModel {

        public RulesTableModel() {
            addColumn("#");
            addColumn(column1Name);
            addColumn(column2Name);
        }

        
        @Override
        public Class<?> getColumnClass(int columnIndex) {

            Class clazz = String.class;

            /*
            switch (columnIndex) {

                case 2:
                    clazz = Icon.class;
                    break;

            }
            */

            return clazz;

        }

    }

    private class LatexIcon implements Icon {

        private TeXFormula texFormula;
        private Icon icon;
        private double size = 14;

        public LatexIcon(String patternTexString, String replacementTexString, double size) {
                texFormula = new TeXFormula("\\ " + patternTexString + " \\ \\ \\boldsymbol{\\textcolor{RoyalBlue}{\\leftarrow}} \\ \\ " + replacementTexString);
                icon = texFormula.createTeXIcon(TeXConstants.STYLE_DISPLAY, (float) size);
                this.size = size;
        }
        
        public void changeSize(double scale)
        {
            size = size * scale;
            icon = texFormula.createTeXIcon(TeXConstants.STYLE_DISPLAY, (float) size);
            repaint();
        }
        

        public int getIconHeight() {
            return icon.getIconHeight();
        }

        public int getIconWidth() {
            return icon.getIconWidth();
        }

        public void paintIcon(Component c, Graphics g, int x, int y) {
            icon.paintIcon(c, g, x, y);
        }

    }
    
    private void zoomFont(double scale)
    {
        Font font = table.getFont();
        font = font.deriveFont((float) (font.getSize2D() * scale));
        table.setFont(font);
        FontMetrics fontMetrics = table.getGraphics().getFontMetrics(font);
        Rectangle2D rectangle = fontMetrics.getStringBounds("H", table.getGraphics());
        table.setRowHeight((int) Math.round(rectangle.getHeight()));

        JTableHeader tableHeader = table.getTableHeader();
        font = tableHeader.getFont();
        font = font.deriveFont((float) (font.getSize2D() * scale));
        tableHeader.setFont(font);

        int rowCount = table.getRowCount();
        TableModel model = table.getModel();
        LatexIcon latexIcon = null;
        for(int index = 0; index < rowCount; index++)
        {
           latexIcon = (LatexIcon) model.getValueAt(index,2);
           latexIcon.changeSize(scale);
        }

        RulesPanel.this.tableColumnAdjuster.adjustColumns();

        if(latexIcon != null)
        {
            table.setRowHeight(latexIcon.getIconHeight() + 3);
            table.repaint();
        }
            
    }
    
    public void clearSelection()
    {
        table.clearSelection();
    }
    
    
    public void addListSelectionListener(ListSelectionListener listener) {
        ListSelectionModel selectionModel = table.getSelectionModel();
        selectionModel.addListSelectionListener(listener);
    }

    public void removeListSelection(ListSelectionListener listener) {
        ListSelectionModel selectionModel = table.getSelectionModel();
    }   

}
