package org.mathpiper.ui.gui.applications.voscilloscope.oscilloscope;

import org.mathpiper.ui.gui.applications.voscilloscope.simulator.Tokenizer;
import java.awt.Button;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.Label;
import java.awt.List;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StreamTokenizer;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * <p>Title: Virtual Oscilloscope.</p>
 * <p>Description: A Oscilloscope simulator</p>
 * <p>Copyright (C) 2003 José Manuel Gómez Soriano</p>
 * <h2>License</h2>
 * <p>
 This file is part of Virtual Oscilloscope.

 Virtual Oscilloscope is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Virtual Oscilloscope is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Virtual Oscilloscope; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 * </p>
 */

// Referenced classes of package es.upv.scope:
//            Preview, Signal

public class FileDialog extends Dialog
{
    class FileDialogMouseAdapter extends MouseAdapter
    {

        public void mouseClicked(MouseEvent e)
        {
            if(e.getSource() == btnAccept)
            {
                if(listFiles.getSelectedIndex() == -1)
                {
                    accept = false;
                } else
                {
                    accept = true;
                }
                prevScreen.stop();
                setVisible(false);
            }
            if(e.getSource() == btnCancelar)
            {
                accept = false;
                prevScreen.stop();
                setVisible(false);
            }
            if(e.getSource() == listFiles)
            {
                try
                {
                    if(listFiles.getSelectedIndex() != -1)
                    {
                        Signal sig = new Signal((new URL(dir + listFiles.getSelectedItem())).openStream());
                        prevScreen.setSignal(sig);
                    }
                }
                catch(MalformedURLException mue)
                {
                    System.out.println("File no encontrado.\n" + mue);
                }
                catch(IOException ioe)
                {
                    System.out.println("Error al abrir el archivo.\n" + ioe);
                }
            }
        }

        FileDialogMouseAdapter()
        {
        }
    }

    class FileDialogWindowAdapter extends WindowAdapter
    {

        public void windowClosing(WindowEvent e)
        {
            prevScreen.stop();
            accept = false;
            setVisible(false);
        }

        FileDialogWindowAdapter()
        {
        }
    }


    private Button btnAccept;
    private Button btnCancelar;
    private List listFiles;
    private static Frame fFrame;
    private boolean accept;
    private URL dir;
    private Preview prevScreen;
    private TextField txtTitulo;
    private TextArea txtDesc;
    private Label lFiles;
    private Label lTitulo;
    private Label lDesc;

    public FileDialog(Frame parent, URL direc)
    {
        super(parent, "Acquire a signal from a file", false);
        dir = direc;
        btnAccept = new Button("Accept");
        btnCancelar = new Button("Cancel");
        listFiles = new List(2, false);
        prevScreen = new Preview();
        txtTitulo = new TextField("No title");
        txtDesc = new TextArea("No description");
        lFiles = new Label("Signals:");
        lTitulo = new Label("Title:");
        lDesc = new Label("Description:");
        add(btnAccept);
        add(btnCancelar);
        add(listFiles);
        add(prevScreen);
        add(txtTitulo);
        add(txtDesc);
        add(lFiles);
        add(lTitulo);
        add(lDesc);
        FileDialogMouseAdapter mouseAdapter = new FileDialogMouseAdapter();
        btnAccept.addMouseListener(mouseAdapter);
        btnCancelar.addMouseListener(mouseAdapter);
        listFiles.addMouseListener(mouseAdapter);
        addWindowListener(new FileDialogWindowAdapter());
    }

    public void addNotify()
    {
        super.addNotify();
        int margenlat = getInsets().left;
        int margensup = getInsets().top;
        setSize(489 + margenlat + getInsets().right, 297 + margensup + getInsets().bottom);
        setLayout(null);
        Dimension dim = getToolkit().getScreenSize();
        setLocation(dim.width / 2 - getSize().width / 2, dim.height / 2 - getSize().height / 2);
        prevScreen.setLocation(312 + margenlat, 24 + margensup);
        btnAccept.setBounds(312 + margenlat, 248 + margensup, 81, 41);
        btnCancelar.setBounds(403 + margenlat, 248 + margensup, 81, 41);
        listFiles.setBounds(5 + margenlat, 24 + margensup, 297, 113);
        txtTitulo.setBounds(8 + margenlat, 160 + margensup, 297, 21);
        txtDesc.setBounds(8 + margenlat, 200 + margensup, 297, 89);
        lFiles.setLocation(8 + margenlat, 4 + margensup);
        lFiles.setSize(lFiles.getPreferredSize());
        lTitulo.setLocation(8 + margenlat, 140 + margensup);
        lTitulo.setSize(lTitulo.getPreferredSize());
        lDesc.setLocation(8 + margenlat, 180 + margensup);
        lDesc.setSize(lDesc.getPreferredSize());
        listDirectory();
        prevScreen.start();
    }

    public void listDirectory()
    {
        try
        {
            Tokenizer dir = new Tokenizer((new URL(this.dir + "directory.txt")).openStream());
            listFiles.removeAll();
            do
            {
                if(dir.buscarFile(".sig") != null)
                {
                    listFiles.add(((StreamTokenizer) (dir)).sval);
                }
            } while(((StreamTokenizer) (dir)).ttype != -1);
        }
        catch(IOException e)
        {
            System.out.println("Cannot open the file");
        }
    }

    public synchronized boolean showModal()
    {
        accept = false;
        setModal(true);
        setVisible(true);
        return accept;
    }

    public static synchronized FileDialog makeDialog(URL dir)
    {
        if(fFrame == null)
        {
            fFrame = new Frame();
        }
        FileDialog result = new FileDialog(fFrame, dir);
        return result;
    }

    public String getFile()
    {
        return listFiles.getItem(listFiles.getSelectedIndex());
    }
}

