package org.mathpiper.ui.gui.applications.circuitpiper.model;

import java.awt.Point;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Viewer;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Block;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.ACCurrentSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.ACVoltageSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Capacitor;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.Battery;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.CurrentSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.VoltageSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.transistors.Transistor;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.integratedcircuits.LogicalPackage;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.CurrentIntegrator;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Inductor;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Meter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Resistor;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Switch;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Wire;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.VoltageIntegrator;
import org.mathpiper.ui.gui.applications.circuitpiper.model.linearalgebra.SparseMatrix;
import org.mathpiper.ui.gui.applications.circuitpiper.view.CircuitPanel;
/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses//*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */

public final class Circuit {

    public ArrayList<Component> electricComponents;
    
    private HashMap<String, Integer> numbers;
    
    public HashMap<Point, Terminal> myTerminals;
    
    public HashMap<String, String> myComponentNames;
    
    private CircuitPanel circuitEnginePanel;
    
    public double time;
    public double stepSize;
    public SparseMatrix mainMatrix;
    

    public Circuit(CircuitPanel circuitEnginePanel) {
        
        this.circuitEnginePanel = circuitEnginePanel;
        
        init();
        
    }
    
    private void init()
    {
        numbers = new HashMap<String, Integer>();
        
        electricComponents = new ArrayList<Component>();
        
        myComponentNames = new HashMap<String, String>();
        
        myTerminals = new HashMap<Point, Terminal>();
        
        //todo:tk:find a better way to initialize these variables.
        Terminal.terminalCounter = 1;
        Transistor.componentCounter = 1;
        ACCurrentSource.componentCounter = 1;
        ACVoltageSource.componentCounter = 1;
        Battery.componentCounter = 1;
        CurrentSource.componentCounter = 1;
        VoltageSource.componentCounter = 1;
        LogicalPackage.componentCounter = 1;
        Meter.componentCounter = 1;
        Capacitor.componentCounter = 1;
        Inductor.componentCounter = 1;
        Resistor.componentCounter = 1;
        Switch.componentCounter = 1;
        Wire.componentCounter = 1;
        Block.componentCounter = 1;  
    }
    
    
    public void clear()
    {
        this.init();
    }
    
    
    public void putNumbers(Object object, Integer value)
    {
       // myComponentNames.put(object.toString(), object.toString());
        numbers.put(object.toString(), value);

    }
    
    public void putNumbers(Object object, String key, Integer value) throws Exception
    {
        
        if(object instanceof Component)
        {
            Component component = (Component) object;
            
            myComponentNames.put(component.primarySymbol + component.componentUID + key, component.primarySymbol + component.componentUID + key);
            numbers.put(component.primarySymbol + component.componentUID + key, value);
        }
        else if(object instanceof Point)
        {
            Point point = (Point) object;
            
            Terminal terminal = myTerminals.get(point);
            
            myComponentNames.put(point.toString() + key, "T" + terminal.terminalNumber + key);
            numbers.put(object + key, value);
        }        
        else
        {
            throw new Exception("Unknown circuit element.");
        }
    }
    
    public HashMap<String, Integer> getNumbers()
    {
        return numbers;
    }
    
    public void setNumbers(HashMap<String, Integer> hm)
    {
        numbers = hm;
    }

    public void save(String filePath) throws IOException{

        FileOutputStream file = new FileOutputStream(filePath);

        file.write(export().getBytes());

        file.close();
    }

    public void open(String filePath) throws Exception {

        String deck = new String(Files.readAllBytes(Paths.get(filePath)));
            
        readCircuitPiperFormat(deck);
    }
    
    
    public void exampleCircuit() throws Exception
    {
        String in =
"Capacitor 256 67 256 162 9.999999999999999E-6\n" +
"VoltageSource 162 67 162 162 5.0\n" +
"Wire 256 162 382 162\n" +
"Resistor 162 67 256 67 1000.0\n" +
"Voltmeter 382 67 382 162\n" +
"Wire 256 67 382 67\n" +
"Ammeter 162 162 256 162\n";

        readCircuit(in);
    }
    
    
    public void readCircuit(String code) throws Exception
    {
        if(code.trim().startsWith("$"))
        {
            readFalstadFormat(code);
        }
        else
        {
            readCircuitPiperFormat(code);
        }
        
        circuitEnginePanel.drawingPanel.isDrawGrid = false;
        circuitEnginePanel.showGridCheckBox.setSelected(false);
    }
    
    public void readFalstadFormat(String in) throws Exception
    {
        this.circuitEnginePanel.isRunning = false;
        while(this.circuitEnginePanel.isBusy)
        {
            Thread.sleep(100);
        }
        
        this.init();
        
        in = in.trim();
                
        String[] lines = in.split("\n");

        for(String line:lines)
        {
            if(line.startsWith("$"))
            {
                continue;
            }
            
            Stack<String> fields = new Stack<String>();
            Pattern regex = Pattern.compile("[^\\s\"']+|\"[^\"]*\"|'[^']*'");
            Matcher regexMatcher = regex.matcher(line);
            while (regexMatcher.find()) {
                fields.push(regexMatcher.group());
            }

            Collections.reverse(fields);

            String name = fields.pop();
            int headX = Integer.parseInt(fields.pop());
            int headY = Integer.parseInt(fields.pop());
            int tailX = Integer.parseInt(fields.pop());
            int tailY = Integer.parseInt(fields.pop());
            
            fields.pop();
            
            if(name.equals("v"))
            {
                name = "VoltageSource";
                fields.pop();
                fields.pop();
                String value = fields.pop();
                fields.clear();
                fields.push(value);
                
            }
            else if(name.equals("w"))
            {
                name = "Wire";
            }
            else if(name.equals("c"))
            {
                name = "Capacitor";
            }
            else if(name.equals("l"))
            {
                name = "Inductor";
            }
            else if(name.equals("r"))
            {
                name = "Resistor";
            }
            else if(name.equals("t"))
            {
                name = "TransistorPNP";
            }
            else if(name.equals("370"))
            {
                name = "Ammeter";
            }
            else
            {
                name = "Block";
            }


            Component component = circuitEnginePanel.newComponent(name, headX, headY, fields);

            component.moveTail(tailX, tailY);

            circuitEnginePanel.addElectricComponent(component, tailX, tailY);
        }

        circuitEnginePanel.drawingPanel.revalidate();
        circuitEnginePanel.drawingPanel.repaint();
        
    }
    
    
    public void readCircuitPiperFormat(String in) throws Exception
    {
        this.circuitEnginePanel.isRunning = false;
        while(this.circuitEnginePanel.isBusy)
        {
            Thread.sleep(100);
        }
        
        this.init();
        
        in = in.trim();
                
        String[] lines = in.split("\n");

        for(String line:lines)
        {
            Stack<String> fields = new Stack<String>();
            Pattern regex = Pattern.compile("[^\\s\"']+|\"[^\"]*\"|'[^']*'");
            Matcher regexMatcher = regex.matcher(line);
            while (regexMatcher.find()) {
                fields.push(regexMatcher.group());
            }

            Collections.reverse(fields);

            String name = fields.pop();
            int headX = Integer.parseInt(fields.pop());
            int headY = Integer.parseInt(fields.pop());
            int tailX = Integer.parseInt(fields.pop());
            int tailY = Integer.parseInt(fields.pop());


            Component component = circuitEnginePanel.newComponent(name, headX, headY, fields);

            component.moveTail(tailX, tailY);

            circuitEnginePanel.addElectricComponent(component, tailX, tailY);
        }

        circuitEnginePanel.drawingPanel.revalidate();
        circuitEnginePanel.drawingPanel.repaint();
        
    }
    
    
    public void viewAutoLayout()
    {
        Graph graph = new SingleGraph("Circuit");
        
        for(Terminal terminal:myTerminals.values())
        {
            String nodeName = "T" + terminal.terminalNumber;
            graph.addNode(nodeName);

        }

        for(Component component: this.electricComponents)
        {
            graph.addEdge(component.getID(), "T" + component.headTerminal.terminalNumber, "T" + component.tailTerminal.terminalNumber);

            Edge edge = graph.getEdge(component.getID());
            
            if(! (component instanceof Wire))
            {
                edge.addAttribute("ui.label", component.getID());
                edge.addAttribute("ui.style", "text-size: 20;");
            }
        }


        Viewer viewer = graph.display();
        viewer.setCloseFramePolicy(Viewer.CloseFramePolicy.CLOSE_VIEWER);
    }
    

    
    public String export()
    {
        StringBuilder sb = new StringBuilder();
        Iterator<Component> itr = electricComponents.iterator();
        while (itr.hasNext()) {
            Component component = itr.next();

            sb.append(component.toString() + "\n");
        }
        
        return sb.toString().trim();
    }
    
    public void run(boolean isRun)
    {
        if(isRun)
        {
            this.circuitEnginePanel.isRunning = true;
        }
        else
        {
            this.circuitEnginePanel.isRunning = false;
        }
    }
    
    
  
    public void rk(double h, double origh) throws Exception {
        //System.out.println(h);
        for (Component ec : circuitEnginePanel.myCircuit.electricComponents) {
            if ((ec.getClass() != Capacitor.class && ec.getClass() != Inductor.class) || ec.primaryValue == null || ec.isConstant) {
                if (ec.getClass() != CurrentIntegrator.class && ec.getClass() != VoltageIntegrator.class
                        && ec.getClass() != ACVoltageSource.class && ec.getClass() != ACCurrentSource.class) {
                    continue;
                }
            }
            ec.originalValue = ec.secondaryValue;
        }
        rk2(h / 2, false, time);
        
        for (Component ec : circuitEnginePanel.myCircuit.electricComponents) {
            if ((ec.getClass() != Capacitor.class && ec.getClass() != Inductor.class) || ec.primaryValue == null || ec.isConstant) {
                if (ec.getClass() != CurrentIntegrator.class && ec.getClass() != VoltageIntegrator.class
                        && ec.getClass() != ACVoltageSource.class && ec.getClass() != ACCurrentSource.class) {
                    continue;
                }
            }
            ec.originalCurrent = ec.I1;
        }
        rk2(h / 2, false, time + h / 2);
        
        for (Component ec : circuitEnginePanel.myCircuit.electricComponents) {
            if ((ec.getClass() != Capacitor.class && ec.getClass() != Inductor.class) || ec.primaryValue == null || ec.isConstant) {
                if (ec.getClass() != CurrentIntegrator.class && ec.getClass() != VoltageIntegrator.class
                        && ec.getClass() != ACVoltageSource.class && ec.getClass() != ACCurrentSource.class) {
                    continue;
                }
            }
            ec.twoStepValue = ec.y2;
            ec.secondaryValue = ec.originalValue;
        }
        rk2(h, false, time);
        
        for (Component ec : circuitEnginePanel.myCircuit.electricComponents) {
            if ((ec.getClass() != Capacitor.class && ec.getClass() != Inductor.class) || ec.primaryValue == null || ec.isConstant) {
                if (ec.getClass() != CurrentIntegrator.class && ec.getClass() != VoltageIntegrator.class
                        && ec.getClass() != ACVoltageSource.class && ec.getClass() != ACCurrentSource.class) {
                    continue;
                }
            }
            ec.oneStepValue = ec.y2;
            ec.secondaryValue = ec.originalValue;
        }
        
        double minh = h;
        for (Component ec : circuitEnginePanel.myCircuit.electricComponents) {
            if ((ec.getClass() != Capacitor.class && ec.getClass() != Inductor.class) || ec.primaryValue == null || ec.isConstant) {
                if (ec.getClass() != CurrentIntegrator.class && ec.getClass() != VoltageIntegrator.class
                        && ec.getClass() != ACVoltageSource.class && ec.getClass() != ACCurrentSource.class) {
                    continue;
                }
            }
            if (ec.originalValue == ec.twoStepValue) {
                //ec.isConstant=true;  #commented out dec 21 08
                //System.out.println("=");
                //continue;  //##$$removed dec 21 08 testing
            }
            double delta0;
            if (ec.getClass() == Capacitor.class || ec.getClass() == CurrentIntegrator.class
                    || ec.getClass() == ACCurrentSource.class) {
                delta0 = circuitEnginePanel.capacitorError;
            } else if (ec.getClass() == Inductor.class || ec.getClass() == ACVoltageSource.class) {
                delta0 = circuitEnginePanel.inductanceError;//error allowed in magnetic flux
            } else {//voltage integrator
                delta0 = circuitEnginePanel.inductanceError;//error allowed in magnetic flux
            }
            //double delta0=Math.max(circuitEnginePanel.capacitorError*h*Math.abs(h*ec.originalCurrent),
            //                      circuitEnginePanel.inductanceError);
            // *((//Math.abs(ec.twoStepValue)+
            //   Math.abs(h*ec.originalCurrent)))
            // +circuitEnginePanel.minimumAbsoluteError
            //   ;//was 10^-30
            if (ec.getClass() == Capacitor.class || ec.getClass() == CurrentIntegrator.class) {
                //delta0=Math.min(delta0,0.000001);
            }
            ec.delta1 = ec.twoStepValue - ec.oneStepValue;
            if (ec.delta1 != 0) {//##added Dec 31 2008
                minh = Math.min(minh,
                        0.9 * h * Math.pow(//.9
                                delta0
                                / Math.abs(ec.delta1) //Math.abs(0.000000000000000001/(ec.oneStepValue-ec.twoStepValue))
                                , 0.25));
            }
            if (minh < h) {
                //todo:tk:recursive use of rk.
                rk(Math.min(0.5 * minh, h), h);
                //System.out.println("recursive rk");
                return;
            }
            //System.out.println(minh);
        }
        
        for (Component ec : circuitEnginePanel.myCircuit.electricComponents) {
            if ((ec.getClass() != Capacitor.class && ec.getClass() != Inductor.class) || ec.primaryValue == null || ec.isConstant) {
                if (ec.getClass() != CurrentIntegrator.class && ec.getClass() != VoltageIntegrator.class) {
                    continue;
                }
            }
            ec.secondaryValue = ec.twoStepValue + ec.delta1 / 15;
            //System.out.println("delta1: "+ec.delta1);
            //System.out.println("twoStepValue: "+ec.twoStepValue);
            //System.out.println("oneStepValue: "+ec.oneStepValue);
        }
        //rk2(minh,false);
        time += h;
        
        this.stepSize = h;
    }


    public void rk2(double h, boolean reset, double t) throws Exception {
        mainMatrix = new SparseMatrix(circuitEnginePanel,
                circuitEnginePanel.myCircuit.myTerminals.values().size()
                + 3 * circuitEnginePanel.myCircuit.electricComponents.size()//max 3 equations
                ,
                2 * circuitEnginePanel.myCircuit.myTerminals.values().size() +//v and v'
                3 * circuitEnginePanel.myCircuit.electricComponents.size()
                + 1);
        
        // k1.
        mainMatrix.setUpMainMatrix(t);
        mainMatrix.rowEchelonFormMatrix(mainMatrix);
        mainMatrix.reducedRowEchelonFormMatrix(mainMatrix);
        for (Component ec : circuitEnginePanel.myCircuit.electricComponents) {
            if ((ec.getClass() != Capacitor.class && ec.getClass() != Inductor.class) || ec.primaryValue == null || ec.isConstant) {
                if (ec.getClass() != CurrentIntegrator.class && ec.getClass() != VoltageIntegrator.class
                        && ec.getClass() != ACVoltageSource.class && ec.getClass() != ACCurrentSource.class) {
                    continue;
                }
            }
            if (ec.getClass() == Capacitor.class || ec.getClass() == CurrentIntegrator.class
                    || ec.getClass() == ACCurrentSource.class) {
                ec.k1 = mainMatrix.getCurrent(ec);
                //System.out.println("k1:"+ec.k2);
            } else if (ec.getClass() == Inductor.class) {
                ec.k1 = ec.primaryValue * mainMatrix.getCurrentPrime(ec);
            } else {//voltage integrator or ACVoltageSource
                ec.k1 = mainMatrix.getVoltageDeltaV(ec);
            }
            ec.I1 = ec.k1;
            ec.y1 = ec.secondaryValue;
            ec.secondaryValue = ec.y1 + h / 2 * ec.k1;
        }
        
        // k2.
        mainMatrix.rows.clear();
        mainMatrix.cols.clear();
        mainMatrix.setUpMainMatrix(t + h / 2);//added dec 21 08
        mainMatrix.rowEchelonFormMatrix(mainMatrix);
        mainMatrix.reducedRowEchelonFormMatrix(mainMatrix);
        for (Component ec : circuitEnginePanel.myCircuit.electricComponents) {
            if ((ec.getClass() != Capacitor.class && ec.getClass() != Inductor.class) || ec.primaryValue == null || ec.isConstant) {
                if (ec.getClass() != CurrentIntegrator.class && ec.getClass() != VoltageIntegrator.class
                        && ec.getClass() != ACVoltageSource.class && ec.getClass() != ACCurrentSource.class) {
                    continue;
                }
            }
            if (ec.getClass() == Capacitor.class || ec.getClass() == CurrentIntegrator.class
                    || ec.getClass() == ACCurrentSource.class) {
                ec.k2 = mainMatrix.getCurrent(ec);
                //System.out.println("k2:"+ec.k2);
            } else if (ec.getClass() == Inductor.class) {
                ec.k2 = ec.primaryValue * mainMatrix.getCurrentPrime(ec);
            } else {//voltage integrator or ACVoltageSource
                ec.k2 = mainMatrix.getVoltageDeltaV(ec);
            }
            ec.secondaryValue = ec.y1 + h / 2 * ec.k2;
        }
        
        // k3. 
        mainMatrix.rows.clear();
        mainMatrix.cols.clear();
        mainMatrix.setUpMainMatrix(t + h / 2);//added dec 21 08
        mainMatrix.rowEchelonFormMatrix(mainMatrix);
        mainMatrix.reducedRowEchelonFormMatrix(mainMatrix);
        for (Component ec : circuitEnginePanel.myCircuit.electricComponents) {
            if ((ec.getClass() != Capacitor.class && ec.getClass() != Inductor.class) || ec.primaryValue == null || ec.isConstant) {
                if (ec.getClass() != CurrentIntegrator.class && ec.getClass() != VoltageIntegrator.class
                        && ec.getClass() != ACVoltageSource.class && ec.getClass() != ACCurrentSource.class) {
                    continue;
                }
            }
            if (ec.getClass() == Capacitor.class || ec.getClass() == CurrentIntegrator.class
                    || ec.getClass() == ACCurrentSource.class) {
                ec.k3 = mainMatrix.getCurrent(ec);
                //System.out.println("k3:"+ec.k3);
            } else if (ec.getClass() == Inductor.class) {
                ec.k3 = ec.primaryValue * mainMatrix.getCurrentPrime(ec);
            } else {//voltage integrator or ACVoltageSource
                ec.k3 = mainMatrix.getVoltageDeltaV(ec);
            }
            ec.secondaryValue = ec.y1 + h * ec.k3;
        }
        
        // k4.
        mainMatrix.rows.clear();
        mainMatrix.cols.clear();
        mainMatrix.setUpMainMatrix(t + h);//added dec 21 08
        mainMatrix.rowEchelonFormMatrix(mainMatrix);
        mainMatrix.reducedRowEchelonFormMatrix(mainMatrix);
        for (Component ec : circuitEnginePanel.myCircuit.electricComponents) {
            if ((ec.getClass() != Capacitor.class && ec.getClass() != Inductor.class) || ec.primaryValue == null || ec.isConstant) {
                if (ec.getClass() != CurrentIntegrator.class && ec.getClass() != VoltageIntegrator.class
                        && ec.getClass() != ACVoltageSource.class && ec.getClass() != ACCurrentSource.class) {
                    continue;
                }
            }
            if (ec.getClass() == Capacitor.class || ec.getClass() == CurrentIntegrator.class
                    || ec.getClass() == ACCurrentSource.class) {
                ec.k4 = mainMatrix.getCurrent(ec);
                //System.out.println("k4:"+ec.k4);
            } else if (ec.getClass() == Inductor.class) {
                ec.k4 = ec.primaryValue * mainMatrix.getCurrentPrime(ec);
            } else {//voltage integrator or ACVoltageSource
                ec.k4 = mainMatrix.getVoltageDeltaV(ec);
            }
            ec.y2 = ec.y1 + h * (ec.k1 + 2 * ec.k2 + 2 * ec.k3 + ec.k4) / 6.0;
            //ec.secondaryValue=ec.y2;
            if (reset) {
                ec.secondaryValue = ec.y1;
            }
        }
    }


    
    public synchronized void updateCircuit() throws Exception {
        //System.out.println("entered update");
        circuitEnginePanel.isBusy = true;

        if (circuitEnginePanel.myCircuit.myTerminals.size() > 0) {

            SparseMatrix instantaneousChargeMovementMatrix = new SparseMatrix(circuitEnginePanel,
                    circuitEnginePanel.myCircuit.myTerminals.values().size()
                    + 3 * circuitEnginePanel.myCircuit.electricComponents.size(), //max 3 equations
                    2 * circuitEnginePanel.myCircuit.myTerminals.values().size() +//v and v'
                    3 * circuitEnginePanel.myCircuit.electricComponents.size() + 1);

            instantaneousChargeMovementMatrix.setUpInstantaneousChargeMovementMatrix(time);
            //System.out.println("orig");
            //SparseMatrix.dumpMatrix(mainMatrix);
            instantaneousChargeMovementMatrix.rowEchelonFormMatrix(instantaneousChargeMovementMatrix);
            //System.out.println("after ref");
            //SparseMatrix.dumpMatrix(mainMatrix);
            instantaneousChargeMovementMatrix.reducedRowEchelonFormMatrix(instantaneousChargeMovementMatrix);
            //System.out.println("after rref");
            //SparseMatrix.dumpMatrix(mainMatrix);
            for (Component ec : circuitEnginePanel.myCircuit.electricComponents) {
                if (ec.getClass() == Capacitor.class) {
                    if (ec.primaryValue != null) {
                        //System.out.println("Old charge: "+ec.secondaryValue);
                        ec.secondaryValue += instantaneousChargeMovementMatrix.getDeltaQ(ec);
                        //System.out.println("Q= "+ec.secondaryValue);
                        //System.out.println("deltaQ= "+mainMatrix.getDeltaQ(ec));
                        //System.out.println("New charge: "+ec.secondaryValue);
                        //System.out.println(ec.secondaryValue);
                        //System.out.println("changed "+mainMatrix.getDeltaQ(ec));
                    }
                }
                if (ec.getClass() == CurrentIntegrator.class) {
                    ec.secondaryValue += instantaneousChargeMovementMatrix.getDeltaQ(ec);
                }
            }

            SparseMatrix instantaneousCurrentOrMagneticFluxMatrix = new SparseMatrix(circuitEnginePanel,
                    circuitEnginePanel.myCircuit.myTerminals.values().size()
                    + 3 * circuitEnginePanel.myCircuit.electricComponents.size()//max 3 equations
                    ,
                    2 * circuitEnginePanel.myCircuit.myTerminals.values().size() +//v and v'
                    3 * circuitEnginePanel.myCircuit.electricComponents.size()
                    + 1);

            instantaneousCurrentOrMagneticFluxMatrix.setUpInstantaneousCurrentOrMagneticFluxMatrix(time);
            //System.out.println("orig");
            //SparseMatrix.dumpMatrix(mainMatrix);
            instantaneousCurrentOrMagneticFluxMatrix.rowEchelonFormMatrix(instantaneousCurrentOrMagneticFluxMatrix);
            //System.out.println("after ref");
            //SparseMatrix.dumpMatrix(mainMatrix);
            instantaneousCurrentOrMagneticFluxMatrix.reducedRowEchelonFormMatrix(instantaneousCurrentOrMagneticFluxMatrix);
            //System.out.println("after rref");
            //SparseMatrix.dumpMatrix(mainMatrix);
            //System.out.println("start");
            for (Component ec : circuitEnginePanel.myCircuit.electricComponents) {
                if (ec.getClass() == Inductor.class) {
                    if (ec.primaryValue != null) {
                        //System.out.println("Old charge: "+ec.secondaryValue);
                        ec.secondaryValue += ec.primaryValue * instantaneousCurrentOrMagneticFluxMatrix.getDeltaCurrent(ec);
                        //if (mainMatrix.getCurrent(ec)==0){
                        //  ec.secondaryValue=0;
                        //}
                        //System.out.println("I= "+ec.secondaryValue);
                        //System.out.println("deltacurrent= "+mainMatrix.getDeltaCurrent(ec));
                        //System.out.println("New charge: "+ec.secondaryValue);
                        //System.out.println(ec.secondaryValue);
                        //System.out.println("changed "+mainMatrix.getDeltaQ(ec));
                    }
                } else if (ec.getClass() == VoltageIntegrator.class) {
                    ec.secondaryValue += instantaneousCurrentOrMagneticFluxMatrix.getDeltaFlux(ec);
                }
            }

            mainMatrix = new SparseMatrix(circuitEnginePanel,
                    circuitEnginePanel.myCircuit.myTerminals.values().size()
                    + 3 * circuitEnginePanel.myCircuit.electricComponents.size()//max 3 equations
                    ,
                    2 * circuitEnginePanel.myCircuit.myTerminals.values().size() +//v and v'
                    3 * circuitEnginePanel.myCircuit.electricComponents.size()
                    + 1);
            mainMatrix.setUpMainMatrix(time);
            mainMatrix.rowEchelonFormMatrix(mainMatrix);
            mainMatrix.reducedRowEchelonFormMatrix(mainMatrix);
            circuitEnginePanel.drawingPanel.updateGraphsAndMeters(mainMatrix, time);

            //sparseMatrix.setUpMainMatrix();
            double timeStep = 0.05;
            double finalTime = time + timeStep;
            long start = System.currentTimeMillis();
            //findConstantCaps();  #commented out Dec 21 08
            if (circuitEnginePanel.timeOption) {
                do {
                    //System.out.println("entering rk");
                    rk(timeStep, timeStep);
                } while (time < finalTime && System.currentTimeMillis() < start + 0.03);
                
                circuitEnginePanel.drawingPanel.updateGraphsAndMeters(mainMatrix, time);
            }

            //while (t<finalt&&System.currentTimeMillis()<start+0.5){
            //rk(0.05/2);
            // }
            //System.out.println("end rk"+System.currentTimeMillis());
            //below is just for testing
     /* mainMatrix=new SparseMatrix(circuitEnginePanel,
             circuitEnginePanel.myGridPoints.values().size()+
             3*circuitEnginePanel.myCircuit.electricComponents.size()//max 3 equations
             ,
             2*circuitEnginePanel.myGridPoints.values().size()+//v and v'
             3*circuitEnginePanel.myCircuit.electricComponents.size()+
             1);
             mainMatrix.setUpMainMatrix();
             //System.out.println("original matrix:");
             //SparseMatrix.dumpMatrix(mainMatrix);
      
             mainMatrix.refMatrix(mainMatrix);
             //System.out.println("after ref:");
             //SparseMatrix.dumpMatrix(mainMatrix);
             //System.out.println("after ref");
      
             mainMatrix.rrefMatrix(mainMatrix);*/
        }

        circuitEnginePanel.isBusy = false;
        //System.out.println("leaving update");
    }
    

    
    public String dumpMatrix() throws Exception {
        return SparseMatrix.dumpMatrix(mainMatrix) + toString();
    }
    
    
    public String toMathPiper()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("[\n");
        Iterator<Component> components = electricComponents.iterator();
        while (components.hasNext()) {
            Component component = components.next();
            String componentName = component.getID();
            sb.append("[\"" + componentName + "\",[");
            sb.append("[\"name\", \"" + componentName + "\"]");

            sb.append(", [\"Terminals\", ");
            Terminal headTerminal = component.headTerminal;
            Terminal tailTerminal = component.tailTerminal;
            sb.append("[\"T" + headTerminal.terminalNumber +  "\", \"T" + tailTerminal.terminalNumber + "\"]]");
            
            Double componentCurrent = mainMatrix.getCurrent(component);
            sb.append(", [\"Current\", " + componentCurrent + "]");
            
            if(component instanceof Resistor) //todo:tk:handle more components than just resistors.
            {
                Double voltage = component.primaryValue * componentCurrent;
                sb.append(", [\"Voltage\", " + voltage + "]");
            }
            sb.append("]],\n");
        }
        
        sb.append("];\n");
        
        return sb.toString();
    }
        
    
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        // Terminals.
        sb.append("Terminals\n");
        ArrayList<Terminal> terminalsList = new ArrayList<Terminal>(myTerminals.values());
        
        Collections.sort(terminalsList, new Comparator<Terminal>() {
            public int compare(Terminal lhs, Terminal rhs) {
                return lhs.terminalNumber < rhs.terminalNumber ? -1 : (lhs.terminalNumber > rhs.terminalNumber) ? 1 : 0;
            }
        });

        for (Terminal terminal : terminalsList){
            sb.append(terminal.getID() + ", " + "(" + terminal.getX() + ", " + terminal.getY() + ")\n");
        }
        
        sb.append("\n\n");
        
        
        // Components.
        sb.append("Componets\n");
        Collections.sort(electricComponents, new Comparator<Component>() {
            public int compare(Component lhs, Component rhs) {
                return lhs.getID().compareTo(rhs.getID());
            }
        });
        
        Iterator<Component> itr = electricComponents.iterator();
        while (itr.hasNext()) {
            Component component = itr.next();
            
            if(component instanceof Meter)
            {
                continue;
            }

            String componentName = component.getID();
            Terminal headTerminal = component.headTerminal;
            Terminal tailTerminal = component.tailTerminal;
            Double componentCurrent = mainMatrix.getCurrent(component);
            sb.append(componentName +
                      ", T" + headTerminal.terminalNumber +
                      ", T" + tailTerminal.terminalNumber +
                      ", I: " + String.format("%.8f",componentCurrent));
            if(component instanceof Resistor)
            {
                Double voltage = component.primaryValue * componentCurrent;
                sb.append(", V: " + String.format("%.8f", voltage));
            }
            sb.append("\n");
        }

        return sb.toString().trim();// + "\n\n" + toMathPiper();
    }
    


}
