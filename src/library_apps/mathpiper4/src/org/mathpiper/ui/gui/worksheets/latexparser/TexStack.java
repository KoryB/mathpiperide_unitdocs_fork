
package org.mathpiper.ui.gui.worksheets.latexparser;


public interface TexStack
{
    Object pop();
    
    void push(Object o);

    void process(String aType) throws Throwable;

    void processLiteral(String aExpression);
}
