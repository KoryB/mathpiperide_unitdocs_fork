package org.mathpiper.ui.gui.applications.voscilloscope.oscilloscope;

import java.awt.*;
import java.awt.event.*;
import java.io.PrintStream;

/**
 * <p>Title: Virtual Oscilloscope.</p>
 * <p>Description: A Oscilloscope simulator</p>
 * <p>Copyright (C) 2003 José Manuel Gómez Soriano</p>
 * <h2>License</h2>
 * <p>
 This file is part of Virtual Oscilloscope.

 Virtual Oscilloscope is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Virtual Oscilloscope is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Virtual Oscilloscope; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 * </p>
 */

// Referenced classes of package es.upv.scope:
//            Signal, Screen

public class Preview extends Panel
    implements Runnable
{
    class PreviewMouseInputAdapter extends MouseAdapter
        implements MouseMotionListener
    {

        private int x;
        private int y;
        private int posant;
        private int yant;

        public void mouseDragged(MouseEvent e)
        {
            int newpos = (posant + x) - e.getX();
            int newy = yant - (y - e.getY()) / 10;
            if(newpos < 0)
            {
                posicion = 0;
            } else
            if(newpos > signal.getLength())
            {
                posicion = signal.getLength();
            } else
            {
                posicion = newpos;
            }
            if(newy < -20)
            {
                signal.setYPos(-20);
            } else
            if(newy > 20)
            {
                signal.setYPos(20);
            } else
            {
                signal.setYPos(newy);
            }
        }

        public void mouseMoved(MouseEvent mouseevent)
        {
        }

        public void mousePressed(MouseEvent e)
        {
            y = e.getY();
            yant = signal.getYPos();
            x = e.getX();
            posant = posicion;
        }

        PreviewMouseInputAdapter()
        {
        }
    }


    private Screen screen;
    private Scrollbar sbQuality;
    private Scrollbar sbAmplitud;
    private Scrollbar sbTiempos;
    private Label LPrev;
    private Label LCal;
    private Label LMin;
    private Label LMax;
    private Signal signal;
    private Thread hilo;
    private int posicion;

    public Preview()
    {
        signal = new Signal();
        init();
    }

    public void start()
    {
        hilo.setPriority(1);
        hilo.start();
    }

    public void stop()
    {
        hilo.stop();
    }

    public Preview(Signal signal)
    {
        this.signal = signal;
        init();
    }

    public void setSignal(Signal signal)
    {
        this.signal = signal;
    }

    private void init()
    {
        posicion = 0;
        screen = new Screen();
        screen.setFocus(2);
        screen.setIntens(255);
        screen.encender();
        screen.setCursor(new Cursor(13));
        sbQuality = new Scrollbar(0, 1, 1, 1, 11);
        sbAmplitud = new Scrollbar(1, 0, 1, -10, 10);
        sbTiempos = new Scrollbar(0, 0, 1, -10, 10);
        LPrev = new Label("Preview");
        LCal = new Label("Quality");
        LMin = new Label("MIN");
        LMax = new Label("MAX");
        setSize(174, 220);
        setLayout(null);
        hilo = new Thread(this);
        PreviewMouseInputAdapter pmia = new PreviewMouseInputAdapter();
        screen.addMouseListener(pmia);
        screen.addMouseMotionListener(pmia);
    }

    public void addNotify()
    {
        super.addNotify();
        add(screen);
        add(sbQuality);
        add(sbAmplitud);
        add(sbTiempos);
        add(LPrev);
        add(LCal);
        add(LMin);
        add(LMax);
    }

    public void setSize(int width, int height)
    {
        super.setSize(width, height);
        screen.setBounds(4, 19, width - 24, height - 70);
        screen.setBackground(Color.black);
        sbQuality.setBounds(4, height - 34, width - 24, 13);
        sbAmplitud.setBounds(width - 18, 19, 13, height - 70);
        sbTiempos.setBounds(4, height - 49, width - 24, 13);
        LPrev.setBounds(width / 2 - 50, 3, 100, 13);
        LCal.setBounds(width / 2 - 25, height - 20, 50, 13);
        LMin.setBounds(4, height - 20, 40, 13);
        LMax.setBounds(width - 46, height - 20, 43, 13);
    }

    public void drawSignal()
    {
        double cal = sbQuality.getValue();
        int width = screen.getSize().width;
        int height = screen.getSize().height;
        double escalax = (double)Math.pow(2D, sbTiempos.getValue());
        double time = ((double)signal.getLength() * escalax) / (signal.getFastSampling() * (double)10 * cal * (double)width);
        double escalay = ((double)Math.pow(2D, -sbAmplitud.getValue()) * (double)height) / (signal.getVoltageMax() * (double)2);
        for(double x = 0.0F; x < (double)width * cal; x++)
        {
            double v = signal.sigSample(time);
            double y = (int)(v * escalay);
            screen.drawCol((int)(x / cal));
            screen.putPixel((int)(x / cal), (int)(-y), Color.white);
        }

    }

    public void run()
    {
        do
        {
            signal.setPosShow(posicion);
            drawSignal();
            try
            {
                Thread.sleep(200L);
            }
            catch(InterruptedException e)
            {
                System.out.println(e);
            }
        } while(true);
    }
}

