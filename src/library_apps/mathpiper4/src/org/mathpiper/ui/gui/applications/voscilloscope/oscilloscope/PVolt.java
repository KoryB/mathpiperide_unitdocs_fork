package org.mathpiper.ui.gui.applications.voscilloscope.oscilloscope;

import org.mathpiper.ui.gui.applications.voscilloscope.simulator.RotateBitmap;
import org.mathpiper.ui.gui.applications.voscilloscope.simulator.StaticBitmapButton;
import org.mathpiper.ui.gui.applications.voscilloscope.simulator.Message;
import org.mathpiper.ui.gui.applications.voscilloscope.simulator.InterruptBitmap;
import java.awt.*;
import java.net.URL;

/**
 * <p>Title: Virtual Oscilloscope.</p>
 * <p>Description: A Oscilloscope simulator</p>
 * <p>Copyright (C) 2003 José Manuel Gómez Soriano</p>
 * <h2>License</h2>
 * <p>
 This file is part of Virtual Oscilloscope.

 Virtual Oscilloscope is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Virtual Oscilloscope is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Virtual Oscilloscope; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 * </p>
 */

// Referenced classes of package es.upv.scope:
//            ButtonVolt, CommutatorACDC, PBackground

public class PVolt extends Panel
{

    public InterruptBitmap BtnCH12;
    public InterruptBitmap BtnDual;
    public InterruptBitmap BtnAdd;
    public ButtonVolt BtnVolt1;
    public StaticBitmapButton BtnChannel1;
    public InterruptBitmap BtnInv1;
    public CommutatorACDC BtnAcDc1;
    public RotateBitmap BtnYPos1;
    public InterruptBitmap BtnCiclica1;
    public ButtonVolt BtnVolt2;
    public StaticBitmapButton BtnChannel2;
    public InterruptBitmap BtnInv2;
    public CommutatorACDC BtnAcDc2;
    public RotateBitmap BtnYPos2;
    public InterruptBitmap BtnCiclica2;

    public PVolt()
    {
        setBackground(PBackground.background);
        setForeground(PBackground.panel);
        setLayout(null);
        constructElements();
    }

    public void iniciarElementos()
    {
        BtnCH12.setValue(false);
        BtnDual.setValue(false);
        BtnAdd.setValue(false);
        BtnVolt1.setValue(20F);
        BtnInv1.setValue(false);
        BtnAcDc1.setValue(2);
        BtnYPos1.setValue(0.0F);
        BtnCiclica1.setValue(false);
        BtnVolt2.setValue(20F);
        BtnInv2.setValue(false);
        BtnAcDc2.setValue(2);
        BtnYPos2.setValue(0.0F);
        BtnCiclica2.setValue(false);
    }

    public void addNotify()
    {
        super.addNotify();
        positionElements();
        iniciarElementos();
    }

    private void constructElements()
    {
        String fino[] = {
            "bb00.gif", "bb01.gif", "bb02.gif", "bb03.gif", "bb04.gif", "bb05.gif", "bb06.gif", "bb07.gif", "bb08.gif", "bb09.gif",
            "bb10.gif", "bb11.gif", "bb12.gif", "bb13.gif", "bb14.gif", "bb15.gif", "bb16.gif", "bb17.gif", "bb18.gif", "bb19.gif",
            "bb20.gif", "bb21.gif", "bb22.gif", "bb23.gif", "bb24.gif", "bb25.gif", "bb26.gif", "bb27.gif", "bb28.gif", "bb29.gif",
            "bb31.gif", "bb32.gif", "bb33.gif", "bb34.gif", "bb35.gif", "bb36.gif", "bb37.gif", "bb38.gif", "bb39.gif", "bb40.gif",
            "bb41.gif", "bb42.gif", "bb43.gif", "bb44.gif", "bb45.gif"
        };
        String Voltios[] = {
            "ba26.gif", "ba29.gif", "ba33.gif", "ba37.gif", "ba40.gif", "ba43.gif", "ba01.gif", "ba05.gif", "ba09.gif", "ba13.gif",
            "ba17.gif", "ba20.gif"
        };
        String channel1[] = {
            "channel1.gif"
        };
        String channel2[] = {
            "channel2.gif"
        };
        String pulsador[] = {
            "Bc01.gif", "Bc00.gif"
        };
        String AcDc[] = {
            "AcDc02.gif", "AcDc01.gif", "AcDc00.gif"
        };
        double ValVoltage[] = {
            0.005F, 0.01F, 0.02F, 0.05F, 0.1F, 0.2F, 0.5F, 1.0F, 2.0F, 5F,
            10F, 20F
        };
        double ValuesFinos[] = new double[45];
        for(int i = 0; i < 45; i++)
        {
            ValuesFinos[i] = (double)((double)i * 0.5D);
        }

        double ValuesYPos[] = new double[291];
        for(int i = 0; i < 291; i++)
        {
            ValuesYPos[i] = i - 145;
        }

        /*Message message = Message.makeDialog("              Panel 2 of 3. Loading button 1 of 12: button of INV I        " +
"     "
, "Information", false);*/
        BtnInv1 = new InterruptBitmap(pulsador);
        add(BtnInv1);
        BtnInv1.putTitulo("Inv. I");
        //message.setText("Panel 2 of 3. Loading button 2 of 15: button of INV II");
        BtnInv2 = new InterruptBitmap(pulsador);
        add(BtnInv2);
        BtnInv2.putTitulo("Inv. II");
        //message.setText("Panel 2 of 3. Loading button 3 of 15: button of CH I/II");
        BtnCH12 = new InterruptBitmap(pulsador);
        add(BtnCH12);
        BtnCH12.putTitulo("CH I/II");
        //message.setText("Panel 2 of 3. Loading button 4 of 15: button of DUAL");
        BtnDual = new InterruptBitmap(pulsador);
        add(BtnDual);
        BtnDual.putTitulo("Dual");
        //message.setText("Panel 2 of 3. Loading button 5 of 15: button of ADD");
        BtnAdd = new InterruptBitmap(pulsador);
        add(BtnAdd);
        BtnAdd.putTitulo("Add");
        //message.setText("Panel 2 of 3. Loading button 6 of 15: button of CICLICA I");
        BtnCiclica1 = new InterruptBitmap(pulsador);
        add(BtnCiclica1);
        BtnCiclica1.putTitulo("Ciclica I");
        BtnCiclica2 = new InterruptBitmap(pulsador);
        //message.setText("Panel 2 of 3. Loading button 7 of 15: button of CICLICA II");
        add(BtnCiclica2);
        BtnCiclica2.putTitulo("Ciclica II");
        //message.setText("Panel 2 of 3. Loading button 8 of 15: potenci\363metro of YPOS I");
        BtnYPos1 = new RotateBitmap(fino, ValuesYPos);
        add(BtnYPos1);
        BtnYPos1.putTitulo("YPos I");
        //message.setText("Panel 2 of 3. Loading button 9 of 15: potenci\363metro of YPOS II");
        BtnYPos2 = new RotateBitmap(fino, ValuesYPos);
        add(BtnYPos2);
        BtnYPos2.putTitulo("YPos II");
        //message.setText("Panel 2 of 3. Loading button 10 of 15: potenci\363metro of V/DIV I");
        BtnVolt1 = new ButtonVolt(Voltios, ValVoltage, fino, ValuesFinos);
        add(BtnVolt1);
        BtnVolt1.putTitulo("V/DIV I");
        //message.setText("Panel 2 of 3. Loading button 11 of 15: potenci\363metro of V/DIV II");
        BtnVolt2 = new ButtonVolt(Voltios, ValVoltage, fino, ValuesFinos);
        add(BtnVolt2);
        BtnVolt2.putTitulo("V/DIV II");
        //message.setText("Panel 2 of 3. Loading button 12 of 15: conmutador of AC/DC/GND I");
        BtnAcDc1 = new CommutatorACDC(AcDc);
        add(BtnAcDc1);
        BtnAcDc1.setJustificacion(true);
        //message.setText("Panel 2 of 3. Loading button 13 of 15: conmutador of AC/DC/GND II");
        BtnAcDc2 = new CommutatorACDC(AcDc);
        add(BtnAcDc2);
        BtnAcDc2.setJustificacion(false);
        //message.setText("Panel 2 of 3. Loading button 14 of 15: ENTRADA I");
        BtnChannel1 = new StaticBitmapButton(channel1);
        add(BtnChannel1);
        BtnChannel1.putTitulo("Ent. I");
        //message.setText("Panel 2 of 3. Loading button 15 of 15: ENTRADA II");
        BtnChannel2 = new StaticBitmapButton(channel2);
        add(BtnChannel2);
        BtnChannel2.putTitulo("Ent. II");
        //message.closeDialog();
    }

    public void positionElements()
    {
        BtnYPos1.setLocation(1, 3);
        BtnYPos2.setLocation(347, 3);
        BtnAcDc1.setLocation(4, 71);
        BtnAcDc2.setLocation(357, 71);
        BtnChannel1.setLocation(4, 119);
        BtnChannel2.setLocation(350, 119);
        BtnVolt1.setLocation(59, 3);
        BtnVolt2.setLocation(200, 3);
        BtnCiclica1.setLocation(57, 156);
        BtnCiclica2.setLocation(299, 156);
        BtnInv1.setLocation(104, 156);
        BtnInv2.setLocation(265, 156);
        BtnCH12.setLocation(140, 156);
        BtnDual.setLocation(184, 156);
        BtnAdd.setLocation(223, 156);
    }

    public Dimension getMinimumSize()
    {
        return new Dimension(396, 197);
    }

    public Dimension getPreferredSize()
    {
        return getMinimumSize();
    }

    public void setLocation(int x, int y)
    {
        super.setLocation(x, y);
        setSize(getMinimumSize());
    }

    public void update(Graphics g)
    {
        paint(g);
    }

    public void paint(Graphics g)
    {
        drawBackground(g);
    }

    public void drawBackground(Graphics g)
    {
        g.setColor(PBackground.relleno);
        g.fillRoundRect(0, 0, 396, 197, 10, 10);
    }
}
