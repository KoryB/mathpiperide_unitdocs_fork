package org.mathpiper.ui.gui.applications.voscilloscope.oscilloscope;


import java.awt.*;

/**
 * <p>Title: Virtual Oscilloscope.</p>
 * <p>Description: A Oscilloscope simulator</p>
 * <p>Copyright (C) 2003 José Manuel Gómez Soriano</p>
 * <h2>License</h2>
 * <p>
 This file is part of Virtual Oscilloscope.

 Virtual Oscilloscope is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Virtual Oscilloscope is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Virtual Oscilloscope; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 * </p>
 */

// Referenced classes of package es.upv.scope:
//            PBackground

public class Screen extends Canvas
{

    private boolean enabled;
    private int intens;
    private int focus;
    private int level;
    private Graphics g;

    public synchronized void drawCol(int x)
    {
        int width = getSize().width;
        int height = getSize().height;
        int div10Width = width / 10;
        int div10Height = height / 10;
        int div50Width = width / 50;
        int div50Height = height / 50;
        if(x >= 0 && x <= width)
        {
            g.setColor(Color.black);
            g.drawLine(x, 0, x, height - 2);
            g.setColor(Color.green);
            g.drawLine(x, height - 1, x, height - 1);
            for(int i = 0; i <= height; i += div10Height)
            {
                g.drawLine(x, i, x, i);
            }

            if(x >= (width >> 1) - 3 && x <= (width >> 1) + 3)
            {
                for(int i = 1; i < 50; i++)
                {
                    g.drawLine(x, div50Height * i, x, div50Height * i);
                }

            }
            if(x % div10Width == 0)
            {
                g.drawLine(x, 0, x, height);
            } else
            if(x % div50Width == 0)
            {
                g.drawLine(x, (height >> 1) - 3, x, (height >> 1) + 3);
            } else
            if(x == width - 1)
            {
                g.drawLine(x, 0, x, height);
            }
        }
    }

    public synchronized void setLevel(int level)
    {
        eraseLevel();
        this.level = level;
        drawLevel();
    }

    private synchronized void eraseLevel()
    {
        int height = getSize().height;
        int x = getLocation().x - 10;
        int y = (getLocation().y + getSize().height / 2) - level;
        Graphics g = getParent().getGraphics();
        if(y < getLocation().y || y > getLocation().y + height)
        {
            return;
        } else
        {
            g.setColor(PBackground.relleno);
            g.drawLine(x, y - 2, x, y + 2);
            g.setColor(PBackground.background);
            g.drawLine(x + 1, y - 2, x + 1, y + 2);
            g.drawLine(x + 2, y - 2, x + 2, y + 2);
            g.drawLine(x + 3, y - 4, x + 3, y + 5);
            g.drawLine(x + 4, y - 2, x + 4, y + 2);
            g.setColor(Color.black);
            g.drawLine(x + 5, y - 2, x + 5, y + 2);
            return;
        }
    }

    private synchronized void drawLevel()
    {
        int height = getSize().height;
        int x = getLocation().x - 10;
        int y = (getLocation().y + height / 2) - level;
        Graphics g = getParent().getGraphics();
        if(y < getLocation().y || y > getLocation().y + height)
        {
            return;
        } else
        {
            g.setColor(Color.red);
            g.drawLine(x, y - 2, x, y + 2);
            g.drawLine(x + 1, y - 2, x + 1, y + 2);
            g.drawLine(x + 2, y - 2, x + 2, y + 2);
            g.drawLine(x + 3, y - 4, x + 3, y + 4);
            g.drawLine(x + 4, y - 2, x + 4, y + 2);
            g.drawLine(x + 5, y, x + 5, y);
            return;
        }
    }

    private synchronized void drawEjes()
    {
        int width = getSize().width;
        int height = getSize().height;
        g.setColor(Color.green);
        g.drawRect(0, 0, width - 1, height - 1);
        for(int i = 1; i < 10; i++)
        {
            g.drawLine((width * i) / 10, 0, (width * i) / 10, height);
            g.drawLine(0, (height * i) / 10, width, (height * i) / 10);
        }

        for(int i = 1; i < 50; i++)
        {
            g.drawLine((width * i) / 50, height / 2 - 3, (width * i) / 50, height / 2 + 3);
            g.drawLine(width / 2 - 3, (height * i) / 50, width / 2 + 3, (height * i) / 50);
        }

    }

    public synchronized void drawBackground()
    {
      g = getGraphics();
        g.setColor(Color.black);
        g.fillRect(0, 0, getSize().width, getSize().height);
        drawEjes();
    }

    public Screen()
    {
        focus = 0;
        intens = 0;
        level = 0;
    }

    public synchronized void addNotify()
    {
        super.addNotify();
        setBackground(Color.black);
        setForeground(Color.black);
        g = getGraphics();
    }

    public synchronized void encender()
    {
        enabled = true;
    }

    public synchronized void apagar()
    {
        enabled = false;
    }

    public synchronized boolean isPower()
    {
        return enabled;
    }

    public synchronized void setIntens(int intensidad)
    {
        intens = intensidad;
    }

    public synchronized int getIntens()
    {
        return intens;
    }

    public synchronized void setFocus(int focus)
    {
        this.focus = focus;
    }

    public synchronized int getFocus()
    {
        return focus;
    }
    
    public synchronized void putLine(int oldX, int oldY, int x, int y, Color color)
    {
        if(enabled)
        {
            int height = getSize().height;
            g.setColor(new Color((color.getRed() * intens) / 255, (color.getGreen() * intens) / 255, (color.getBlue() * intens) / 255));
            g.drawLine(oldX, height / 2 - oldY, x, height / 2 - y);
        }
    }

    public synchronized void putPixel(int x, int y, Color color)
    {
        int height = getSize().height;
        g.setColor(new Color((color.getRed() * intens) / 255, (color.getGreen() * intens) / 255, (color.getBlue() * intens) / 255));
        if(enabled)
        {
            y = height / 2 - y;
            switch(focus)
            {
            case 17: // '\021'
                g.drawLine(x - 3, y - 2, x - 3, y - 2);
                // fall through

            case 16: // '\020'
                g.drawLine(x - 3, y + 2, x - 3, y + 2);
                // fall through

            case 15: // '\017'
                g.drawLine(x - 4, y - 1, x - 4, y + 1);
                // fall through

            case 14: // '\016'
                g.drawLine(x, y - 1, x, y - 1);
                // fall through

            case 13: // '\r'
                g.drawLine(x, y + 1, x, y + 1);
                // fall through

            case 12: // '\f'
                g.drawLine(x - 2, y - 2, x - 1, y - 2);
                // fall through

            case 11: // '\013'
                g.drawLine(x - 2, y + 2, x - 1, y + 2);
                // fall through

            case 10: // '\n'
                g.drawLine(x - 3, y - 1, x - 3, y - 1);
                // fall through

            case 9: // '\t'
                g.drawLine(x - 3, y + 1, x - 3, y + 1);
                // fall through

            case 8: // '\b'
                g.drawLine(x - 2, y - 1, x - 2, y - 1);
                // fall through

            case 7: // '\007'
                g.drawLine(x - 2, y + 1, x - 2, y + 1);
                // fall through

            case 6: // '\006'
                g.drawLine(x - 3, y, x - 3, y);
                // fall through

            case 5: // '\005'
                g.drawLine(x - 2, y, x - 2, y);
                // fall through

            case 4: // '\004'
                g.drawLine(x - 1, y - 1, x - 1, y - 1);
                // fall through

            case 3: // '\003'
                g.drawLine(x - 1, y + 1, x - 1, y + 1);
                // fall through

            case 2: // '\002'
                g.drawLine(x - 1, y, x - 1, y);
                // fall through

            case 1: // '\001'
                g.drawLine(x, y, x, y);
                break;
            }
        }
    }

    public synchronized void erase()
    {
        drawBackground();
    }

    public synchronized void update(Graphics g)
    {
        paint(g);
    }

    public synchronized void repaint()
    {
        paint(getGraphics());
    }

    public synchronized void paint(Graphics g)
    {
        drawBackground();
        drawLevel();
    }
}
