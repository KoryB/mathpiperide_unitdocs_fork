package org.mathpiper.ui.gui.applications.circuitpiper.model.linearalgebra;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.mathpiper.ui.gui.applications.circuitpiper.model.Terminal;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.ACCurrentSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.ACVoltageSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Ammeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.CapacitanceMeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Capacitor;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.CurrentIntegrator;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.CurrentSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.InductanceMeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Inductor;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Ohmmeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Resistor;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Switch;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.VoltageIntegrator;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.VoltageSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Voltmeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Wire;
import org.mathpiper.ui.gui.applications.circuitpiper.view.CircuitPanel;
import org.mathpiper.ui.gui.applications.circuitpiper.view.PrintVertical;

/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */
public final class SparseMatrix {

    public ArrayList<Vector> rows;
    public ArrayList<Vector> cols;
    int firstPointVoltageIndex;

    private Boolean matrixError;
    private CircuitPanel myParent;
    private int numberOfKCLeqs;
    private static final String LAST = "-1";

    public SparseMatrix(CircuitPanel parent, int numRows, int numCols) {
        myParent = parent;
        rows = new ArrayList<Vector>(0);//changed to 0 on dec 6 08 numRows);//Row[numRows];
        //for (int i=0;i<numRows;i++){
        //rows[i]=new Row();   
        //}
        cols = new ArrayList<Vector>(0);//changed to 0 on dec 6 08  numCols);//Row[numCols];
        //for(int i=0;i<numCols;i++){
        //   cols[i]=new Row();
        //}
    }

    public Vector get(int row) {
        return rows.get(row);//[row];   
    }

    public void putElement(int row, int col, double x) {
        rows.get(row).put(col, x);
        cols.get(col).put(row, x);
    }

    public void swapRows(int row1, int row2) {//swap rows
        
        for (Object obj : rows.get(row1).contents.keySet()) {//for each element in row1
            Integer colNum = (Integer) obj;
            if (rows.get(row2).contents.containsKey(colNum)) {//if row2 has an element in the same position
                double temp = cols.get(colNum).get(row1);//temp gets element in row1
                //this.putElement(row1, colNum, x)
                cols.get(colNum).put(row1, cols.get(colNum).get(row2));//row1 gets row2
                cols.get(colNum).put(row2, temp);//row2 gets temp
            } else {//if row2 doesn't have an element in the same position
                cols.get(colNum).put(row2, cols.get(colNum).get(row1));//row2 gets row1
                cols.get(colNum).remove(row1);//remove element in row1
            }
        }
        
        for (Object obj : rows.get(row2).contents.keySet()) {//for each element in row2
            Integer colNum = (Integer) obj;
            if (!rows.get(row1).contents.containsKey(colNum)) {//if row1 doesn't have an element in the same position
                cols.get(colNum).put(row1, rows.get(row2).get(colNum));//better than line below//##added Dec 26 2008
                //cols.get(i).put(row1,cols.get(i).get(row2));
                cols.get(colNum).remove(row2);
            }
        }
        
        Vector temp = rows.get(row1);
        rows.set(row1, rows.get(row2));
        //rows.get(row1)=rows.get(row2);
        rows.set(row2, temp);
        //rows.get(row2)=temp;
        //System.out.println("swap");
        //printMatrix(sparseMatrix);
    }

    public void swapCols(int col1, int col2) {//swap cols
        for (Object obj : cols.get(col1).contents.keySet()) {//for each element in col1
            Integer rowNum = (Integer) obj;//i is the row num
            if (cols.get(col2).contents.containsKey(rowNum)) {//if col2 has an element in the same position
                double temp = rows.get(rowNum).get(col1);
                rows.get(rowNum).put(col1, rows.get(rowNum).get(col2));
                rows.get(rowNum).put(col2, temp);
            } else {//if col2 doesn't have an element in the same position
                rows.get(rowNum).put(col2, rows.get(rowNum).get(col1));
                rows.get(rowNum).remove(col1);
            }
        }
        for (Object obj : cols.get(col2).contents.keySet()) {//for each element in col2
            Integer rowNum = (Integer) obj;
            if (!cols.get(col1).contents.containsKey(rowNum)) {//if this isn't in col1
                rows.get(rowNum).put(col1, cols.get(col2).get(rowNum));
                //rows.get(i).put(col1,rows.get(i).get(col2));
                rows.get(rowNum).remove(col2);
            }
        }
        Vector temp = cols.get(col1);
        cols.set(col1, cols.get(col2));
        //rows.get(row1)=rows.get(row2);
        cols.set(col2, temp);
        for (String obj : this.myParent.myCircuit.getNumbers().keySet()) {
            if (this.myParent.myCircuit.getNumbers().get(obj) == col1) {
                for (String obj2 : this.myParent.myCircuit.getNumbers().keySet()) {
                    if (this.myParent.myCircuit.getNumbers().get(obj2) == col2) {
                        this.myParent.myCircuit.putNumbers(obj, col2);
                        this.myParent.myCircuit.putNumbers(obj2, col1);
                        return;
                    }
                }
            }
        }
        //rows.get(row2)=temp;
        //System.out.println("swap");
        //printMatrix(sparseMatrix);
    }

    public double getMatrixElement(int row, int col) {
        if (rows.get(row).get(col) != cols.get(col).get(row)) {
            //System.out.println("sparse matrix error");
        }
        return rows.get(row).get(col);
    }

    public boolean rowHasVoltagePoint(int rownum) {
        //does this row include an entry for the voltage of a point
        for (Object obj : this.rows.get(rownum).contents.keySet()) {
            Integer colnum = (Integer) obj;
            if (colnum >= firstPointVoltageIndex && colnum < this.myParent.myCircuit.getNumbers().get(LAST)) {
                return true;
            }
        }
        return false;
    }

    public double getDeltaQ(Component electricComponent) {
        for (Object obj : cols.get(this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaQ")).contents.keySet()) {
            Integer i = (Integer) obj;
            if (this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaQ")) == 1.0) {
                //System.out.println(rows[i]);
                if (rows.get(i).contents.size() == 1
                        || (rows.get(i).contents.size() == 2 && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)
                        || (rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                        || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                        && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                    //System.out.println(sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)));
                    if ((rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                            || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                            && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                        //System.out.println("single v point for deltaQ");
                        this.printMatrixRow(this, i);
                    }
                    return this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST));
                }
            }
        }
        //System.out.println("deltaQ error");
        //printMatrix(this);
        return 0;
    }

    public double getDeltaFlux(Component electricComponent) {
        for (Object obj : cols.get(this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "SdeltaVdt")).contents.keySet()) {
            Integer i = (Integer) obj;
            if (this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "SdeltaVdt")) == 1.0) {
                //System.out.println(rows[i]);
                if (rows.get(i).contents.size() == 1
                        || (rows.get(i).contents.size() == 2 && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)
                        || (rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                        || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                        && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                    //System.out.println(sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)));
                    if ((rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                            || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                            && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                        //System.out.println("single v point for deltaflux");
                        this.printMatrixRow(this, i);
                    }
                    return this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST));
                }
            }
        }
        //System.out.println("get delta flux error");
        //printMatrix(this);
        return 0;
    }

    public double getDeltaCurrent(Component electricComponent) {
        for (Object obj : cols.get(this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaI")).contents.keySet()) {
            Integer i = (Integer) obj;
            if (this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaI")) == 1.0) {
                //System.out.println(rows[i]);
                if (rows.get(i).contents.size() == 1
                        || (rows.get(i).contents.size() == 2 && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)
                        || (rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                        || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                        && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                    //System.out.println(sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)));
                    if ((rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                            || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                            && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                        //System.out.println("single v point for delta current");
                        this.printMatrixRow(this, i);
                    }
                    return this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST));
                }
            }
        }
        //System.out.println("get delta current error");
        //printMatrix(this);
        return 0;
    }

    public Double getCurrent(Component electricComponent) {
        for (Object obj : cols.get(this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I")).contents.keySet()) {
            Integer i = (Integer) obj;
            if (this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I")) == 1.0) {
                if (rows.get(i).contents.size() == 1
                        || (rows.get(i).contents.size() == 2 && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)
                        || (rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                        || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                        && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                    if ((rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                            || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                            && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                        //System.out.println("single v point for current");
                        this.printMatrixRow(this, i);
                    }
                    //System.out.println(sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)));
                    return this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST));// /electricComponent.primaryValue;
                }
            }
        }

        //System.out.println("get current error");
        for (Object obj : cols.get(this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I")).contents.keySet()) {
            Integer i = (Integer) obj;
            if (this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I")) == 1.0) {
                this.printMatrixRow(this, i);
            }
        }
        electricComponent.color = Color.RED;
        return 0.0;
    }

    public Double getCurrentPrime(Component electricComponent) {
        for (Object obj : cols.get(this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I'")).contents.keySet()) {
            Integer i = (Integer) obj;
            if (this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I'")) == 1.0) {
                if (rows.get(i).contents.size() == 1
                        || (rows.get(i).contents.size() == 2 && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)
                        || (rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                        || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                        && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                    //System.out.println(sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)));
                    if ((rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                            || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                            && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                        //System.out.println("has v point in getcurrentprime");
                    }
                    return this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST));// /electricComponent.primaryValue;
                }
            }
        }
        //System.out.println("get I' error");
        electricComponent.color = Color.RED;
        return 0.0;
    }

    public String getCurrentString(Component electricComponent, boolean simple) {
        for (Object obj : cols.get(this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I")).contents.keySet()) {
            Integer i = (Integer) obj;
            if (this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I")) == 1.0) {
                if (rows.get(i).contents.size() == 1
                        || (rows.get(i).contents.size() == 2 && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)
                        || (rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                        || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                        && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                    //System.out.println(sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)));
          /*if (this.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST))==0){
                     System.out.println("zero");
                     System.out.println("zero");
                     System.out.println("zero");
                     System.out.println("zero");
                     System.out.println("zero");
                     System.out.println("zero");
                     System.out.println("zero");
                     System.out.println("zero");
                     System.out.println("zero");
                     this.printMatrix(this);
                     }*/
                    if ((rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                            || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                            && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                        //System.out.println("single v point for current string");
                        this.printMatrixRow(this, i);
                    }
                    if (simple) {
                        return "" + Component.formatValue(this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST))) + "A";
                    } else {
                        return "" + this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) + "A";
                    }
                }
            }
        }
        //System.out.println("get current string error");
        return "??? A";
    }

    public double getVoltageDeltaV(Component electricComponent) {
        Integer ecNumber = this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV");
        for (Object obj : cols.get(ecNumber).contents.keySet() ) {
            Integer rowIndex = (Integer) obj;
            if (this.getMatrixElement(rowIndex, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV")) == 1.0) {
                if (rows.get(rowIndex).contents.size() == 1
                        || (rows.get(rowIndex).contents.size() == 2 && this.getMatrixElement(rowIndex, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)
                        || (rows.get(rowIndex).contents.size() == 2 && rowHasVoltagePoint(rowIndex))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                        || (rows.get(rowIndex).contents.size() == 3 && rowHasVoltagePoint(rowIndex)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                        && this.getMatrixElement(rowIndex, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                    //System.out.println(sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)));
                    if ((rows.get(rowIndex).contents.size() == 2 && rowHasVoltagePoint(rowIndex))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                            || (rows.get(rowIndex).contents.size() == 3 && rowHasVoltagePoint(rowIndex)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                            && this.getMatrixElement(rowIndex, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                        //System.out.println("single v point for voltage");
                        this.printMatrixRow(this, rowIndex);
                    }
                    return this.getMatrixElement(rowIndex, this.myParent.myCircuit.getNumbers().get(LAST));
                }
            }
        }
        //System.out.println("get voltage error");
        return 0.0;
    }

    //for voltmeters
    public String getVoltageDeltaVString(Component electricComponent, boolean simple) {
        for (Object obj : cols.get(this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV")).contents.keySet()) {
            Integer rowIndex = (Integer) obj;
            if (this.getMatrixElement(rowIndex, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV")) == 1.0) {
                if (rows.get(rowIndex).contents.size() == 1
                        || (rows.get(rowIndex).contents.size() == 2 && this.getMatrixElement(rowIndex, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)
                        || (rows.get(rowIndex).contents.size() == 2 && rowHasVoltagePoint(rowIndex))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                        || (rows.get(rowIndex).contents.size() == 3 && rowHasVoltagePoint(rowIndex)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                        && this.getMatrixElement(rowIndex, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                    
                    if ((rows.get(rowIndex).contents.size() == 2 && rowHasVoltagePoint(rowIndex))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                            || (rows.get(rowIndex).contents.size() == 3 && rowHasVoltagePoint(rowIndex)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                            && this.getMatrixElement(rowIndex, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                        //System.out.println("single v point in voltage string");
                        this.printMatrixRow(this, rowIndex);
                    }
                    //System.out.println("got voltage");
                    //this.printMatrixRow(this, i);
                    if (simple) {
                        return "" + Component.formatValue(this.getMatrixElement(rowIndex, this.myParent.myCircuit.getNumbers().get(LAST))) + "V";
                    } else {
                        return "" + this.getMatrixElement(rowIndex, this.myParent.myCircuit.getNumbers().get(LAST)) + "V";
                    }

                }
            }
        }
        //System.out.println("get voltage error");
        for (Object obj : cols.get(this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV")).contents.keySet()) {
            Integer i = (Integer) obj;
            if (this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV")) == 1.0) {
                //this.printMatrixRow(this, i);
            }
        }
        //System.out.println("get voltage string error");
        return "??? V";

    }

    public double getResistanceDeltaV(Component electricComponent) {
        for (Object obj : cols.get(this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV")).contents.keySet()) {
            Integer i = (Integer) obj;
            if (this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV")) == 1.0) {
                if (rows.get(i).contents.size() == 1
                        || (rows.get(i).contents.size() == 2 && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)
                        || (rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                        || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                        && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                    //System.out.println(sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)));
                    if ((rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                            || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                            && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                        //System.out.println("single v point in resistance");
                        this.printMatrixRow(this, i);
                    }
                    return this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST));
                }
            }
        }
        //System.out.println("get resistance error");
        return 0.00;
    }

    public String getResistanceDeltaVString(Component electricComponent, boolean simple) {
        for (Object obj : cols.get(this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV")).contents.keySet()) {
            Integer i = (Integer) obj;
            if (this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV")) == 1.0) {
                if (rows.get(i).contents.size() == 1
                        || (rows.get(i).contents.size() == 2 && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)
                        || (rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                        || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                        && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                    //System.out.println(sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)));
                    if ((rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                            || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                            && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                        //System.out.println("single v point in resistance");
                        this.printMatrixRow(this, i);
                    }
                    if (simple) {
                        return "" + Component.formatValue(this.getMatrixElement(i,
                                this.myParent.myCircuit.getNumbers().get(LAST))) + Resistor.ohm;
                    } else {
                        return "" + this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) + Resistor.ohm;
                    }
                }
            }
        }
        //System.out.println("get resistance error");
        return "??? " + Resistor.ohm;
    }

    public double getCapacitance(Component electricComponent) {
        for (Object obj : cols.get(this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I")).contents.keySet()) {
            Integer i = (Integer) obj;
            if (this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I")) == 1.0) {
                if (rows.get(i).contents.size() == 1
                        || (rows.get(i).contents.size() == 2 && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)
                        || (rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                        || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                        && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                    //System.out.println(sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)));
                    if ((rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                            || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                            && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                        //System.out.println("single v point in capacitance");
                        this.printMatrixRow(this, i);
                    }
                    return this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST));
                }
            }
        }
        //System.out.println("get capacitance error");
        return 0.00;
    }

    public String getCapacitanceString(Component electricComponent, boolean simple) {
        for (Object obj : cols.get(this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I")).contents.keySet()) {
            Integer i = (Integer) obj;
            if (this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I")) == 1.0) {
                if (rows.get(i).contents.size() == 1
                        || (rows.get(i).contents.size() == 2 && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)
                        || (rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                        || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                        && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                    //System.out.println(sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)));
                    if ((rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                            || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                            && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                        //System.out.println("single v point in capacitance string");
                        this.printMatrixRow(this, i);
                    }
                    if (simple) {
                        return "" + Component.formatValue(this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST))) + "F";
                    } else {
                        return "" + this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) + "F";
                    }
                }
            }
        }
        //System.out.println("get capacitance string error");
        return "??? F";
    }

    public String getInductanceDeltaVString(Component electricComponent, boolean simple) {
        for (Object obj : cols.get(this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV")).contents.keySet()) {
            Integer i = (Integer) obj;
            if (this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV")) == 1.0) {
                if (rows.get(i).contents.size() == 1
                        || (rows.get(i).contents.size() == 2 && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)
                        || (rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                        || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                        && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                    //System.out.println(sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)));
                    if ((rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                            || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                            && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                        //System.out.println("single v point in inductance string");
                        this.printMatrixRow(this, i);
                    }
                    if (simple) {
                        return "" + Component.formatValue(this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST))) + "H";
                    } else {
                        return "" + this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) + "H";
                    }
                }
            }
        }
        //System.out.println("get inductance string error");
        return "??? H";
    }

    public double getInductanceDeltaV(Component electricComponent) {
        for (Object obj : cols.get(this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV")).contents.keySet()) {
            Integer i = (Integer) obj;
            if (this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV")) == 1.0) {
                if (rows.get(i).contents.size() == 1
                        || (rows.get(i).contents.size() == 2 && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)
                        || (rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                        || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                        && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                    //System.out.println(sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)));
                    if ((rows.get(i).contents.size() == 2 && rowHasVoltagePoint(i))//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0)
                            || (rows.get(i).contents.size() == 3 && rowHasVoltagePoint(i)//sparseMatrix.getElement(i,(Integer)this.myParent.myCircuit.getNumbers().get(LAST)-1)!=0.0
                            && this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST)) != 0.0)) {
                        //System.out.println("single v point in inductance");
                        this.printMatrixRow(this, i);
                    }
                    return this.getMatrixElement(i, this.myParent.myCircuit.getNumbers().get(LAST));
                }
            }
        }
        //System.out.println("get inductance error");
        return 0.00;
    }
    // public double getDeltaV(Component electricComponent){
    //  return 0.0;
    //}

    public void makePivotOne(int row, int col) {
        Set s = new HashSet(rows.get(row).contents.keySet());
        for (Object obj : s) {//for each colnum used in this row
            Integer colNum = (Integer) obj;//was called i before Dec 25 2008
            if (colNum >= col && colNum != this.cols.size() - 1) {//don't care about stuff to left of pivot, it should have already been zeroed out by zerobelow//was colNum<col+1//##changed on Dec 25 2008//##changed to < on Dec 26 20
                if (colNum > col && colNum != this.cols.size() - 1) {
                    //System.out.println("make pivot one misused");
                    this.printMatrixRow(this, row);
                }
                continue;
            }
            //System.out.println(i+" "+col+" "+row);
            //sparseMatrix.putElement(i,col,rows[i].get(col)/rows[row].get(col));
            this.putElement(row, colNum, rows.get(row).get(colNum) / rows.get(row).get(col));//divide each element by pivot
        }
        this.putElement(row, col, 1.0);//put exactly 1 in pivot position
    }

    public void makePivotPositive(int row, int col) {
        Set s = new HashSet(rows.get(row).contents.keySet());
        if (this.getMatrixElement(row, col) < 0) {
            for (Object obj : s) {//for each colnum used in this row
                Integer colNum = (Integer) obj;
                if (colNum < col) {//don't care about stuff to left of pivot, it should have already been zeroed out by zerobelow//was colNum<col+1//##changed on Dec 25 2008//##changed to < on Dec 26 20
                    if (colNum < col) {
                        //System.out.println("make pivot positive misused");
                        this.printMatrixRow(this, row);
                    }
                    continue;
                }
                //System.out.println(i+" "+col+" "+row);
                //sparseMatrix.putElement(i,col,rows[i].get(col)/rows[row].get(col));
                this.putElement(row, colNum, -rows.get(row).get(colNum));//divide each element by pivot
            }
        }
    }

    public void zeroAbove(int row, int col) {
        Set s = new HashSet(cols.get(col).contents.keySet());
        //makePivotPositive(row,col);
        for (Object obj : s) {//for each row
            Integer rowNum = (Integer) obj;//i is rownum
            if (rowNum >= row) {
                continue;
            }
            Set s2 = new HashSet(rows.get(row).contents.keySet());//for each entry in this row
            for (Object obj2 : s2) {
                Integer colNum = (Integer) obj2;//i2 is colnum
                if (colNum == col) {
                    continue;//##removed on Dec 26 2008
                }
                this.putElement(rowNum, colNum, rows.get(rowNum).get(colNum) * rows.get(row).get(col) - rows.get(row).get(colNum) * rows.get(rowNum).get(col));
            }
            this.putElement(rowNum, col, 0.0);
        }
    }

    public void zeroBelow(int row, int col) {//,int numberOfKCLeqs){
        Set s = new HashSet(cols.get(col).contents.keySet());
        //makePivotPositive(row,col);
        for (Object obj : s) {//for each row
            Integer rowNum = (Integer) obj;//i is rownum
            if (rowNum <= row) { //||rowNum>=numberOfKCLeqs){
                continue;
            }//code below only works if pivot is 1.  we need to iterate over both rows
            Set s2 = new HashSet(rows.get(row).contents.keySet());//for each entry in this row
            for (Object obj2 : s2) {
                Integer colNum = (Integer) obj2;//i2 is colnum of entry in orw
                if (colNum == col) {
                    continue;//##removed on Dec 26 2008
                }
                this.putElement(rowNum, colNum, rows.get(rowNum).get(colNum) * rows.get(row).get(col) - rows.get(row).get(colNum) * rows.get(rowNum).get(col));
            }
            this.putElement(rowNum, col, 0.0);
        }
    }
    /*
     * Used for instantaneous charge movement-eg caps discharging/charging.
     */

    public void setUpInstantaneousChargeMovementMatrix(double t) throws Exception {
        int numCols = myParent.myCircuit.electricComponents.size() + myParent.myCircuit.myTerminals.size() + 1;
        this.myParent.myCircuit.setNumbers(new HashMap<String, Integer>());
        Iterator componentIterator;
        componentIterator = myParent.myCircuit.electricComponents.iterator();
        int count = 0;

        while (componentIterator.hasNext()) {

            Component electricComponent = (Component) componentIterator.next();

            if (!deltaQIsZero(electricComponent)) {
                cols.add(new Vector("Component current"));//This component's current
                this.myParent.myCircuit.putNumbers(electricComponent, "deltaQ", count);
                count++;
            }

            if (electricComponent.getClass() == Voltmeter.class || electricComponent.getClass() == Ohmmeter.class) {
                cols.add(new Vector("The voltage difference across this component"));//The voltage difference across this component
                this.myParent.myCircuit.putNumbers(electricComponent, "deltaV", count);
                count++;//we make room for deltav
            }
        }
        firstPointVoltageIndex = count;
        Iterator terminalIterator;
        terminalIterator = myParent.myCircuit.myTerminals.keySet().iterator();

        while (terminalIterator.hasNext()) {
            rows.add(new Vector("Thisterminals kirchoff's current law equation"));//Thisterminals kirchoff's current law equation todo:tk
            cols.add(new Vector("Thisterminals potential"));//Thisterminals potential
            this.myParent.myCircuit.putNumbers(terminalIterator.next(), "V", count);
            count++;
        }
//    sparseMatrix=new SparseMatrix(terminals.size()+electricComponents.size()+numVoltmeters,terminals.size()+electricComponents.size()+numVoltmeters+1);
        this.myParent.myCircuit.putNumbers(LAST, count);
        cols.add(new Vector("V&I"));//Far right col of matrix
        componentIterator = myParent.myCircuit.electricComponents.iterator();
        int rowNum = 0;
        rowNum = setUpInstantaneousChargeMovementEquations(rowNum);
        this.numberOfKCLeqs = rowNum;
        while (componentIterator.hasNext()) {
            Component electricComponent = (Component) componentIterator.next();
            //Batteries have a voltage difference between electrodes
            if (electricComponent.getClass() == VoltageSource.class) {
                if (electricComponent.primaryValue != null) {
                    rows.add(new Vector("Voltage source component's equation"));//This component's equation todo:tk
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), -1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(LAST), electricComponent.primaryValue);
                    rowNum++;
                }
                //Wires have 0 voltage difference between electrodes
            } else if (electricComponent.getClass() == ACVoltageSource.class) {
                if (electricComponent.primaryValue != null && electricComponent.frequency != null
                        && electricComponent.phaseShift != null) {
                    rows.add(new Vector("AC voltage source component's equation"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), -1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(LAST), electricComponent.primaryValue
                            * Math.sin(2 * Math.PI * electricComponent.frequency * (t + electricComponent.phaseShift)));
                    rowNum++;
                }
            } else if (electricComponent.getClass() == Wire.class) {
                rows.add(new Vector("Wire component's equation"));//This component's equation todo:tk
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), -1.0);
                rowNum++;
            } else if (electricComponent.getClass() == CurrentIntegrator.class) {
                rows.add(new Vector("Current integrator component's equation"));//This component's equation todo:tk
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), -1.0);
                rowNum++;
                //Resistors have 0 instantaneous charge movement.
            } else if (electricComponent.getClass() == Switch.class) {

                if (!((Switch) electricComponent).isOpen) {
                    //System.out.println("closed");
                    rows.add(new Vector("Switch component's equation"));//This component's equation todo:tk
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), -1.0);
                    rowNum++;
                } else {
                    //rows.add(new Row());//This component's equation
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"deltaQ"),1.0);
                    //rowNum++;
                }
            } else if (electricComponent.getClass() == Resistor.class) {
                if (electricComponent.primaryValue != null) {
                    //rows.add(new Row());//This component's equation
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"deltaQ"),1);
                    //rowNum++;
                }
                //Inductors have 0 instantaneous charge movement
            } else if (electricComponent.getClass() == Inductor.class) {
                if (electricComponent.primaryValue != null) {
                    //rows.add(new Row());//This component's equation
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"deltaQ"),1);
                    //rowNum++;
                }
                //deltaV across electrodes=charge/capacitance+deltaQ/capacitance
            } else if (electricComponent.getClass() == Capacitor.class) {
                if (electricComponent.primaryValue != null) {
                    rows.add(new Vector("Capacitor component's equation"));//This component's equation todo:tk
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), -1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaQ"),
                            -1 / electricComponent.primaryValue);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(LAST),
                            electricComponent.secondaryValue / electricComponent.primaryValue);
                    rowNum++;
                }
                //0 instantaneous charge movement,voltage difference defined by endpoints
            } else if (electricComponent.getClass() == Voltmeter.class) {
                //rows.add(new Row());//This component's equation
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"deltaQ"),1.0);
                //rowNum++;

                rows.add(new Vector("Voltmeter component's equation"));//This component's equation todo:tk
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), -1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), 1.0);
                rowNum++;
                //System.out.println("voltmeter");
                //Instantaneus charge movement in an ohmmeter is zero, voltage difference defined by endpoints
            } else if (electricComponent.getClass() == VoltageIntegrator.class) {
                //deltaQ=0 is expressed elsewhere
            } else if (electricComponent.getClass() == Ohmmeter.class) {
                //rows.add(new Row());//This component's equation
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"deltaQ"),1.0);
                //rowNum++;

                rows.add(new Vector("Ohmmeter component's equation"));//This component's equation todo:tk
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), -1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), 1.0);
                rowNum++;
            } else if (electricComponent.getClass() == Ammeter.class) {
                rows.add(new Vector("Ammeter component's equation"));//This component's equation todo:tk
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), -1.0);
                rowNum++;
                //No instantaneous charge movement through current source
            } else if (electricComponent.getClass() == CurrentSource.class) {
                if (electricComponent.primaryValue != null) {
                    //rows.add(new Row());//This component's equation
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"deltaQ"),1.0);
                    //rowNum++;
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(LAST),0);//electricComponent.primaryValue);
                }
            }
        }
        //iterateOverGridPoints2(rowNum);
        if (myParent.myDraggedTerminal != null) {

        }
    }

    //Used for instantaneous change in current//magnetic flux
    public void setUpInstantaneousCurrentOrMagneticFluxMatrix(double t) throws Exception {
        int numCols = myParent.myCircuit.electricComponents.size() + myParent.myCircuit.myTerminals.size() + 1;
        this.myParent.myCircuit.setNumbers(new HashMap<String, Integer>());
        Iterator componentIterator;
        componentIterator = myParent.myCircuit.electricComponents.iterator();
        int count = 0;

        while (componentIterator.hasNext()) {

            Component electricComponent = (Component) componentIterator.next();

            if (!currentIsZero(electricComponent)) {
                cols.add(new Vector(electricComponent.primarySymbol + electricComponent.componentUID + " I"));//This component's current
                this.myParent.myCircuit.putNumbers(electricComponent, "I", count);//count~current, count+1~deltacurrent
                count++;
            }

            //if (!deltaCurrentIsZero(electricComponent)){//##removed Dec 24 2008
            //   cols.add(new Row());//This components's change in current
            //   this.myParent.myCircuit.putNumbers(electricComponent+"deltaI",count);
            //  count++;
            //}
            if (electricComponent.getClass() == Inductor.class) {
                cols.add(new Vector("Inductor component deltaI"));//This components's change in current todo:tk
                this.myParent.myCircuit.putNumbers(electricComponent, "deltaI", count);
                count++;
            }
            if (electricComponent.getClass() == Voltmeter.class) {
                //  cols.add(new Row());//The voltage difference across this component
                // this.myParent.myCircuit.putNumbers(electricComponent+"deltaV",count);
                //count+=1;//we make room for deltav
            }
            if (electricComponent.getClass() == VoltageIntegrator.class) {
                cols.add(new Vector("Voltage integrator component SdeltaVdt"));//The voltage difference across this component todo:tk
                this.myParent.myCircuit.putNumbers(electricComponent, "SdeltaVdt", count);
                count += 1;//we make room for SdeltaVdt
            }
            if (electricComponent.getClass() == Ohmmeter.class) {
                //cols.add(new Row());//The voltage difference across this component
                //this.myParent.myCircuit.putNumbers(electricComponent+"deltaV",count);
                //count+=1;//we make room for deltav
            }
        }
        firstPointVoltageIndex = count;
        //Iterator i;
        componentIterator = myParent.myCircuit.myTerminals.keySet().iterator();

        while (componentIterator.hasNext()) {
            Object obj = componentIterator.next();
            rows.add(new Vector("Tie point KCL equation"));//Thisterminals kirchoff's current law equation todo:tk
            //rows.add(new Row());//Thisterminal's kirchoff's "magnetic flux law" equation
            //cols.add(new Row());//Thisterminals potential
            //this.myParent.myCircuit.putNumbers(obj+"V",count);
            cols.add(new Vector("Tie point SVdt"));//Thisterminal's Integral voltage dT
            //this.myParent.myCircuit.putNumbers(i.next(),count);
            //count++;
            this.myParent.myCircuit.putNumbers(obj, "SVdt", count);
            count++;//for SVdt
        }
//    sparseMatrix=new SparseMatrix(terminals.size()+electricComponents.size()+numVoltmeters,terminals.size()+electricComponents.size()+numVoltmeters+1);
        this.myParent.myCircuit.putNumbers(LAST, count);
        cols.add(new Vector("V&I"));//Far right col of matrix
        componentIterator = myParent.myCircuit.electricComponents.iterator();
        int rowNum = 0;
        rowNum = setUpKCLEquations(rowNum);
        this.numberOfKCLeqs = rowNum;
        while (componentIterator.hasNext()) {
            Component electricComponent = (Component) componentIterator.next();
            //Batteries have a voltage difference between electrodes
            if (electricComponent.getClass() == VoltageSource.class) {
                if (electricComponent.primaryValue != null) {
                    //rows.add(new Row());
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.head.getPosition()+"V"),1.0);
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.tail.getPosition()+"V"),-1.0);
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(LAST),electricComponent.primaryValue);
                    //rowNum++;

                    rows.add(new Vector("Voltage source SVdt"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "SVdt"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "SVdt"), -1.0);
                    rowNum++;
                }
                //Wires have 0 voltage difference between electrodes
            } else if (electricComponent.getClass() == ACVoltageSource.class) {
                if (electricComponent.primaryValue != null && electricComponent.frequency != null
                        && electricComponent.phaseShift != null) {
                    rows.add(new Vector("AC voltage source SVdt"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "SVdt"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "SVdt"), -1.0);
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(LAST),0.0);
                    rowNum++;
                }
            } else if (electricComponent.getClass() == ACCurrentSource.class) {
                if (electricComponent.primaryValue != null && electricComponent.frequency != null
                        && electricComponent.phaseShift != null) {
                    rows.add(new Vector("AC current source SVdt"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(LAST), electricComponent.primaryValue
                            * Math.sin(2 * Math.PI * electricComponent.frequency * (t + electricComponent.phaseShift)));
                    rowNum++;
                }
            } else if (electricComponent.getClass() == Wire.class) {
                //rows.add(new Row());
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.head.getPosition()+"V"),1.0);
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.tail.getPosition()+"V"),-1.0);
                //rowNum++;

                rows.add(new Vector("Wire SVdt"));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "SVdt"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "SVdt"), -1.0);
                rowNum++;
            } else if (electricComponent.getClass() == InductanceMeter.class) {
                //rows.add(new Row());
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.head.getPosition()+"V"),1.0);
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.tail.getPosition()+"V"),-1.0);
                //rowNum++;

                rows.add(new Vector("Inductance meter SVdt"));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "SVdt"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "SVdt"), -1.0);
                rowNum++;
            } else if (electricComponent.getClass() == CurrentIntegrator.class) {
                //rows.add(new Row());
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.head.getPosition()+"V"),1.0);
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.tail.getPosition()+"V"),-1.0);
                //rowNum++;

                rows.add(new Vector("Current integrator SVdt"));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "SVdt"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "SVdt"), -1.0);
                rowNum++;
            } else if (electricComponent.getClass() == Switch.class) {

                if (!((Switch) electricComponent).isOpen) {
                    //rows.add(new Row());
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.head.getPosition()+"V"),1.0);
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.tail.getPosition()+"V"),-1.0);
                    //rowNum++;

                    rows.add(new Vector("Switch SVdt"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "SVdt"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "SVdt"), -1.0);
                    rowNum++;
                } else {
            //rows.add(new Row());
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"I"),1.0);
                    //rowNum++;

                    //rows.add(new Row());
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"deltaI"),1.0);
                    //rowNum++;
                }
            } else if (electricComponent.getClass() == Resistor.class) {
                if (electricComponent.primaryValue != null) {
                    //rows.add(new Row());
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.head.getPosition()+"V"),1.0);
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.tail.getPosition()+"V"),-1.0);
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"I"),
                    //               -electricComponent.primaryValue);
                    //rowNum++;

                    rows.add(new Vector("Resistor SVdt"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "SVdt"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "SVdt"), -1.0);
                    rowNum++;
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent)+1,-electricComponent.primaryValue);removed on 11/30/08
                }
                //Inew = Iold+deltaI#1/L*deltaflux
            } else if (electricComponent.getClass() == Inductor.class) {
                if (electricComponent.primaryValue != null) {
                    rows.add(new Vector("Inductor SVdt and deltaI"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "SVdt"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "SVdt"), -1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaI"), -electricComponent.primaryValue);
                    rowNum++;

                    rows.add(new Vector("Inductor I and deltaI"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I"), 1);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaI"), -1);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(LAST), electricComponent.secondaryValue / electricComponent.primaryValue);
                    rowNum++;
                }
                //0 instantaneous flux change,V=1/C*Q
            } else if (electricComponent.getClass() == Capacitor.class) {
                if (electricComponent.primaryValue != null) {
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent)+1,1);
                    //rowNum++;
                    //rows.add(new Row());
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.head.getPosition()+"V"),1.0);
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.tail.getPosition()+"V"),-1.0);
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(LAST),
                    //                      electricComponent.secondaryValue/electricComponent.primaryValue); 
                    //rowNum++;

                    rows.add(new Vector("Capacitor SVdt"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "SVdt"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "SVdt"), -1.0);
                    rowNum++;
                }
                //0 current,0 instantaneous flux change,voltage difference defined by endpoints
            } else if (electricComponent.getClass() == Voltmeter.class) {
          //rows.add(new Row());
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"I"),1.0);//current
                //rowNum++;
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"deltaI"),1.0);//deltaI#I'#flux movement
                //rowNum++;

                //rows.add(new Row());
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"deltaV"),1.0);//v diff across electrodes
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.head.getPosition()+"V"),-1.0);
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.tail.getPosition()+"V"),1.0);
                //rowNum++;
                //System.out.println("voltmeter");
                //voltage difference defined by endpoints,
            } else if (electricComponent.getClass() == VoltageIntegrator.class) {
                //rows.add(new Row());
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"I"),1.0);//current
                //rowNum++;
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"deltaI"),1.0);//deltaI#I'#flux movement
                //rowNum++;

                rows.add(new Vector("Voltage integrator SdeltaVdt and SVdt"));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "SdeltaVdt"), 1.0);//v diff across electrodes
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "SVdt"), -1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "SVdt"), 1.0);
                rowNum++;
                //System.out.println("voltage integrator");
                //voltage difference defined by endpoints,
            } else if (electricComponent.getClass() == Ohmmeter.class) {
                rows.add(new Vector("Ohmmeter I"));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I"), 1.0);//current
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(LAST), 1);
                rowNum++;

                //rows.add(new Row());
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"deltaV"),1.0);//v diff across electrodes
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.head.getPosition()+"V"),-1.0);
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.tail.getPosition()+"V"),1.0);
                //rowNum++;
            } else if (electricComponent.getClass() == Ammeter.class) {
                //rows.add(new Row());
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.head.getPosition()+"V"),1.0);
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent.tail.getPosition()+"V"),-1.0);
                //rowNum++;

                rows.add(new Vector("Ammeter SVdt"));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "SVdt"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "SVdt"), -1.0);
                rowNum++;
                //
            } else if (electricComponent.getClass() == CurrentSource.class) {
                if (electricComponent.primaryValue != null) {
                    rows.add(new Vector("Current source I"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(LAST), electricComponent.primaryValue);
                    rowNum++;
                }
            }
        }
        //iterateOverGridPoints3(rowNum);
        if (myParent.myDraggedTerminal != null) {

        }
    }

    //Sets up current equations (Kirchoff's current law) todo:tk
    public int setUpKCLEquations(int rowNum) {
        Iterator i = myParent.myCircuit.myTerminals.values().iterator();
        while (i.hasNext()) {
            Terminal terminal = (Terminal) i.next();
            Iterator i2 = terminal.in.iterator();
            double c = 1;
            while (i2.hasNext()) {
                Component electricComponent = (Component) i2.next();
                if (!currentIsZero(electricComponent)) {
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I"), -1);
                }
                // this.putElement(rowNum+1, (Integer)this.myParent.myCircuit.getNumbers().get(electricComponent)+1, -1);
            }
            if (myParent.myDraggedTerminal != null
                    && myParent.myDraggedTerminal.getPosition().equals(terminal.getPosition())
                    && myParent.myDraggedTerminal != terminal) {
                //System.out.println("draggedtiepoint");
                for (Component ec : myParent.myDraggedTerminal.in) {
                    if (!currentIsZero(ec)) {
                        this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(ec.getID() + "I"), -1);
                    }
                    //this.putElement(rowNum+1, (Integer)this.myParent.myCircuit.getNumbers().get(ec)+1, -1);
                }
                for (Component ec : myParent.myDraggedTerminal.out) {
                    if (!currentIsZero(ec)) {
                        this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(ec.getID() + "I"), 1);
                    }
                    //this.putElement(rowNum+1, (Integer)this.myParent.myCircuit.getNumbers().get(ec)+1, 1);
                }
            }
            i2 = terminal.out.iterator();
            while (i2.hasNext()) {
                Component electricComponent = (Component) i2.next();
                if (!currentIsZero(electricComponent)) {
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I"), 1);
                }
                //this.putElement(rowNum+1, (Integer)this.myParent.myCircuit.getNumbers().get(electricComponent)+1, 1);
            }
            rowNum++;
            //rowNum++;
        }
        return rowNum;
        //System.out.println();
    }

    //Sets up instantaneous charge movement equations (Kirchoff's current law) todo:tk
    public int setUpInstantaneousChargeMovementEquations(int rowNum) {
        Iterator terminalsIterator = myParent.myCircuit.myTerminals.values().iterator();

        while (terminalsIterator.hasNext()) {
            Terminal terminal = (Terminal) terminalsIterator.next();
            Iterator componentsInIterator = terminal.in.iterator();
            double c = 1;
            while (componentsInIterator.hasNext()) {
                Component electricComponentIn = (Component) componentsInIterator.next();
                // if (electricComponent.getClass()==Capacitor.class||electricComponent.getClass()==Wire.class||
                //    electricComponent.getClass()==Battery.class||electricComponent.getClass()==Ammeter.class||
                //   (electricComponent.getClass()==Switch.class&&!((Switch)electricComponent).isOpen)
                // // ||electricComponent.getClass()==Ohmmeter.class
                // ){
                if (!deltaQIsZero(electricComponentIn)) {
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponentIn.getID() + "deltaQ"), -1);//row.put((Integer)this.myParent.myCircuit.getNumbers().get(electricComponent),1.0);
                }
                //}
            }
            if (myParent.myDraggedTerminal != null
                    && myParent.myDraggedTerminal.getPosition().equals(terminal.getPosition())
                    && myParent.myDraggedTerminal != terminal) {
                //System.out.println("draggedtiepoint");
                for (Component ec : myParent.myDraggedTerminal.in) {
                    //if (ec.getClass()==Capacitor.class||ec.getClass()==Wire.class||
                    //    ec.getClass()==Battery.class||ec.getClass()==Ammeter.class||(ec.getClass()==Switch.class&&!((Switch)ec).isOpen)
                    //  //ec.getClass()==Ohmmeter.class
                    //    ){
                    if (!deltaQIsZero(ec)) {
                        this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(ec.getID() + "deltaQ"), -1);
                    }
                    //}
                }
                for (Component ec : myParent.myDraggedTerminal.out) {
                    //if (ec.getClass()==Capacitor.class||ec.getClass()==Wire.class||
                    //   ec.getClass()==Battery.class||ec.getClass()==Ammeter.class||(ec.getClass()==Switch.class&&!((Switch)ec).isOpen)
                    //   //ec.getClass()==Ohmmeter.class)
                    //   ){
                    if (!deltaQIsZero(ec)) {
                        this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(ec.getID() + "deltaQ"), 1);
                    }
                    // }
                }
            }
            Iterator componentsOutIterator = terminal.out.iterator();
            while (componentsOutIterator.hasNext()) {
                Component electricComponentOut = (Component) componentsOutIterator.next();
                // if (electricComponent.getClass()==Capacitor.class||electricComponent.getClass()==Wire.class||
                //     electricComponent.getClass()==Battery.class||electricComponent.getClass()==Ammeter.class||
                //     (electricComponent.getClass()==Switch.class&&!((Switch)electricComponent).isOpen)
                //     //electricComponent.getClass()==Ohmmeter.class){
                // ){
                //ElectricComponent electricComponent=(Component)i2.next();
                if (!deltaQIsZero(electricComponentOut)) {
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponentOut.getID() + "deltaQ"), 1);//row.put((Integer)this.myParent.myCircuit.getNumbers().get(electricComponent),-1.0);
                }
                // }
            }
            rowNum++;
        }
        return rowNum;
        //System.out.println();
    }

    public boolean currentIsZero(Component electricComponent) {
        if (electricComponent.getClass() == Voltmeter.class
                || electricComponent.getClass() == VoltageIntegrator.class
                || (electricComponent.getClass() == Switch.class
                && ((Switch) electricComponent).isOpen)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean deltaQIsZero(Component electricComponent) {
        return currentPrimeIsZero(electricComponent) || electricComponent.getClass() == Resistor.class
                || electricComponent.getClass() == Inductor.class || electricComponent.getClass() == CapacitanceMeter.class
                || electricComponent.getClass() == ACCurrentSource.class;
    }

    public boolean currentPrimeIsZero(Component electricComponent) {
        return currentIsZero(electricComponent) || electricComponent.getClass() == CurrentSource.class
                || electricComponent.getClass() == Ohmmeter.class;
    }

    public boolean deltaCurrentIsZero(Component ec) {//not used, only inductors have deltaI
        return currentIsZero(ec) || ec.getClass() == CurrentSource.class || ec.getClass() == Ohmmeter.class;
    }

    public int setUpKCLEquations2(int rowNum) {
        
        Iterator terminalIterator = myParent.myCircuit.myTerminals.values().iterator();
        
        while (terminalIterator.hasNext()) {
            
            Terminal terminal = (Terminal) terminalIterator.next();
            
            rows.add(new Vector("T" + terminal.terminalNumber + " I"));//Thisterminal's I equation todo:tk
            rows.add(new Vector("T" + terminal.terminalNumber + " I'"));//Thisterminal's I' equation todo:tk
            

            
            Iterator terminalInIterator = terminal.in.iterator();
            while (terminalInIterator.hasNext()) {
                Component electricComponent = (Component) terminalInIterator.next();
                
                if (!currentIsZero(electricComponent)) {
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I"), -1);//row.put((Integer)this.myParent.myCircuit.getNumbers().get(electricComponent),1.0);  
                }
                
                if (!currentPrimeIsZero(electricComponent)) {
                    this.putElement(rowNum + 1, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I'"), -1);
                }
            }
            
            Iterator terminalOutIterator = terminal.out.iterator();
            while (terminalOutIterator.hasNext()) {
                Component electricComponent = (Component) terminalOutIterator.next();
                //ElectricComponent electricComponent=(Component)i2.next();
                if (!currentIsZero(electricComponent)) {
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I"), 1);//row.put((Integer)this.myParent.myCircuit.getNumbers().get(electricComponent),-1.0);
                }
                if (!currentPrimeIsZero(electricComponent)) {
                    this.putElement(rowNum + 1, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I'"), 1);
                }
            }
            
            if (myParent.myDraggedTerminal != null
                    && myParent.myDraggedTerminal.getPosition().equals(terminal.getPosition())
                    && myParent.myDraggedTerminal != terminal) {
                
                for (Component ec : myParent.myDraggedTerminal.in) {
                    if (!currentIsZero(ec)) {
                        this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(ec.getID() + "I"), -1);
                    }
                    if (!currentPrimeIsZero(ec)) {
                        this.putElement(rowNum + 1, this.myParent.myCircuit.getNumbers().get(ec.getID() + "I'"), -1);
                    }
                }
                
                for (Component ec : myParent.myDraggedTerminal.out) {
                    if (!currentIsZero(ec)) {
                        this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(ec.getID() + "I"), 1);
                    }
                    if (!currentPrimeIsZero(ec)) {
                        this.putElement(rowNum + 1, this.myParent.myCircuit.getNumbers().get(ec.getID() + "I'"), 1);
                    }
                }
            }
            rowNum++;
            rowNum++;
        }
        return rowNum;
    }

    public void setUpMainMatrix(double t) throws Exception {
        int numCols = myParent.myCircuit.electricComponents.size() + myParent.myCircuit.myTerminals.size() + 1;
        
        this.myParent.myCircuit.setNumbers(new HashMap());
        
        Iterator componentIterator;
        
        componentIterator = myParent.myCircuit.electricComponents.iterator();
        
        int count = 0;
        
        while (componentIterator.hasNext()) {

            Component electricComponent = (Component) componentIterator.next();

            if (!currentIsZero(electricComponent)) {
                cols.add(new Vector(electricComponent.primarySymbol + electricComponent.componentUID + " I"));//This component's current todo:tk
                this.myParent.myCircuit.putNumbers(electricComponent, "I", count);
                count++;
            }
            
            if (!currentPrimeIsZero(electricComponent)) {
                cols.add(new Vector(electricComponent.primarySymbol + electricComponent.componentUID + " I'"));//This component's I' todo:tk
                this.myParent.myCircuit.putNumbers(electricComponent, "I'", count);
                count++;
            }

            if (electricComponent.getClass() == Voltmeter.class 
                    || electricComponent.getClass() == Ohmmeter.class
                    || electricComponent.getClass() == InductanceMeter.class
                    || electricComponent.getClass() == VoltageIntegrator.class
                    || electricComponent.getClass() == ACVoltageSource.class) {
                cols.add(new Vector("Ohmmeter, InductanceMeter, VoltageIntegrator, or ACVoltageSource deltaV"));//This component's voltage difference todo:tk
                this.myParent.myCircuit.putNumbers(electricComponent, "deltaV", count);
                count++;//we make room for deltav
            }
        }
        
        firstPointVoltageIndex = count;
        
        
        Iterator<Point> terminalIterator = myParent.myCircuit.myTerminals.keySet().iterator();

        while (terminalIterator.hasNext()) {
            Point point = terminalIterator.next();
            //rows.add(new Row());//Thisterminal's I equation
            cols.add(new Vector("T" + myParent.myCircuit.myTerminals.get(point).terminalNumber + " V"));//Thisterminal's voltage todo:tk
            //rows.add(new Row());//Thisterminal's I' equation
            this.myParent.myCircuit.putNumbers(point, "V", count);
            count++;
            //count++;
            cols.add(new Vector("T" + myParent.myCircuit.myTerminals.get(point).terminalNumber + " V'"));//Thisterminals's dV/dt todo:tk
            this.myParent.myCircuit.putNumbers(point, "V'", count);
            //this.myParent.myCircuit.putNumbers(i.next(),count);
            count++;
        }

//  sparseMatrix=new SparseMatrix(terminals.size()+electricComponents.size()+numVoltmeters,terminals.size()+electricComponents.size()+numVoltmeters+1);
        this.myParent.myCircuit.putNumbers(LAST, count);
        
        
        cols.add(new Vector("V&I"));//Far right column of the matrix todo:tk
        
        
        int rowNum = 0;
        rowNum = setUpKCLEquations2(rowNum);
        this.numberOfKCLeqs = rowNum;
        
        Iterator componentIterator2 = myParent.myCircuit.electricComponents.iterator();

        while (componentIterator2.hasNext()) {
            Component electricComponent = (Component) componentIterator2.next();
            if (electricComponent.getClass() == VoltageSource.class) {
                if (electricComponent.primaryValue != null) {
                    rows.add(new Vector("VoltageSource " + electricComponent.getID() + " V"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), -1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(LAST), electricComponent.primaryValue);
                    rowNum++;

                    rows.add(new Vector("VoltageSource " + electricComponent.getID() + " V'"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V'"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V'"), -1.0);
                    rowNum++;
                }
            } else if (electricComponent.getClass() == ACVoltageSource.class) {
                if (electricComponent.primaryValue != null && electricComponent.frequency != null
                        && electricComponent.phaseShift != null) {
                    rows.add(new Vector("AC voltage source V"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), -1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(LAST), electricComponent.primaryValue
                            * Math.sin(2 * Math.PI * electricComponent.frequency * (t + electricComponent.phaseShift)));
                    rowNum++;

                    rows.add(new Vector("AC voltage source V'"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V'"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V'"), -1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(LAST),
                            2 * Math.PI * electricComponent.frequency * electricComponent.primaryValue
                            * Math.cos(2 * Math.PI * electricComponent.frequency * (t + electricComponent.phaseShift)));
                    rowNum++;

                    rows.add(new Vector("AC voltage source deltaV and V"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), -1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), 1.0);
                    rowNum++;
                }
            } else if (electricComponent.getClass() == ACCurrentSource.class) {
                if (electricComponent.primaryValue != null && electricComponent.frequency != null
                        && electricComponent.phaseShift != null) {
                    rows.add(new Vector("AC current source I"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(LAST), electricComponent.primaryValue
                            * Math.sin(2 * Math.PI * electricComponent.frequency * (t + electricComponent.phaseShift)));
                    rowNum++;

                    rows.add(new Vector("AC current source I'"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I'"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(LAST),
                            2 * Math.PI * electricComponent.frequency * electricComponent.primaryValue
                            * Math.cos(2 * Math.PI * electricComponent.frequency * (t + electricComponent.phaseShift)));
                    rowNum++;
                }
            } else if (electricComponent.getClass() == Wire.class) {
                rows.add(new Vector("Wire " + electricComponent.getID() + " V"));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), -1.0);
                rowNum++;

                rows.add(new Vector("Wire " + electricComponent.getID() + " V'"));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V'"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V'"), -1.0);
                rowNum++;
            } else if (electricComponent.getClass() == CurrentIntegrator.class) {
                rows.add(new Vector("Current integrator V"));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), -1.0);
                rowNum++;

                rows.add(new Vector("Current integrator V'"));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V'"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V'"), -1.0);
                rowNum++;
            } else if (electricComponent.getClass() == Switch.class) {

                if (!((Switch) electricComponent).isOpen) {
                    rows.add(new Vector("Switch V"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), -1.0);
                    rowNum++;

                    rows.add(new Vector("Switch V'"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V'"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V'"), -1.0);
                    rowNum++;
                } else {
            //rows.add(new Row());
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"I"),1.0);
                    //rowNum++;

                    //rows.add(new Row());
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"I'"),1.0);
                    //rowNum++;
                }
            } else if (electricComponent.getClass() == Resistor.class) {
                if (electricComponent.primaryValue != null) {
                    rows.add(new Vector("Resistor " + electricComponent.getID() + " V"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), -1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I"), -electricComponent.primaryValue);
                    rowNum++;

                    rows.add(new Vector("Resistor " + electricComponent.getID() + " V'"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V'"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V'"), -1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I'"), -electricComponent.primaryValue);
                    rowNum++;
                }
            } else if (electricComponent.getClass() == Capacitor.class) {
                if (electricComponent.primaryValue != null) {
                    //System.out.println(electricComponent.isConstant);
                    //System.out.println("heldconst: "+electricComponent.isHeldConstant);
                    if (!electricComponent.isHeldConstant) {
                        rows.add(new Vector("Capacitor " + electricComponent.getID() + " V"));
                        this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), 1.0);
                        this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), -1.0);
                        //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent),-h/electricComponent.primaryValue);//removed this line 11/12/08,put it back11/16/08
                        this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(LAST),
                                electricComponent.secondaryValue / electricComponent.primaryValue);
                        rowNum++;
                    }

                    rows.add(new Vector("Capacitor " + electricComponent.getID() + " V' and I"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V'"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V'"), -1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I"), -1 / electricComponent.primaryValue);//was I' dec 5 08
                    rowNum++;
                }
            } else if (electricComponent.getClass() == Inductor.class) {
                if (electricComponent.primaryValue != null) {
                    rows.add(new Vector("Inductor " + electricComponent.getID() + " V and I'"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), -1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I'"), -electricComponent.primaryValue);
                    rowNum++;

                    rows.add(new Vector("Inductor " + electricComponent.getID() + " I"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I"), 1);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(LAST), electricComponent.secondaryValue / electricComponent.primaryValue);
                    rowNum++;
                }
            } else if (electricComponent.getClass() == Voltmeter.class) {
          //rows.add(new Row());
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"I"),1.0);
                //rowNum++;

                //rows.add(new Row());
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"I'"),1.0);
                //rowNum++;
                rows.add(new Vector("Voltmeter deltaV and V"));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), -1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), 1.0);
                rowNum++;
                //System.out.println("voltmeter");
            } else if (electricComponent.getClass() == VoltageIntegrator.class) {
                //I=0 and I'=0 are expressed elsewhere
                rows.add(new Vector("Voltage integrator deltaV and V"));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV"), 1.0);//deltaV is integrated to get magnetic flux
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), -1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), 1.0);
                rowNum++;
            } else if (electricComponent.getClass() == Ohmmeter.class) {
                rows.add(new Vector("Ohmmeter I."));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(LAST), 1);
                rowNum++;

                //rows.add(new Row());
                //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"I'"),1.0);
                //rowNum++;
                rows.add(new Vector("Ohmmeter deltaV and V"));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), -1.0);
                rowNum++;
            } else if (electricComponent.getClass() == Ammeter.class) {
                rows.add(new Vector("Ammeter V"));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), -1.0);
                rowNum++;

                rows.add(new Vector("Ammeter V'"));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V'"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V'"), -1.0);
                rowNum++;
            } else if (electricComponent.getClass() == CurrentSource.class) {
                if (electricComponent.primaryValue != null) {
                    //rows.add(new Row());
                    //this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent+"I'"),1.0);
                    //rowNum++;

                    rows.add(new Vector("Current source I"));
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I"), 1.0);
                    this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(LAST), electricComponent.primaryValue);
                    rowNum++;
                }
            } else if (electricComponent.getClass() == CapacitanceMeter.class) {
                rows.add(new Vector("Capacitance meter V'"));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V'"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V'"), -1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(LAST), -1);
                rowNum++;
            } else if (electricComponent.getClass() == InductanceMeter.class) {
                rows.add(new Vector("Inductance meter I'"));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "I'"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(LAST), -1);
                rowNum++;

                rows.add(new Vector("Inductance meter deltaV and V"));
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.getID() + "deltaV"), 1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.headTerminal.getPosition() + "V"), -1.0);
                this.putElement(rowNum, this.myParent.myCircuit.getNumbers().get(electricComponent.tailTerminal.getPosition() + "V"), 1.0);
                rowNum++;
            }
        }
        //iterateOverGridPoints(rowNum);//moved to top of method on dec 10 08
    /*i=myParent.myGridPoints.values().iterator();
         while (i.hasNext()){
         Terminal terminal=(Terminal)i.next();
         Iterator i2=terminal.in.iterator();
         while(i2.hasNext()){
         Component electricComponent=(Component)i2.next();
         this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent),1.0);//row.put((Integer)this.myParent.myCircuit.getNumbers().get(electricComponent),1.0);
         }
         i2=terminal.out.iterator();
         while(i2.hasNext()){
         Component electricComponent=(Component)i2.next();
         this.putElement(rowNum,(Integer)this.myParent.myCircuit.getNumbers().get(electricComponent),-1.0);//row.put((Integer)this.myParent.myCircuit.getNumbers().get(electricComponent),-1.0);
         }
         rowNum++;
         }*/
    }

    public void reduceKCLeqs(SparseMatrix sparseMatrix) {
        int pivotCol = 0;
        int pivotRow = 0;
        pivotCol = sparseMatrix.cols.size() - 2;
        whilelabel:
        while (pivotCol >= 0 && pivotRow < sparseMatrix.numberOfKCLeqs) {
            //while(pivotCol<sparseMatrix.cols.size()-1&&pivotRow<sparseMatrix.numberOfKCLeqs){//Nov 11 08: -1 to sparseMatrix.rows.length//Nov12 08 removed -1 associated with rows
            //i is what col we are on

            if (sparseMatrix.cols.get(pivotCol).contents.size() > 0) {
                Set s = new HashSet(sparseMatrix.cols.get(pivotCol).contents.keySet());
                double maxSize = 0;
                //int row=-1;
                for (Object obj : s) {//for each element in this col
                    Integer index = (Integer) obj;
                    if (index >= pivotRow && index < sparseMatrix.numberOfKCLeqs) {//we are not considering rows above the pivot row, or outside of the KCL eqs
                        if (index != pivotRow) {//don't swap a row with itself
                            sparseMatrix.swapRows(pivotRow, index);
                        }//this brace-see below
                        if (rows.get(pivotRow).get(pivotCol) != 1.0) {
                            makePivotOne(pivotRow, pivotCol);
                        }
                        sparseMatrix.zeroBelow(pivotRow, pivotCol);//,numberOfKCLeqs);
                        sparseMatrix.zeroAbove(pivotRow, pivotCol);

                        pivotRow++;
                        pivotCol--;//++;
                        continue whilelabel;
                        //was here-see above  //##changed Dec 26 2008
                    }
                }
            }
            pivotCol--;//++;
        }
    }

    public void rowEchelonFormMatrix(SparseMatrix sparseMatrix) {
        reduceKCLeqs(sparseMatrix);
        //System.out.println("after reduce kcl eqs:");
        //this.printMatrix(sparseMatrix);
        int pivotCol = 0;
        int pivotRow = 0;
        //System.out.print(" "+pivotRow+" "+pivotCol);
        //printMatrix(sparseMatrix);
  /* for (int rowNum=0;rowNum<sparseMatrix.rows.size();rowNum++){
         if (sparseMatrix.rows.get(rowNum).contents.size()==1&&sparseMatrix.get(rowNum).get((Integer)this.myParent.myCircuit.getNumbers().get(LAST))==0.0){
         //System.out.println(this.myParent.myCircuit.getNumbers().get(LAST));
         //sparseMatrix.get(rowNum).get((Integer)this.myParent.myCircuit.getNumbers().get("LAST"));
         int colNum=0;
         Set s=new HashSet(sparseMatrix.rows.get(rowNum).contents.keySet());
         for(Object obj:s){//for each element in this row
         colNum = (Integer)obj;
         break;
         }
         zeroBelow(rowNum,colNum);
         zeroAbove(rowNum,colNum);
         }
         }*/
        pivotCol = sparseMatrix.cols.size() - 2;
        whilelabel:
        while (pivotCol >= 0 && pivotRow < sparseMatrix.rows.size()) {
            //while(pivotCol<sparseMatrix.cols.size()-1&&pivotRow<sparseMatrix.rows.size()){//Nov 11 08: -1 to sparseMatrix.rows.length//Nov12 08 removed -1 associated with rows
            //i is what col we are on

            if (sparseMatrix.cols.get(pivotCol).contents.size() > 0) {//if this col has stuff in it
                Set s = new HashSet(sparseMatrix.cols.get(pivotCol).contents.keySet());
                double scaledMaxSizeInCol = 0;
                int maxRow = -1;
                for (Object obj : s) {//itereate over elements in this col to find the biggest element in this col
                    Integer rowNum = (Integer) obj;
                    Set s2 = new HashSet(sparseMatrix.rows.get(rowNum).contents.keySet());
                    double maxSizeInRow = 0;//we know this row isn't empty-this will become nonzero
                    for (Object obj2 : s2) {//iterate over elements in this row to find the biggest element in this row
                        Integer colNum = (Integer) obj2;//was obj //##changed Dec 26 2008
                        if (Math.abs(sparseMatrix.getMatrixElement(rowNum, colNum)) > maxSizeInRow) {
                            maxSizeInRow = Math.abs(sparseMatrix.getMatrixElement(rowNum, colNum));
                        }
                    }
                    if (rowNum >= pivotRow && Math.abs(sparseMatrix.getMatrixElement(rowNum, pivotCol) / maxSizeInRow) > scaledMaxSizeInCol) {//we are not considering rows above the pivot row
                        scaledMaxSizeInCol = Math.abs(sparseMatrix.getMatrixElement(rowNum, pivotCol) / maxSizeInRow);
                        maxRow = rowNum;
                    }
                }
                if (maxRow == -1) {
                    //if execution reaches this point, then all the elements in this col are above the pivot row and we can go to the next col
                    pivotCol--;//++;
                    continue whilelabel;
                }
                if (maxRow != pivotRow) {//don't swap a row with itself
                    sparseMatrix.swapRows(pivotRow, maxRow);
                    //System.out.println("swapped rows "+pivotRow+" and "+maxRow+":");
                    //this.printMatrix(sparseMatrix);
                }

                /*Set s2=new HashSet(sparseMatrix.rows.get(maxRow).contents.keySet());
                 double maxSizeInRow=0;//we know this row isn't empty-this will become nonzero
                 for(Object obj2:s2){//iterate over elements in this row to find the biggest element in this row
                 Integer colNum = (Integer)obj;
                 if (Math.abs(sparseMatrix.getElement(maxRow, colNum))>maxSizeInRow){
                 maxSizeInRow=Math.abs(sparseMatrix.getElement(maxRow, colNum));
                 }
                 }*/
                if (rows.get(pivotRow).get(pivotCol) != 1.0) {//make sure we have a 1 in the pivot position before moving on
                    makePivotOne(pivotRow, pivotCol);
                    //System.out.println("made pivot one at "+pivotRow+", "+pivotCol+":");
                    //this.printMatrix(sparseMatrix);
                }
                sparseMatrix.zeroBelow(pivotRow, pivotCol);//,sparseMatrix.rows.size());
                //System.out.println("zeroed below "+pivotRow+", "+pivotCol+":");
                //this.printMatrix(sparseMatrix);
                sparseMatrix.zeroAbove(pivotRow, pivotCol);
                //System.out.println("zeroed above "+pivotRow+", "+pivotCol+":");
                //this.printMatrix(sparseMatrix);

                pivotRow++;
                pivotCol--;//++;
            } else {//this col is empty
                pivotCol--;//++;
            }

            /*double maxSize=0;
             int maxRow=-1;
             int maxCol=-1;
             for (int rowNum=pivotRow;rowNum<sparseMatrix.rows.size();rowNum++){//for each row
             Set s=new HashSet(sparseMatrix.rows.get(rowNum).contents.keySet());//the elements in this row
             for(Object obj:s){//for each element in this row
             Integer colNum = (Integer)obj;
             if (colNum>=pivotCol&&colNum<sparseMatrix.cols.size()-1&&Math.abs(sparseMatrix.getElement(rowNum, colNum))>maxSize){
             maxSize=Math.abs(sparseMatrix.getElement(rowNum, colNum));
             maxRow=rowNum;
             maxCol=colNum;
             }
             }
             }
             if (maxSize==0){
             return;//We're done
             }
             if (pivotCol!=maxCol){
             sparseMatrix.swapCols(pivotCol,maxCol);
             }
             if (pivotRow!=maxRow){
             sparseMatrix.swapRows(pivotRow,maxRow);
             }
             sparseMatrix.zeroBelow(pivotRow,pivotCol);
             pivotRow++;
             pivotCol++;*/
            /*   if(sparseMatrix.cols.get(pivotCol).contents.values().contains(1.0)){
             Set s=new HashSet(sparseMatrix.cols.get(pivotCol).contents.keySet());
             for(Object obj:s){//for each element in this col
             Integer index=(Integer)obj;
             if (index<pivotRow){
             continue;//we are not considering rows above the pivot row
             }
             if ((double)sparseMatrix.cols.get(pivotCol).get(index)==1.0){
             if (index!=pivotRow){
             sparseMatrix.swap(pivotRow,index);
             }
             sparseMatrix.zeroBelow(pivotRow,pivotCol);
             pivotRow++;
             pivotCol++;
             continue whilelabel;
             }
             //}
             }//end for
            
             }
             if (sparseMatrix.cols.get(pivotCol).contents.values().contains(-1.0)){
             //System.out.println("b");
             if (sparseMatrix.cols.get(pivotCol).get(pivotRow)==-1.0){
             /*Set s2 = new HashSet(rows[pivotRow].contents.keySet());//for each entry in this row
             for(Object obj2:s2){
             Integer i2=(Integer)obj2;//i2 is colnum
             this.putElement(pivotRow,i2,-rows[pivotRow].get(i2));
             }
             sparseMatrix.zeroBelow(pivotRow,pivotCol);
             pivotRow++;
             pivotCol++;
             continue whilelabel;
             }
             Set s=new HashSet(sparseMatrix.cols.get(pivotCol).contents.keySet());
             for (Object obj:s){
             Integer index=(Integer)obj;
             if (index<pivotRow){
             continue;
             }
             if ((double)sparseMatrix.cols.get(pivotCol).get(index)==-1.0){
             if (index!=pivotRow){
             sparseMatrix.swap(pivotRow,index);
             }
             sparseMatrix.zeroBelow(pivotRow,pivotCol);
             pivotRow++;
             pivotCol++;
             continue whilelabel;
             }
             }
             }
             if (sparseMatrix.cols.get(pivotCol).contents.size()>0){//nov 9 2007 added .values()
             //System.out.println("c");
             if (sparseMatrix.cols.get(pivotCol).get(pivotRow)!=0.0){
             sparseMatrix.zeroBelow(pivotRow,pivotCol);
             pivotRow++;
             pivotCol++;
             continue whilelabel;
             }
             Set s=new HashSet(sparseMatrix.cols.get(pivotCol).contents.keySet());
             for (Object obj:s){
             Integer index=(Integer)obj;
             if (index<pivotRow){
             continue;
             }
             //System.out.println(sparseMatrix.cols[pivotCol].get(index)!=0.0);
             if (index!=pivotRow&&sparseMatrix.cols.get(pivotCol).get(index)!=0.0){
             //added &&sparseMatrix.cols[pivotCol].get(index)!=0.0 Nov 11 2008
             sparseMatrix.swap(pivotRow,index);
             }
             sparseMatrix.zeroBelow(pivotRow,pivotCol);
             pivotRow++;
             pivotCol++;
             continue whilelabel;
             }
             //if execution reaches this point, then all the elements in this col are above the pivot row
             pivotCol++;
             continue whilelabel;
             }else{//this col is empty
             //System.out.println("d");
             //goto next col
             //break label1;
             pivotCol++;//nov 9 2007
             continue whilelabel;
             }*/
        }
    }

    public void reducedRowEchelonFormMatrix(SparseMatrix sparseMatrix) {
        matrixError = false;
        if (true) {
            return;
        }
        for (int rownum = sparseMatrix.rows.size() - 1; rownum >= 0; rownum--) {//start at the bottom row
            if (sparseMatrix.rows.get(rownum).contents.size() == 1//if the bottom row has one entry
                    && sparseMatrix.rows.get(rownum).get(sparseMatrix.cols.size() - 1) != 0.0) {//and it is in the rightmost col
                //System.out.println("error");
                //printMatrix(sparseMatrix);
                //matrixError=true;
                //System.out.println("error in rrefmatrix");
                //return;
                continue;
            }
            if (sparseMatrix.rows.get(rownum).contents.size() == 0) {//if this row is empty
                continue;
            }
            int leftmostentry = sparseMatrix.cols.size();//initialize to far right-we're going to decrease to find the leftmost. note: we know that there is an entry to the left of the rightmost col, we could have initialized to Integer.MAX if we wanted to
            for (Object obj : sparseMatrix.rows.get(rownum).contents.keySet()) {//for each colnum used in this row
                Integer colnum = (Integer) obj;
                if (colnum < leftmostentry) {
                    leftmostentry = colnum;
                }
            }
            sparseMatrix.zeroAbove(rownum, leftmostentry);
        }
    }

    public static void printMatrixRow(SparseMatrix sparseMatrix, int i) {
        String result = "";
        for (int i2 = 0; i2 < sparseMatrix.cols.size(); i2++) {
            if (sparseMatrix.rows.get(i).get(i2) >= 0) {
                result += " ";
            }
            result += (sparseMatrix.rows.get(i).get(i2)) + " ";
            //System.out.print(sparseMatrix.rows[i].get(i2)+" ");
        }
        result += "\n";//System.out.println();
        System.out.println(result);
    }

    public static LinkedHashMap<String, Integer> sortHashMapByValues(HashMap<String, Integer> passedMap) {
        List<String> mapKeys = new ArrayList<>(passedMap.keySet());
        List<Integer> mapValues = new ArrayList<>(passedMap.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);

        LinkedHashMap<String, Integer> sortedMap
                = new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            Integer val = valueIt.next();
            Iterator<String> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                String key = keyIt.next();
                Integer comp1 = passedMap.get(key);
                Integer comp2 = val;

                if (comp1.equals(comp2)) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }
        return sortedMap;
    }

    public static String dumpMatrix(SparseMatrix sparseMatrix) throws Exception {
        
        if (sparseMatrix == null) {
            return "";
        }
        
        StringBuilder sb = new StringBuilder();
        
        sb.append("Rows:" + sparseMatrix.rows.size() + ", Columns:" + sparseMatrix.cols.size() + "\n");
        
        sb.append("Components:" + sparseMatrix.myParent.myCircuit.electricComponents.size() + ", Terminals:" + sparseMatrix.myParent.myCircuit.myTerminals.size() + "\n");

        sb.append("\n");
        
        String spaces = "  ";

        System.out.print("   ");
        
        String columnString = "";

        // Print column numbers.
        for (int i = 0; i < sparseMatrix.cols.size(); i++) {

            if(i == sparseMatrix.firstPointVoltageIndex || i == sparseMatrix.cols.size()-1)
            {
                columnString = columnString.substring(0, columnString.length() - 1);
                columnString += ("  ");
            }
            
            if (i == 9) {
                spaces = " ";
            }
           
            columnString += (i + spaces);

        }

        sb.append(columnString + "\n");

        String result = "";
        for (int rowIndex = 0; rowIndex < sparseMatrix.rows.size(); rowIndex++) {
            
            if(rowIndex == sparseMatrix.rows.size() - (sparseMatrix.firstPointVoltageIndex + 2))
            {
                result += "   ";
                
                for(int x = 0; x <= sparseMatrix.cols.size() * 3 + 2; x++)
                {
                   result += "-";
                }
                result += "\n";
            }

            result += String.format("%2d", rowIndex);
            
            String lastValueString = "";

            for (int columnIndex = 0; columnIndex < sparseMatrix.cols.size(); columnIndex++) {
                
                if(columnIndex == sparseMatrix.firstPointVoltageIndex ||
                   columnIndex == sparseMatrix.cols.size()-1)
                {                            
                    result += ("|");
                }

                if (sparseMatrix.rows.get(rowIndex).get(columnIndex) >= 0) {
                    result += " ";
                }

                double value = sparseMatrix.rows.get(rowIndex).get(columnIndex);
                
                if(value == 0.0)
                {
                    result += "0";
                }
                else if(value == -1.0)
                {
                    result += "-1";
                }
                else if(value == 1.0)
                {
                    result += "1";
                }
                else
                {
                    lastValueString = String.format("%.2f", value);
                    result += lastValueString;
                }

                result += " ";
            }
            
            /* todo:tk the descriptions don't seem to match the rows they are in.
            String padding = (lastValueString.length() > 1 ? " " : "    ");
            result += padding + sparseMatrix.rows.get(rowIndex).description;
            */

            result += "\n";//System.out.println();
        }
        sb.append(result + "\n");


        
        String[] columnDescriptions = new String[sparseMatrix.cols.size()];
        for (int colIndex = 0; colIndex < sparseMatrix.cols.size(); colIndex++)
        {
            columnDescriptions[colIndex] = sparseMatrix.cols.get(colIndex).description;
        }
        
        PrintVertical.printWordsVertically(columnDescriptions, sparseMatrix.firstPointVoltageIndex, sparseMatrix.cols.size() - 1);
        
        sb.append("\n");
        
        //=================================

        Set s = (sortHashMapByValues(sparseMatrix.myParent.myCircuit.getNumbers())).keySet();
        
        Iterator<String> it = s.iterator();
        while (it.hasNext()) {
            String keyName = it.next();
            
            String componentName = sparseMatrix.myParent.myCircuit.myComponentNames.get(keyName);
            
            if(componentName == null)
            {
                componentName = keyName;
            }
            
            
            String padding = "   ";
            if(componentName.length() == 3) padding = "  ";
            if(componentName.length() == 4) padding = " ";

            sb.append(componentName + padding + sparseMatrix.myParent.myCircuit.getNumbers().get(keyName) + "\n");
        }

        sb.append( "\n");

        sb.append("# of KCL Equations:" + sparseMatrix.numberOfKCLeqs + "\n");
        
        sb.append("\n");
        
        return sb.toString();
    }

}
