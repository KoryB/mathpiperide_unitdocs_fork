package org.mathpiper.ui.gui.applications.voscilloscope.oscilloscope;

import java.applet.Applet;
import org.mathpiper.ui.gui.applications.voscilloscope.util.Parameter;
import org.mathpiper.ui.gui.applications.voscilloscope.simulator.Evaluation;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.IOException;

/**
 * <p>Title: Virtual Oscilloscope.</p>
 * <p>Description: A Oscilloscope simulator</p>
 * <p>Copyright (C) 2003 José Manuel Gómez Soriano</p>
 * <h2>License</h2>
 * <p>
 This file is part of Virtual Oscilloscope.

 Virtual Oscilloscope is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Virtual Oscilloscope is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Virtual Oscilloscope; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 * </p>
 */

public class VOApplet extends Applet
    implements Evaluation
{
    public Oscilloscope osc;
    private static final int INVALIDRANDOMPARAM = 10;
    Parameter params[] = {
        new Parameter("ACTSENYALES", "BOOLEAN"), new Parameter("ENT1", "STRING"), new Parameter("ENT2", "STRING"), new Parameter("SIGNAL", "STRING"), new Parameter("EXECUTE", "NONE"), new Parameter("PLAY", "NONE"), new Parameter("RESET", "NONE"), new Parameter("PAUSE", "NONE"), new Parameter("STOP", "NONE"), new Parameter("VISIBLE", "BOOLEAN"),
        new Parameter("+-", "BOOLEAN"), new Parameter("ACDC1", "INT"), new Parameter("ACDC2", "INT"), new Parameter("ADD", "BOOLEAN"), new Parameter("ATNORMAL", "BOOLEAN"), new Parameter("BTN+-", "BOOLEAN"), new Parameter("BTNACDC1", "INT"), new Parameter("BTNACDC2", "INT"), new Parameter("BTNADD", "BOOLEAN"), new Parameter("BTNATNORMAL", "BOOLEAN"),
        new Parameter("BTNCALIDAD", "INT"), new Parameter("BTNCICLICA1", "BOOLEAN"), new Parameter("BTNCICLICA2", "BOOLEAN"), new Parameter("BTNCH12", "BOOLEAN"), new Parameter("BTNDIGITAL", "BOOLEAN"), new Parameter("BTNDUAL", "BOOLEAN"), new Parameter("BTNFOCO", "INT"), new Parameter("BTNHOLDOFF", "INT"), new Parameter("BTNINTENS", "INT"), new Parameter("BTNINV1", "BOOLEAN"),
        new Parameter("BTNINV2", "BOOLEAN"), new Parameter("BTNLED", "BOOLEAN"), new Parameter("BTNLEVEL", "INT"), new Parameter("BTNPOWER", "BOOLEAN"), new Parameter("BTNTIME", "FLOAT"), new Parameter("BTNTIMEFINO", "FLOAT"), new Parameter("BTNVOLT1", "FLOAT"), new Parameter("BTNVOLT2", "FLOAT"), new Parameter("BTNVOLTFINO1", "FLOAT"), new Parameter("BTNVOLTFINO2", "FLOAT"),
        new Parameter("BTNXMAG", "BOOLEAN"), new Parameter("BTNXPOS", "INT"), new Parameter("BTNXY", "BOOLEAN"), new Parameter("BTNYPOS1", "INT"), new Parameter("BTNYPOS2", "INT"), new Parameter("CALIDAD", "INT"), new Parameter("CICLICA1", "BOOLEAN"), new Parameter("CICLICA2", "BOOLEAN"), new Parameter("CH12", "BOOLEAN"), new Parameter("DIGITAL", "BOOLEAN"),
        new Parameter("DUAL", "BOOLEAN"), new Parameter("FOCO", "INT"), new Parameter("HOLDOFF", "INT"), new Parameter("INTENS", "INT"), new Parameter("INV1", "BOOLEAN"), new Parameter("INV2", "BOOLEAN"), new Parameter("POWER", "BOOLEAN"), new Parameter("TIME", "FLOAT"), new Parameter("VOLT1", "FLOAT"), new Parameter("VOLT2", "FLOAT"),
        new Parameter("XMAG", "BOOLEAN"), new Parameter("XPOS", "INT"), new Parameter("XY", "BOOLEAN"), new Parameter("YPOS1", "INT"), new Parameter("YPOS2", "INT")
    };

    public VOApplet()
    {
    }

    public void init()
    {
        /*
      try
      {
        String urlImages = this.getParameter("urlImages");
        System.out.println(urlImages);
        osc = new Oscilloscope(getCodeBase(), new URL(urlImages), this);
        osc.init();
      }
      catch(MalformedURLException mue)
      { mue.printStackTrace(); }
*/
    }

    public void start()
    {
        osc.start();
    }

    public void stop()
    {
    }

    public void destroy()
    {
        osc.destroy();
    }

    public String getParameterType(String param)
    {
        for(int i = 0; i < params.length; i++)
        {
            if(params[i].getName().equalsIgnoreCase(param))
            {
                return params[i].getType();
            }
        }

        return "NONE";
    }

    public String[] getAllParameters()
    {
        String parametros[] = new String[params.length];
        for(int i = 0; i < params.length; i++)
        {
            parametros[i] = params[i].getName();
        }

        return parametros;
    }

    public boolean hasParameter(String param)
    {
        String params[] = getAllParameters();
        for(int i = 0; i < params.length; i++)
        {
            if(params[i].equalsIgnoreCase(param))
            {
                return true;
            }
        }

        return false;
    }

    public void setValue(String param, Object value)
    {
        if("SIGNAL".equalsIgnoreCase(param))
        {
            osc.signals.channel1 = (Signal)value;
            osc.signals.rewind();
            if(osc.actSignals)
            {
                osc.configuraSignal();
            }
        }
    }

    public void setValueBoolean(String param, boolean value)
    {
        PBackground pBackground = osc.pBackground;
        SignalManager signals = osc.signals;
        if(!hasParameter(param))
        {
            System.out.println("No existe el par\341metro:" + param + ".");
            return;
        }
        if("BTNPOWER".equalsIgnoreCase(param))
        {
            pBackground.pTime.btnPower.setValue(value);
        }
        if("BTNLED".equalsIgnoreCase(param))
        {
            pBackground.pTime.btnLed.setValue(value);
        }
        if("BTNXY".equalsIgnoreCase(param))
        {
            pBackground.pTime.btnXY.setValue(value);
        }
        if("BTNATNORMAL".equalsIgnoreCase(param))
        {
            pBackground.pTime.btnAtNormal.setValue(value);
        }
        if("BTN+-".equalsIgnoreCase(param))
        {
            pBackground.pTime.btnMasMenos.setValue(value);
        }
        if("BTNXMAG".equalsIgnoreCase(param))
        {
            pBackground.pQuality.BtnX10.setValue(value);
        }
        if("BTNINV1".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnInv1.setValue(value);
        }
        if("BTNINV2".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnInv2.setValue(value);
        }
        if("BTNCH12".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnCH12.setValue(value);
        }
        if("BTNDUAL".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnDual.setValue(value);
        }
        if("BTNADD".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnAdd.setValue(value);
        }
        if("BTNDIGITAL".equalsIgnoreCase(param))
        {
            pBackground.pQuality.BtnDigital.setValue(value);
        }
        if("BTNCICLICA1".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnCiclica1.setValue(value);
        }
        if("BTNCICLICA2".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnCiclica2.setValue(value);
        }
        if("POWER".equalsIgnoreCase(param))
        {
            if(value)
            {
                pBackground.screen.encender();
            } else
            {
                pBackground.screen.apagar();
            }
        }
        if("XY".equalsIgnoreCase(param))
        {
            signals.setXY(value);
        }
        if("ATNORMAL".equalsIgnoreCase(param))
        {
            signals.setAtNormal(value);
        }
        if("+-".equalsIgnoreCase(param))
        {
            signals.setDisPosNeg(value);
        }
        if("XMAG".equalsIgnoreCase(param))
        {
            signals.setXMag(value);
        }
        if("INV1".equalsIgnoreCase(param))
        {
            signals.channel1.setInvert(value);
        }
        if("INV2".equalsIgnoreCase(param))
        {
            signals.channel2.setInvert(value);
        }
        if("CH12".equalsIgnoreCase(param))
        {
            signals.setCH12(value);
        }
        if("DUAL".equalsIgnoreCase(param))
        {
            signals.channel1.setVisible(value);
            signals.channel2.setVisible(value);
        }
        if("ADD".equalsIgnoreCase(param))
        {
            signals.setAdd(value);
        }
        if("DIGITAL".equalsIgnoreCase(param))
        {
            signals.setDigital(value);
        }
        if("CICLICA1".equalsIgnoreCase(param))
        {
            signals.channel1.setCiclica(value);
        }
        if("CICLICA2".equalsIgnoreCase(param))
        {
            signals.channel2.setCiclica(value);
        }
        if("ACTSENYALES".equalsIgnoreCase(param))
        {
            osc.actSignals = value;
        }
        if("VISIBLE".equalsIgnoreCase(param))
        {
            osc.setVisible(value);
        }
    }

    public void setValueInt(String param, int value)
    {
        PBackground pBackground = osc.pBackground;
        SignalManager signals = osc.signals;
        if(!hasParameter(param))
        {
            System.out.println("No existe el par\341metro:" + param + ".");
            return;
        }
        if("BTNACDC1".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnAcDc1.setValue(value);
        }
        if("BTNACDC2".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnAcDc2.setValue(value);
        }
        if("BTNYPOS1".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnYPos1.setValue(value);
        }
        if("BTNYPOS2".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnYPos2.setValue(value);
        }
        if("BTNXPOS".equalsIgnoreCase(param))
        {
            pBackground.pTime.btnXPos.setValue(value);
        }
        if("BTNINTENS".equalsIgnoreCase(param))
        {
            pBackground.pTime.btnIntens.setValue(value);
        }
        if("BTNFOCO".equalsIgnoreCase(param))
        {
            pBackground.pTime.btnFocus.setValue(value);
        }
        if("BTNHOLDOFF".equalsIgnoreCase(param))
        {
            pBackground.pTime.btnHoldOff.setValue(value);
        }
        if("BTNLEVEL".equalsIgnoreCase(param))
        {
            pBackground.pTime.btnLevel.setValue(value);
        }
        if("BTNCALIDAD".equalsIgnoreCase(param))
        {
            pBackground.pQuality.BtnQuality.setValue(value);
        }
        if("ACDC1".equalsIgnoreCase(param))
        {
            if(value == 1)
            {
                signals.channel1.sinContinuous();
            } else
            if(value == 2)
            {
                signals.channel1.conContinuous();
            } else
            {
                signals.channel1.signalGND();
            }
        }
        if("ACDC2".equalsIgnoreCase(param))
        {
            if(value == 1)
            {
                signals.channel2.sinContinuous();
            } else
            if(value == 2)
            {
                signals.channel2.conContinuous();
            } else
            {
                signals.channel2.signalGND();
            }
        }
        if("YPOS1".equalsIgnoreCase(param))
        {
            signals.channel1.setYPos(value);
        }
        if("YPOS2".equalsIgnoreCase(param))
        {
            signals.channel2.setYPos(value);
        }
        if("XPOS".equalsIgnoreCase(param))
        {
            signals.setXPos(value);
        }
        if("INTENS".equalsIgnoreCase(param))
        {
            pBackground.screen.setIntens(value);
        }
        if("FOCO".equalsIgnoreCase(param))
        {
            pBackground.screen.setFocus(value);
        }
        if("HOLDOFF".equalsIgnoreCase(param))
        {
            signals.setHoldOff(value);
        }
        if("LEVEL".equalsIgnoreCase(param))
        {
            signals.setLevelDisparo(value);
        }
        if("CALIDAD".equalsIgnoreCase(param))
        {
            signals.setQuality(value);
        }
    }

    public void setValueDouble(String param, double value)
    {
        PBackground pBackground = osc.pBackground;
        SignalManager signals = osc.signals;
        if(!hasParameter(param))
        {
            System.out.println("No existe el par\341metro:" + param + ".");
            return;
        }
        if("BTNTIME".equalsIgnoreCase(param))
        {
            pBackground.pTime.btnTime.setValue(value);
        }
        if("BTNTIMEFINO".equalsIgnoreCase(param))
        {
            pBackground.pTime.btnTime.setValueFino(value);
        }
        if("BTNVOLT1".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnVolt1.setValue(value);
        }
        if("BTNVOLT2".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnVolt2.setValue(value);
        }
        if("BTNVOLTFINO1".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnVolt1.setValueFino(value);
        }
        if("BTNVOLTFINO2".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnVolt2.setValueFino(value);
        }
        if("TIME".equalsIgnoreCase(param))
        {
            signals.setTime(value);
        }
        if("VOLT1".equalsIgnoreCase(param))
        {
            signals.channel1.setVDiv(value);
        }
        if("VOLT2".equalsIgnoreCase(param))
        {
            signals.channel2.setVDiv(value);
        }
    }

    public void setValueString(String param, String value)
    {
        SignalManager signals = osc.signals;
        if(!hasParameter(param))
        {
            System.out.println("No existe el par\341metro:" + param + ".");
            return;
        }
        try
        {
            if("ENT1".equalsIgnoreCase(param))
            {
                signals.channel1.setSamples((new URL(getCodeBase(), value)).openStream());
            }
            if("ENT2".equalsIgnoreCase(param))
            {
                signals.channel2.setSamples((new URL(getCodeBase(), value)).openStream());
            }
        }
        catch(MalformedURLException mue)
        {
            System.out.println("Direcci\363n URL mal formada:" + mue);
        }
        catch(IOException ioe)
        {
            System.out.println("Error I/O:" + ioe);
        }
    }

    public void setValueVoid(String param)
    {
        if("EXECUTE".equalsIgnoreCase(param))
        {
            executeSimulator();
        }
        if("PLAY".equalsIgnoreCase(param))
        {
            playSimulator();
        }
        if("RESET".equalsIgnoreCase(param))
        {
            resetSimulator();
        }
        if("PAUSE".equalsIgnoreCase(param))
        {
            pauseSimulator();
        }
        if("STOP".equalsIgnoreCase(param))
        {
            stopSimulator();
        }
    }

    public boolean getValueBoolean(String param)
    {
        PBackground pBackground = osc.pBackground;
        SignalManager signals = osc.signals;
        if(!hasParameter(param))
        {
            System.out.println("No existe el par\341metro:" + param + ".");
        }
        if("BTNPOWER".equalsIgnoreCase(param))
        {
            return pBackground.pTime.btnPower.getValue();
        }
        if("BTNXY".equalsIgnoreCase(param))
        {
            return pBackground.pTime.btnXY.getValue();
        }
        if("BTNATNORMAL".equalsIgnoreCase(param))
        {
            return pBackground.pTime.btnAtNormal.getValue();
        }
        if("BTN+-".equalsIgnoreCase(param))
        {
            return pBackground.pTime.btnMasMenos.getValue();
        }
        if("BTNXMAG".equalsIgnoreCase(param))
        {
            return pBackground.pQuality.BtnX10.getValue();
        }
        if("BTNINV1".equalsIgnoreCase(param))
        {
            return pBackground.pVolt.BtnInv1.getValue();
        }
        if("BTNINV2".equalsIgnoreCase(param))
        {
            return pBackground.pVolt.BtnInv2.getValue();
        }
        if("BTNCH12".equalsIgnoreCase(param))
        {
            return pBackground.pVolt.BtnCH12.getValue();
        }
        if("BTNDUAL".equalsIgnoreCase(param))
        {
            return pBackground.pVolt.BtnDual.getValue();
        }
        if("BTNADD".equalsIgnoreCase(param))
        {
            return pBackground.pVolt.BtnAdd.getValue();
        }
        if("BTNDIGITAL".equalsIgnoreCase(param))
        {
            return pBackground.pQuality.BtnDigital.getValue();
        }
        if("BTNCICLICA1".equalsIgnoreCase(param))
        {
            return pBackground.pVolt.BtnCiclica1.getValue();
        }
        if("BTNCICLICA2".equalsIgnoreCase(param))
        {
            return pBackground.pVolt.BtnCiclica2.getValue();
        }
        if("POWER".equalsIgnoreCase(param))
        {
            return pBackground.screen.isPower();
        }
        if("XY".equalsIgnoreCase(param))
        {
            return signals.isXY();
        }
        if("ATNORMAL".equalsIgnoreCase(param))
        {
            return signals.getAtNormal();
        }
        if("+-".equalsIgnoreCase(param))
        {
            return signals.getDisPosNeg();
        }
        if("XMAG".equalsIgnoreCase(param))
        {
            return signals.isXMag();
        }
        if("INV1".equalsIgnoreCase(param))
        {
            return signals.channel1.isInvert();
        }
        if("INV2".equalsIgnoreCase(param))
        {
            return signals.channel2.isInvert();
        }
        if("CH12".equalsIgnoreCase(param))
        {
            return signals.getCH12();
        }
        if("DUAL".equalsIgnoreCase(param))
        {
            return signals.isDual();
        }
        if("ADD".equalsIgnoreCase(param))
        {
            return signals.isAdd();
        }
        if("DIGITAL".equalsIgnoreCase(param))
        {
            return signals.isDigital();
        }
        if("CICLICA1".equalsIgnoreCase(param))
        {
            return signals.channel1.isCiclica();
        }
        if("CICLICA2".equalsIgnoreCase(param))
        {
            return signals.channel2.isCiclica();
        }
        if("ACTSENYALES".equalsIgnoreCase(param))
        {
            return osc.actSignals;
        }
        if("VISIBLE".equalsIgnoreCase(param))
        {
            return osc.isVisible();
        } else
        {
            return false;
        }
    }

    public int getValueInt(String param)
    {
        PBackground pBackground = osc.pBackground;
        SignalManager signals = osc.signals;
        if(!hasParameter(param))
        {
            System.out.println("No existe el par\341metro:" + param + ".");
        }
        if("BTNACDC1".equalsIgnoreCase(param))
        {
            return pBackground.pVolt.BtnAcDc1.getValue();
        }
        if("BTNACDC2".equalsIgnoreCase(param))
        {
            return pBackground.pVolt.BtnAcDc2.getValue();
        }
        if("BTNYPOS1".equalsIgnoreCase(param))
        {
            return (int)pBackground.pVolt.BtnYPos1.getValue();
        }
        if("BTNYPOS2".equalsIgnoreCase(param))
        {
            return (int)pBackground.pVolt.BtnYPos2.getValue();
        }
        if("BTNXPOS".equalsIgnoreCase(param))
        {
            return (int)pBackground.pTime.btnXPos.getValue();
        }
        if("BTNINTENS".equalsIgnoreCase(param))
        {
            return (int)pBackground.pTime.btnIntens.getValue();
        }
        if("BTNFOCO".equalsIgnoreCase(param))
        {
            return (int)pBackground.pTime.btnFocus.getValue();
        }
        if("BTNHOLDOFF".equalsIgnoreCase(param))
        {
            return (int)pBackground.pTime.btnHoldOff.getValue();
        }
        if("BTNLEVEL".equalsIgnoreCase(param))
        {
            return (int)pBackground.pTime.btnLevel.getValue();
        }
        if("BTNCALIDAD".equalsIgnoreCase(param))
        {
            return (int)pBackground.pQuality.BtnQuality.getValue();
        }
        if("ACDC1".equalsIgnoreCase(param))
        {
            return signals.channel1.getTipoSignal();
        }
        if("ACDC2".equalsIgnoreCase(param))
        {
            return signals.channel2.getTipoSignal();
        }
        if("YPOS1".equalsIgnoreCase(param))
        {
            return signals.channel1.getYPos();
        }
        if("YPOS2".equalsIgnoreCase(param))
        {
            return signals.channel2.getYPos();
        }
        if("XPOS".equalsIgnoreCase(param))
        {
            return signals.getXPos();
        }
        if("INTENS".equalsIgnoreCase(param))
        {
            return pBackground.screen.getIntens();
        }
        if("FOCO".equalsIgnoreCase(param))
        {
            return pBackground.screen.getFocus();
        }
        if("HOLDOFF".equalsIgnoreCase(param))
        {
            return signals.getHoldOff();
        }
        if("LEVEL".equalsIgnoreCase(param))
        {
            return signals.getLevelDisparo();
        }
        if("CALIDAD".equalsIgnoreCase(param))
        {
            return signals.getQuality();
        } else
        {
            return 0;
        }
    }

    public double getValueDouble(String param)
    {
        PBackground pBackground = osc.pBackground;
        SignalManager signals = osc.signals;
        if(!hasParameter(param))
        {
            System.out.println("No existe el par\341metro:" + param + ".");
        }
        if("BTNTIME".equalsIgnoreCase(param))
        {
            return pBackground.pTime.btnTime.getValue();
        }
        if("BTNTIMEFINO".equalsIgnoreCase(param))
        {
            return pBackground.pTime.btnTime.getValueFino();
        }
        if("BTNVOLT1".equalsIgnoreCase(param))
        {
            return pBackground.pVolt.BtnVolt1.getValue();
        }
        if("BTNVOLT2".equalsIgnoreCase(param))
        {
            return pBackground.pVolt.BtnVolt2.getValue();
        }
        if("BTNVOLTFINO1".equalsIgnoreCase(param))
        {
            return pBackground.pVolt.BtnVolt1.getValueFino();
        }
        if("BTNVOLTFINO2".equalsIgnoreCase(param))
        {
            return pBackground.pVolt.BtnVolt2.getValueFino();
        }
        if("TIME".equalsIgnoreCase(param))
        {
            return signals.getTime();
        }
        if("VOLT1".equalsIgnoreCase(param))
        {
            return signals.channel1.getVDiv();
        }
        if("VOLT2".equalsIgnoreCase(param))
        {
            return signals.channel2.getVDiv();
        } else
        {
            return 0.0F;
        }
    }

    public String getValueString(String param)
    {
        PBackground pBackground = osc.pBackground;
        SignalManager signals = osc.signals;
        if(!hasParameter(param))
        {
            System.out.println("No existe el par\341metro:" + param + ".");
        }
        return "";
    }

    private void setRandomValueInt(String param)
    {
        PBackground pBackground = osc.pBackground;
        SignalManager signals = osc.signals;
        if(!"BTNACDC1".equalsIgnoreCase(param));
        setValueInt(param, (int)(Math.random() * (double)3));
        if("BTNACDC2".equalsIgnoreCase(param))
        {
            setValueInt(param, (int)(Math.random() * (double)3));
        }
        if("BTNYPOS1".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnYPos1.setPosicion((int)(Math.random() * (double)291));
        }
        if("BTNYPOS2".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnYPos2.setPosicion((int)(Math.random() * (double)291));
        }
        if("BTNXPOS".equalsIgnoreCase(param))
        {
            pBackground.pTime.btnXPos.setPosicion((int)(Math.random() * (double)40));
        }
        if("BTNINTENS".equalsIgnoreCase(param))
        {
            pBackground.pTime.btnIntens.setPosicion((int)(Math.random() * (double)25));
        }
        if("BTNFOCO".equalsIgnoreCase(param))
        {
            pBackground.pTime.btnFocus.setPosicion((int)(Math.random() * (double)14));
        }
        if("BTNHOLDOFF".equalsIgnoreCase(param))
        {
            setValueInt(param, (int)(Math.random() * (double)46));
        }
        if("BTNLEVEL".equalsIgnoreCase(param))
        {
            pBackground.pTime.btnLevel.setPosicion((int)(Math.random() * (double)93));
        }
        if("BTNCALIDAD".equalsIgnoreCase(param))
        {
            pBackground.pQuality.BtnQuality.setPosicion((int)(Math.random() * (double)9));
        }
        if("ACDC1".equalsIgnoreCase(param))
        {
            setValueInt(param, (int)(Math.random() * (double)3));
        }
        if("ACDC2".equalsIgnoreCase(param))
        {
            setValueInt(param, (int)(Math.random() * (double)3));
        }
        if("YPOS1".equalsIgnoreCase(param))
        {
            setValueInt(param, (int)(Math.random() * (double)291) - 145);
        }
        if("YPOS2".equalsIgnoreCase(param))
        {
            setValueInt(param, (int)(Math.random() * (double)291) - 145);
        }
        if("XPOS".equalsIgnoreCase(param))
        {
            setValueInt(param, (int)(Math.random() * (double)40) - 20);
        }
        if("INTENS".equalsIgnoreCase(param))
        {
            setValueInt(param, ((int)(Math.random() * (double)25) * 255) / 24);
        }
        if("FOCO".equalsIgnoreCase(param))
        {
            setValueInt(param, (int)(Math.random() * (double)14));
        }
        if("HOLDOFF".equalsIgnoreCase(param))
        {
            setValueInt(param, ((int)(Math.random() * (double)46) * 100) / 45);
        }
        if("LEVEL".equalsIgnoreCase(param))
        {
            setValueInt(param, ((int)(Math.random() * (double)93) * 100) / 46 - 100);
        }
        if("CALIDAD".equalsIgnoreCase(param))
        {
            setValueInt(param, (int)(Math.random() * (double)10) + 1);
        }
    }

    private void setRandomValueDouble(String param)
    {
        PBackground pBackground = osc.pBackground;
        SignalManager signals = osc.signals;
        double voltValues[] = {
            20D, 10D, 5D, 2D, 1.0D, 0.5D, 0.20000000000000001D, 0.10000000000000001D, 0.050000000000000003D, 0.02D,
            0.01D, 0.0050000000000000001D, 0.002D, 0.001D, 0.00050000000000000001D
        };
        double timeValues[] = {
            0.20000000000000001D, 0.10000000000000001D, 0.050000000000000003D, 0.02D, 0.01D, 0.0050000000000000001D, 0.002D, 0.001D, 0.00050000000000000001D, 0.00020000000000000001D,
            0.0001D, 5.0000000000000002E-005D, 2.0000000000000002E-005D, 1.0000000000000001E-005D, 5.0000000000000004E-006D, 1.9999999999999999E-006D, 9.9999999999999995E-007D, 4.9999999999999998E-007D
        };
        if("BTNTIME".equalsIgnoreCase(param))
        {
            pBackground.pTime.btnTime.setPosicion((int)(Math.random() * (double)12));
        }
        if("BTNTIMEFINO".equalsIgnoreCase(param))
        {
            setValueDouble(param, (double)(Math.random() * (double)45 * 0.5D));
        }
        if("BTNVOLT1".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnVolt1.setPosicion((int)(Math.random() * (double)14));
        }
        if("BTNVOLT2".equalsIgnoreCase(param))
        {
            pBackground.pVolt.BtnVolt2.setPosicion((int)(Math.random() * (double)14));
        }
        if("BTNVOLTFINO1".equalsIgnoreCase(param))
        {
            setValueDouble(param, (double)(Math.random() * (double)45 * 0.5D));
        }
        if("BTNVOLTFINO2".equalsIgnoreCase(param))
        {
            setValueDouble(param, (double)(Math.random() * (double)45 * 0.5D));
        }
        if("TIME".equalsIgnoreCase(param))
        {
            setValueDouble(param, (double)timeValues[(int)(Math.random() * (double)18)]);
        }
        if("VOLT1".equalsIgnoreCase(param))
        {
            setValueDouble(param, (double)voltValues[(int)(Math.random() * (double)12)]);
        }
        if("VOLT2".equalsIgnoreCase(param))
        {
            setValueDouble(param, (double)voltValues[(int)(Math.random() * (double)12)]);
        }
    }

    public void setRandomValue(String param)
    {
        if(!hasParameter(param))
        {
            System.out.println("No existe el par\341metro:" + param + ".");
            return;
        } else
        {
            setRandomValueInt(param);
            setValueBoolean(param, Math.random() >= 0.5D);
            setRandomValueDouble(param);
            return;
        }
    }

    public void setRandomValueAll()
    {
        String params[] = getAllParameters();
        for(int i = 10; i < params.length; i++)
        {
            setRandomValue(params[i]);
        }

    }

    public void executeSimulator()
    {
        osc.configuraSignal();
    }

    public boolean playSimulator()
    {
        SignalManager signals = osc.signals;
        boolean hasActive = signals.isSuspend();
        if(!hasActive)
        {
            signals.reanudar();
        }
        return hasActive;
    }

    public void resetSimulator()
    {
        stop();
        init();
    }

    public void pauseSimulator()
    {
        SignalManager signals = osc.signals;
        if(!signals.isSuspend())
        {
            signals.suspender();
        }
    }

    public void stopSimulator()
    {
        resetSimulator();
    }
}
