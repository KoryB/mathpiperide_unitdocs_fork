package org.mathpiper.ui.gui.applications.voscilloscope.simulator;

import java.awt.*;

/**
 * <p>Title: Virtual Oscilloscope.</p>
 * <p>Description: A Oscilloscope simulator</p>
 * <p>Copyright (C) 2003 José Manuel Gómez Soriano</p>
 * <h2>License</h2>
 * <p>
 This file is part of Virtual Oscilloscope.

 Virtual Oscilloscope is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Virtual Oscilloscope is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Virtual Oscilloscope; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 * </p>
 */

public class Message extends Dialog
{

    Button btnAccept;
    private Label lMensaje;
    private static Frame fFrame;
    boolean accept;

    private Message(Frame parent, String text, String caption, boolean accept)
    {
        super(parent, caption, accept);
        setModal(accept);
        btnAccept = new Button("Accept");
        lMensaje = new Label(text, 1);
        add(btnAccept);
        add(lMensaje);
        this.accept = accept;
    }

    public void addNotify()
    {
        super.addNotify();
        setLayout(null);
        setSize();
        btnAccept.setVisible(accept);
        lMensaje.setVisible(true);
    }

    public synchronized void closeDialog()
    {
        setVisible(false);
        dispose();
    }

    public void setSize()
    {
        FontMetrics fm = getFontMetrics(getFont());
        int width = lMensaje.getPreferredSize().width;
        int height = lMensaje.getMinimumSize().height;
        if(accept)
        {
            height += btnAccept.getMinimumSize().height;
        }
        setSize(width + 20, height + 50);
        lMensaje.setLocation(10, 20);
        lMensaje.setSize(lMensaje.getPreferredSize());
        width = btnAccept.getMinimumSize().width;
        btnAccept.setLocation(getSize().width / 2 - width / 2, lMensaje.getMinimumSize().height + 20);
        btnAccept.setSize(btnAccept.getMinimumSize());
        Dimension dim = getToolkit().getScreenSize();
        setLocation(dim.width / 2 - getSize().width / 2, dim.height / 2 - getSize().height / 2);
    }

    public void setText(String text)
    {
        lMensaje.setText(text);
        btnAccept.setVisible(accept);
    }

    public static synchronized Message makeDialog(String text, String caption, boolean modal)
    {
        if(fFrame == null)
        {
            fFrame = new Frame();
        }
        Message mens = new Message(fFrame, text, caption, modal);
        mens.setVisible(true);
        return mens;
    }
}
