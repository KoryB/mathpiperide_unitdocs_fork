%mathpiper,def="HoldArgumentNumber"

Procedure("HoldArgumentNumber",["procedure", "arity", "index"])
{
  Local(args);
  args:=RulebaseArgumentsList(procedure,arity);
/* Echo(["holdnr ",args]); */
  ApplyFast("HoldArgument",[procedure,args[index]]);
}

%/mathpiper



%mathpiper_docs,name="HoldArgumentNumber",categories="Programming Procedures;Miscellaneous;Built In"
*CMD HoldArgumentNumber --- specify argument as not evaluated
*STD
*CALL
        HoldArgumentNumber("procedure", arity, argNum)

*PARMS
{"procedure"} -- string, procedure name

{arity}, {argNum} -- positive integers

*DESC

Declares the argument numbered {argNum} of the procedure named {"procedure"} with
specified {arity} to be unevaluated ("held"). Useful if you don't know symbolic
names of parameters, for instance, when the procedure was not defined using an
explicit {RulebaseHoldArguments} call. Otherwise you could use {HoldArgument}.

*SEE HoldArgument, RulebaseHoldArguments
%/mathpiper_docs