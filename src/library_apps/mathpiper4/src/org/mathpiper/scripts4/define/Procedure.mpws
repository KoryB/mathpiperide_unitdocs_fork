%mathpiper,def="Procedure"

/* Defining a macro-like procedure that declares a procedure
 * with only one rule.
 */
RulebaseHoldArguments("Procedure",["oper", "args", "body"]);



// procedure with variable number of arguments: Procedure("func",[x,y, ...])body;
RuleHoldArguments("Procedure",3,2047,
        And?(GreaterThan?(Length(args), 1), Equal?( MathNth(args, Length(args)), ToAtom("...") ))
)
{
    Check(String?(oper), "The name of the procedure must be specified using a string.");
     Delete!(args,Length(args));        // remove trailing "..."
    Check(
    {
        Local(result, index); 
        Assign(result, True);
        Assing(index, 1);
        While(index <=? Length(args))
        {
            If(!? String?(args[index]))
            {
                result := False;
            }
            Assign(index, index + 1);
        }
        result;
    }
    , "Parameters must be specified using strings."
    );
    Retract(oper,Length(args));
    RulebaseListedEvaluateArguments(oper,args);
    RuleEvaluateArguments(oper,Length(args),1025,True) body;  // at precedence 1025, for flexibility
}

// procedure with a fixed number of arguments
RuleHoldArguments("Procedure",3,2048,True)
{
  Check(String?(oper), "The name of the procedure must be specified using a string.");
  Check(
      {
          Local(result, index); 
          Assign(result, True);
          Assign(index, 1);
          While(index <=? Length(args))
          {
              If(!? String?(args[index]))
              {
                  result := False;
              }
              Assign(index, index + 1);
          }
          result;
      }
      , "Parameters must be specified using strings."
      );
    Retract(oper,Length(args));
    RulebaseEvaluateArguments(oper,args);
    RuleEvaluateArguments(oper,Length(args),1025,True) body;
}


/// shorthand procedure declarations
RulebaseHoldArguments("Procedure",["oper"]);
// procedure with variable number of arguments: Procedure() f(x,y, ...)
RuleHoldArguments("Procedure",1,2047,
        And?(Procedure?(oper), GreaterThan?(Length(oper), 1), Equal?( MathNth(oper, Length(oper)), ToAtom("...") ))
)
{
        Local(args);
        Assign(args,Rest(ProcedureToList(oper)));
        Delete!(args,Length(args));        // remove trailing "..."
        Decide(RulebaseDefined(Type(oper),Length(args)),
                False,        // do nothing
                RulebaseListedEvaluateArguments(Type(oper),args)
        );
}


// procedure with a fixed number of arguments
RuleHoldArguments("Procedure",1,2048,
        And?(Procedure?(oper))
)
{
    Local(args);
    Assign(args,Rest(ProcedureToList(oper)));
    Check(
    {
        Local(result, index); 
        Assign(result, True);
        Assign(index, 1);
        While(index <=? Length(args))
        {
            If(!? String?(args[index]))
            {
                result := False;
            }
            Assign(index, index + 1);
        }
        result;
    }
    , "Parameters must be specified using strings."
    );
    Decide(RulebaseDefined(Type(oper),Length(args)),
            False,        // do nothing
            RulebaseEvaluateArguments(Type(oper),args)
    );
}


HoldArgument("Procedure","oper");
HoldArgument("Procedure","args");
HoldArgument("Procedure","body");

%/mathpiper




%mathpiper_docs,name="Procedure",categories="Programming Procedures;Programming"
*CMD Function --- declare or define a procedure
*STD
*CALL
        Procedure() procedure(arglist)
        Procedure() procedure(arglist, ...)
        Procedure("name", [arglist]) body
        Procedure("name", [arglist, ...]) body

*PARMS

{func(args)} -- procedure declaration, e.g. {f(x,y)}

{"name"} -- string, name of the procedure

{[arglist]} -- list of atoms, formal arguments to the procedure

{...} -- literal ellipsis symbol "{...}" used to denote a variable number of arguments

{body} -- expression comprising the body of the procedure

*DESC

This command can be used to define a new procedure with named arguments.


The number of arguments of the new procedure and their names are determined 
by the list {arglist}. If the ellipsis "{...}" follows the last atom in 
{arglist}, a procedure with a variable number of arguments is declared 
(using {RulebaseListedHoldArguments}). Note that the ellipsis cannot be 
the only element of {arglist} and <i>must</i> be preceded by an atom.

A procedure with variable number of arguments can take more arguments than 
elements in {arglist} in this case, it obtains its last argument as a list 
containing all extra arguments.

The short form of the {Procedure} call merely declares a {RulebaseHoldArguments} 
for the new procedure but does not define any procedure body. This is a convenient 
shorthand for {RulebaseHoldArguments} and {RulebaseListedHoldArguments}, when 
definitions of the procedure are to be supplied by rules. If the new procedure 
has been already declared with the same number of arguments (with or without 
variable arguments), {Procedure} returns false and does nothing.

The second, longer form of the {Procedure} call declares a procedure and also 
defines a procedure body. It is equivalent to a
single rule such as {op(arg1_, arg2_) <-- body}. The rule will be declared at
precedence 1025. Any previous rules associated with {"op"} (with the same
arity) will be discarded. More complicated procedures (with more than one body)
can be defined by adding more rules.

*E.G. notest

This will declare a new procedure with two or more arguments, but define no rules for it. This is equivalent to [Rulebase ("f1", [x, y, ...])].

In> Procedure() f1("x", "y",...);
Result: True

In> f1(3,4,5,[1,2,3])
Result: f1(3,[4,5,[1,2,3]])

In> Procedure() f1("x", "y");
Result: False;

This defines a procedure FirstOf which returns the
first element of a list. Equivalent definitions would be
FirstOf(list_) <-- list[1] or FirstOf(list) := list[1].
In> Procedure("FirstOf", ["list"])  list[1];
Result: True;

In> FirstOf([_a,_b,_c]);
Result: _a;

The following procedure will print all arguments to a string:

In> Procedure("PrintAll",["x", ...]) Decide(List?(x),
          PrintList(x), PipeToString()Write(x));
Result: True;

In> PrintAll(1):
Result: "1";

In> PrintAll(1,2,3);
Result: "1 2 3";

*SEE TemplateProcedure, RuleHoldArguments, RulebaseHoldArguments,  RulebaseListedHoldArguments, :=, Retract
%/mathpiper_docs





%mathpiper,name="Procedure",subtype="automatic_test"

Procedure("count",["from", "to"])
{
   Local(i);
   Local(sum);
   Assign(sum,0);
   For(i:=from,i<?to,i:=i+1)
   {
     Assign(sum,sum+i);
   }
   sum;
}

Testing("Procedure definitions");
Verify(count(1,11),55);

Retract("count", All);

%/mathpiper
