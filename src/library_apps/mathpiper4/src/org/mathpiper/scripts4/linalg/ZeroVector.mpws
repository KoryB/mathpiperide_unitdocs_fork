%mathpiper,def="ZeroVector"

Procedure("ZeroVector",["n"])
{
    Local(i,result);
    result:=[];
    For(i:=1,i<=?n,i++)
    {
      Insert!(result,1,0);
    }
    result;
}

%/mathpiper



%mathpiper_docs,name="ZeroVector",categories="Mathematics Procedures;Linear Algebra"
*CMD ZeroVector --- create a vector with all zeroes
*STD
*CALL
        ZeroVector(n)

*PARMS

{n} -- length of the vector to return

*DESC

This command returns a vector of length "n", filled with zeroes.

*E.G.

In> ZeroVector(4)
Result: [0,0,0,0];

*SEE ZeroMatrix, ZeroVector?
%/mathpiper_docs

*SEE BaseVector