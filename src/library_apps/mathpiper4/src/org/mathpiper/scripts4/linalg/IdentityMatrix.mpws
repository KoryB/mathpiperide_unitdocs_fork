%mathpiper,def="IdentityMatrix"

IdentityMatrix(n_NonNegativeInteger?) <--
{
    Local(i,result);
    result:=[];
    For(i:=1,i<=?n,i++)
    {
      Append!(result,BaseVector(i,n));
    }
    result;
}

%/mathpiper



%mathpiper_docs,name="IdentityMatrix",categories="Mathematics Procedures;Linear Algebra"
*CMD IdentityMatrix --- make identity matrix
*STD
*CALL
        IdentityMatrix(n)

*PARMS

{n} -- size of the matrix

*DESC

This commands returns the identity matrix of size "n" by "n". This
matrix has ones on the diagonal while the other entries are zero.

*E.G.

In> IdentityMatrix(3)
Result: [[1,0,0],[0,1,0],[0,0,1]]

*SEE BaseVector, ZeroMatrix, DiagonalMatrix
%/mathpiper_docs





%mathpiper,name="IdentityMatrix",subtype="automatic_test"

Verify(IdentityMatrix(3) , [[1,0,0],[0,1,0],[0,0,1]]);

Verify(IdentityMatrix(4),
   [ [1,  0,  0,  0] ,
     [0,  1,  0,  0] ,
     [0,  0,  1,  0] ,
     [0,  0,  0,  1] ]);

%/mathpiper