%mathpiper,def="Take"

/*  Take  */

/* Needs to check the parameters */

/*
 * Take( list, n ) gives the first n elements of 'list'
 * Take( list, -n ) gives the last n elements of 'list'
 * Take( list, [m,n] ) elements m through n of 'list'
 */

RulebaseHoldArguments("Take", ["lst", "range"]);

RuleHoldArguments("Take", 2, 1, List?(range))
    Take( Drop(lst, range[1] -1), range[2] - range[1] + 1);

RuleHoldArguments("Take", 2, 2, range >=? 0)
    Decide( Length(lst)=?0 |? range=?0, [],
        Concat([First(lst)], Take(Rest(lst), range-1)));

RuleHoldArguments("Take", 2, 2, range <? 0)
    Drop( lst, Decide(AbsN(range) <? Length(lst), Length(lst) + range, 0 ));

%/mathpiper



%mathpiper_docs,name="Take",categories="Programming Procedures;Lists (Operations)"
*CMD Take --- take a sublist from a list (dropping the rest)
*STD
*CALL
        Take(list, n)
        Take(list, -n)
        Take(list, [m,n])

*PARMS

{list} -- list to act on

{n}, {m} -- positive integers describing the entries to take

*DESC

This command takes a sublist of "list", drops the rest, and returns
the selected sublist. The first calling sequence selects the first
"n" entries in "list". The second form takes the last "n"
entries. The last invocation selects the sublist beginning with entry
number "m" and ending with the "n"-th entry.

*E.G.

In> lst := [_a,_b,_c,_d,_e,_f,_g];
Result: [_a,_b,_c,_d,_e,_f,_g];

In> Take(lst, 2);
Result: [_a,_b];

In> Take(lst, -3);
Result: [_e,_f,_g];

In> Take(lst, [2,4]);
Result: [_b,_c,_d];

*SEE Drop, Select, Remove
%/mathpiper_docs