%mathpiper,def="UnFlatten"

10 ## UnFlatten([], op_ , identity_) <-- identity;
20 ## UnFlatten(list_List?, op_, identity_) <--
     Apply(op,[First(list),UnFlatten(Rest(list),op,identity)]);

%/mathpiper

    %output,sequence="5",timestamp="2013-12-04 14:40:45.095",preserve="false"
      Result: True
.   %/output



%mathpiper_docs,name="UnFlatten",categories="Programming Procedures;Lists (Operations)"
*CMD UnFlatten --- inverse operation of Flatten
*STD
*CALL
        UnFlatten(list,operator,identity)

*PARMS

{list} -- list of objects the operator is to work on

{operator} -- infix operator

{identity} -- identity of the operator

*DESC

UnFlatten is the inverse operation of Flatten. Given
a list, it can be turned into an expression representing
for instance the addition of these elements by calling
UnFlatten with "+" as argument to operator, and 0 as
argument to identity (0 is the identity for addition, since
a+0=a). For multiplication the identity element would be 1.

*E.G.

In> UnFlatten([_a,_b,_c],"+",0)
Result: _a+_b+_c

In> UnFlatten([_a,_b,_c],"*",1)
Result: _a*_b*_c

*SEE Flatten
%/mathpiper_docs