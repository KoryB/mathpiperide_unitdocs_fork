%mathpiper,def="Reverse"

// Non-destructive Reverse operation
Reverse(list):=Reverse!(FlatCopy(list));

%/mathpiper



%mathpiper_docs,name="Reverse",categories="Programming Procedures;Lists (Operations)"
*CMD Reverse --- return the reversed list (without touching the original)
*STD
*CALL
        Reverse(list)

*PARMS

{list} -- list to reverse

*DESC

This procedure returns a list reversed, without changing the
original list. It is similar to {Reverse!}, but safer
and slower.


*E.G.

In> lst:=[_a,_b,_c,13,19]
Result: [_a,_b,_c,13,19];

In> revlst:=Reverse(lst)
Result: [19,13,_c,_b,_a];

In> lst
Result: [_a,_b,_c,13,19];

*SEE FlatCopy, Reverse!
%/mathpiper_docs





%mathpiper,name="Reverse",subtype="automatic_test"

/* Reverse and FlatCopy (and some friends) would segfault in the past if passed a string as argument.
 * I am not opposed to overloading these procedures to also work on strings per se, but for now just
 * check that they return an error in stead of segfaulting.
 */
Verify(ExceptionCatch(Reverse("abc"), "", "Exception"), "Exception");

%/mathpiper