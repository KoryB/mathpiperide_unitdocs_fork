%mathpiper,def="Append!"

Procedure("Append!",["list", "element"])
{
  Insert!(list,Length(list)+1,element);
}

%/mathpiper



%mathpiper_docs,name="Append!",categories="Programming Procedures;Lists (Operations)"
*CMD Append! --- destructively append an entry to a list
*STD
*CALL
        Append!(list, expr)

*PARMS

{list} -- list to append "expr" to

{expr} -- expression to append to the list

*DESC

This is the destructive counterpart of {Append}. This
command yields the same result as the corresponding call to
{Append}, but the original list is modified. So if a
variable is bound to "list", it will now be bound to the list with
the expression "expr" inserted.

Destructive commands run faster than their nondestructive counterparts
because the latter copy the list before they alter it.

*E.G.

In> lst := [_a,_b,_c,_d];
Result: [_a,_b,_c,_d];

In> Append(lst, 1);
Result: [_a,_b,_c,_d,1];

In> lst
Result: [_a,_b,_c,_d];

In> Append!(lst, 1);
Result: [_a,_b,_c,_d,1];

In> lst;
Result: [_a,_b,_c,_d,1];

*SEE Concat, Append
%/mathpiper_docs