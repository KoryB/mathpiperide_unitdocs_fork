%mathpiper,def="BuildList"

/* Juan: TemplateProcedure (as defined in the file "deffunc")
 * also makes the arguments to the procedure local symbols.
 * Use HoldArgumentNumber to specify the index of a variable to hold
 * (since they are defined as local symbols).
 */

TemplateProcedure("BuildList",["body", "var", "countfrom", "countto", "step"])
  {
    MacroLocal(var);
    Local(result,nr,ii);
    result:=[];
    nr := (countto - countfrom) / step;
    ii := 0;
    While( ii <=? nr )
      {
       MacroAssign( var, countfrom + ii * step );
       Insert!( result,1,Eval(body) );
       Assign(ii,AddN(ii,1));
      }
    Reverse!(result);
  }
HoldArgumentNumber("BuildList",5,1); /* body */
HoldArgumentNumber("BuildList",5,2); /* var */
UnFence("BuildList",5);

%/mathpiper



%mathpiper_docs,name="BuildList",categories="Programming Procedures;Lists (Operations)"
*CMD BuildList --- evaluate while some variable ranges over interval
*STD
*CALL
        BuildList(body, var, from, to, step)

*PARMS

{body} -- expression to evaluate multiple times

{var} -- variable to use as loop variable

{from} -- initial value for "var"

{to} -- final value for "var"

{step} -- step size with which "var" is incremented

*DESC

This command generates a list of values from "body", by assigning
variable "var" values from "from" up to "to", incrementing
"step" each time. So, the variable "var" first gets the value
"from", and the expression "body" is evaluated. Then the value
"from"+"step" is assigned to "var" and the expression "body"
is again evaluated. This continues, incrementing "var" with "step"
on every iteration, until "var" exceeds "to". At that moment, all
the results are assembled in a list and this list is returned.

*E.G.
In> BuildList(Factorial(i), i, 1, 9, 1);
Result: [1,2,6,24,120,720,5040,40320,362880]

In> BuildList(i, i, 3, 16, 4);
Result: [3,7,11,15]

In> BuildList(i^2, i, 10, 1, -1);
Result: [100,81,64,49,36,25,16,9,4,1]

The loop variables may not be Constants!
In> BuildList(_a+b, b, 0, 2, 1)
Result: [_a,_a+1,_a+2]

In> BuildList(BuildList(a+b, b, 0, 2, 1), a, 0, 2, 1)
Result: [[0,1,2],[1,2,3],[2,3,4]]

*SEE For, MapSingle, .., TableForm
%/mathpiper_docs