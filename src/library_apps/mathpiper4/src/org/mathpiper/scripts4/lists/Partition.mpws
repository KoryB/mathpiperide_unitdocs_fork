%mathpiper,def="Partition"

/* Partition */

/* Partition( list, n ) partitions 'list' into non-overlapping sublists of length n */

Partition(lst, len):=
        Decide( Length(lst) <? len |? len =? 0, [],
                Concat( [Take(lst,len)], Partition(Drop(lst,len), len) ));

%/mathpiper



%mathpiper_docs,name="Partition",categories="Programming Procedures;Lists (Operations)"
*CMD Partition --- partition a list in sublists of equal length
*STD
*CALL
        Partition(list, n)

*PARMS

{list} -- list to partition

{n} -- length of partitions

*DESC

This command partitions "list" into non-overlapping sublists of
length "n" and returns a list of these sublists. The first "n"
entries in "list" form the first partition, the entries from
position "n+1" up to "2n" form the second partition, and so on. If
"n" does not divide the length of "list", the remaining entries
will be thrown away. If "n" equals zero, an empty list is
returned.

*E.G.

In> Partition([_a,_b,_c,_d,_e,_f,], 2);
Result: [[_a,_b],[_c,_d],[_e,_f]];

In> Partition(1 .. 11, 3);
Result: [[1,2,3],[4,5,6],[7,8,9]];

*SEE Take, PermutationsList
%/mathpiper_docs