%mathpiper,def="Contains?"

Procedure("Contains?",["list", "element"])
{
  Local(result);
  Assign(result,False);
  While(And?(Not?(result), Not?(Equal?(list, []))))
  {
    Decide(Equal?(First(list),element),
      Assign(result, True),
      Assign(list, Rest(list))
      );
  }
  result;
}

%/mathpiper



%mathpiper_docs,name="Contains?",categories="Programming Procedures;Lists (Operations)"
*CMD Contains? --- test whether a list contains a certain element
*STD
*CALL
        Contains?(list, expr)

*PARMS

{list} -- list to examine

{expr} -- expression to look for in "list"

*DESC

This command tests whether "list" contains the expression "expr"
as an entry. It returns {True} if it does and
{False} otherwise. Only the top level of "list" is
examined. The parameter "list" may also be a general expression, in
that case the top-level operands are tested for the occurrence of
"expr".

*E.G.

In> Contains?([_a,_b,_c,_d], _b);
Result: True;

In> Contains?([_a,_b,_c,_d], _x);
Result: False;

In> Contains?([_a,[1,2,3],_z], 1);
Result: False;

In> Contains?(_a*_b, _b);
Result: True;

*SEE Find, Count
%/mathpiper_docs