%mathpiper,def="BubbleSort"

Procedure("BubbleSort",["list", "compare"])
{
  Local(i,j,length,left,right);
  
  //Tell("BubbleSort");
  //Tell("  ",list);
  //Tell("  ",compare);

  list:=FlatCopy(list);
  length:=Length(list);
  //Tell("  ",length);

  For (j:=length,j>?1,j--)
  {
    For(i:=1,i<?j,i++)
    {
      left:=list[i];
      right:=list[i+1];
      //Tell("    ",[i,j,left,right]);
      //Tell("      ",Apply(compare,[left,right]));
      Decide(Not?(Apply(compare,[left,right])),
        {
          Insert!(Delete!(list,i),i+1,left);
        }
      );
      //Tell("   ",list);
    }
  }
  list;
}

%/mathpiper



%mathpiper_docs,name="BubbleSort",categories="Programming Procedures;Sorting"
*CMD BubbleSort --- sort a list
*STD
*CALL
        BubbleSort(list, compare)

*PARMS

{list} -- list to sort

{compare} -- procedure used to compare elements of {list}

*DESC

This command returns {list} after it is sorted using {compare} to
compare elements. The procedure {compare} should accept two arguments,
which will be elements of {list}, and compare them. It should return
{True} if in the sorted list the second argument
should come after the first one, and {False}
otherwise.  The procedure {compare} can either be a string which
contains the name of a procedure or a pure procedure.

The procedure {BubbleSort} uses the so-called "bubble sort" algorithm to do the
sorting by swapping elements that are out of order. This algorithm is easy to
implement, though it is not particularly fast. The sorting time is proportional
to $n^2$ where $n$ is the length of the list.

The original {list} is left unmodified.

*E.G.
In> BubbleSort([4,7,23,53,-2,1], ">?")
Result: [53,23,7,4,1,-2]

In> BubbleSort([3,5,2],Lambda([x,y],x<?y))
Result: [2,3,5]

*SEE HeapSort, Lambda
%/mathpiper_docs