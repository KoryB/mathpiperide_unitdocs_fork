%mathpiper,def="NoneSatisfy?"

10 ## NoneSatisfy?(pred_String?,lst_List?) <-- !? Apply("|?",(MapSingle(pred,lst)));

20 ## NoneSatisfy?(pred_,lst_) <-- True;

%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output

%mathpiper_docs,name="NoneSatisfy?",categories="Programming Procedures;Predicates"

*CMD NoneSatisfy? --- Check if NO element of list [lst] satisfies predicate [pred]

*STD
*CALL
        NoneSatisfy?(pred,lst)

*PARMS

{pred} -- the name of the predicate (as string, with quotes) to be tested

{lst} -- a list

*DESC

The command {NoneSatisfy?} returns {True} if NO element of the list {lst} satisfies 
the predicate {pred}. It returns {False} if at least one element of the list 
satisfies the predicate. It also returns {True} if {lst} is not a list, or if 
{pred} is not a predicate.

*E.G.


In> NoneSatisfy?("NegativeInteger?",[1,0,5])
Result: True

In> NoneSatisfy?("PositiveInteger?",[-1,0,5])
Result: False
    
%/mathpiper_docs
