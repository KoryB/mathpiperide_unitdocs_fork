%mathpiper,def="AllSatisfy?"

10 ## AllSatisfy?(pred_String?,lst_List?) <-- Apply("&?",(MapSingle(pred,lst)));

20 ## AllSatisfy?(pred_,lst_) <-- False;

%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output


%mathpiper_docs,name="AllSatisfy?",categories="Programming Procedures;Predicates"


*CMD AllSatisfy? --- Check if all elements of list [lst] satisfy predicate [pred]

*STD
*CALL
        AllSatisfy?(pred,lst)

*PARMS

{pred} -- the name of the predicate (as string, with quotes) to be tested

{lst} -- a list

*DESC

The command {AllSatisfy?} returns {True} if every element of the list {lst} satisfies the predicate {pred}. 
It returns {False} otherwise. It also returns {False} if {lst} is not a list, or if {pred} is not a predicate.

*E.G.


In> AllSatisfy?("Integer?",[1,0,-5])
Result> True 

In> AllSatisfy?("PositiveInteger?",[1,0,-5])
Result> False
    
%/mathpiper_docs
