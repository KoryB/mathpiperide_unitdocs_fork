%mathpiper,def="HasProcedure?"

/// HasProcedure? --- test for an expression containing a procedure
/// procedure name given as string.
10 ## HasProcedure?(expr_, string_String?) <-- HasProcedure?(expr, ToAtom(string));

/// procedure given as atom.
// atom contains no procedures
10 ## HasProcedure?(expr_Atom?, atom_Atom?) <-- False;

// a list contains the procedure List so we test it together with procedures
// a procedure contains itself, or maybe an argument contains it
20 ## HasProcedure?(expr_Procedure?, atom_Atom?) <-- Equal?(First(ProcedureToList(expr)), atom) |? ListHasProcedure?(Rest(ProcedureToList(expr)), atom);

%/mathpiper



%mathpiper_docs,name="HasProcedure?",categories="Programming Procedures;Predicates"
*CMD HasProcedure? --- check for expression containing a procedure
*STD
*CALL
        HasProcedure?(expr, func)

*PARMS

{expr} -- an expression

{func} -- a procedure atom to be found

*DESC

The command {HasProcedure?} returns {True} if the expression {expr} contains 
a procedure {func}. The expression is recursively traversed.

Note that since the operators "{+}" and "{-}" are prefix as well as infix 
operators, it is currently required to use {ToAtom("+")} to obtain the 
unevaluated atom "{+}".

*E.G.

In> HasProcedure?(x+y*Cos(Ln(z)/z), Ln)
Result: True;

In> HasProcedure?(x+y*Cos(Ln(z)/z), Sin)
Result: False;

*SEE HasProcedureArithmetic?, HasProcedureSome?, ProcedureList, VarList, HasExpression?
%/mathpiper_docs





%mathpiper,name="HasProcedure?",subtype="automatic_test"

Verify(HasProcedure?(_a*_b+1,"*"),True);
Verify(HasProcedure?(_a+Sqrt(_b*_c),"Sqrt"),True);
Verify(HasProcedure?(_a*_b+1,"Lis"),False);

RulebaseHoldArguments("f",["a"]);
Verify(HasProcedure?('a*'b+f(['b,'c]),"List"),True);
Retract("f",All);

%/mathpiper
