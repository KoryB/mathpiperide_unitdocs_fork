%mathpiper,def="HasExpression?"

/// HasExpression? --- test for an expression containing a subexpression
/// for checking dependence on variables, this may be faster than using VarList or FreeOf? and this also can be used on non-variables, e.g. strings or numbers or other atoms or even on non-atoms
// an expression contains itself -- check early
10 ## HasExpression?(expr_, atom_):: Equal?(expr, atom) <-- True;
// an atom contains itself
15 ## HasExpression?(expr_Atom?, atom_) <-- Equal?(expr, atom);
// a list contains an atom if one element contains it
// we test for lists now because lists are also procedures
// first take care of the empty list:
19 ## HasExpression?([], atom_) <-- False;
20 ## HasExpression?(expr_List?, atom_) <-- HasExpression?(First(expr), atom) |? HasExpression?(Rest(expr), atom);
// a procedure contains an atom if one of its arguments contains it
30 ## HasExpression?(expr_Procedure?, atom_) <-- HasExpression?(Rest(ProcedureToList(expr)), atom);

%/mathpiper



%mathpiper_docs,name="HasExpression?",categories="Programming Procedures;Predicates"
*CMD HasExpression? --- check for expression containing a subexpression
*STD
*CALL
        HasExpression?(expr, x)

*PARMS

{expr} -- an expression

{x} -- a subexpression to be found



*DESC

The command {HasExpression?} returns {True} if the expression {expr} 
contains a literal subexpression {x}. The expression is recursively traversed.

Note that since the operators "{+}" and "{-}" are prefix as well as 
infix operators, it is currently required to use {ToAtom("+")} to obtain 
the unevaluated atom "{+}".

*E.G.

In> HasExpression?(_x+_y*Cos(Ln(_z)/_z), _z)
Result: True;

In> HasExpression?(_x+_y*Cos(Ln(_z)/_z), Ln(_z))
Result: True;

In> HasExpression?(_x+_y*Cos(Ln(_z)/_z), _z/Ln(_z))
Result: False;

In> HasExpression?(_x+_y*Cos(Ln(_z)/_z), Ln(_z)/_z)
Result: True

In> HasExpression?(_x+_y*Cos(Ln(_z)/_z), _z)
Result: True

In> HasExpression?(_x+_y*Cos(Ln(_z)/_z), 1/_z)
Result: False


*SEE HasExpressionArithmetic?, HasExpressionSome?, ProcedureList, VarList, HasProcedure?
%/mathpiper_docs



%mathpiper,name="HasExpression?",subtype="automatic_test"

Verify(HasExpression?(_a*_b+1,1),True);
Verify(HasExpression?(_a+Sqrt(_b*_c),_c),True);
Verify(HasExpression?(_a*_b+1,2),False);
Verify(HasExpressionArithmetic?(_a*_b+1,ToAtom("+")),False);
Verify(HasExpressionArithmetic?(_a*_b+1,1),True);
Verify(HasExpressionArithmetic?(_a+Sqrt(_b*_c),_c),False);
Verify(HasExpressionArithmetic?(_a+Sqrt(_b*_c),Sqrt(_b*_c)),True);

RulebaseHoldArguments("f",["a"]);
Verify(HasExpression?(_a*_b+f([_b,_c]),'f),False);
Verify(HasExpressionArithmetic?(_a*_b+f([_b,_c]),_c),False);
Retract("f",All);

%/mathpiper