%mathpiper,def="HasExpressionSome?"

/// Same except only look at procedure arguments for procedures in a given list
HasExpressionSome?(expr_, atom_, looklist_):: Equal?(expr, atom) <-- True;
// an atom contains itself
15 ## HasExpressionSome?(expr_Atom?, atom_, looklist_) <-- Equal?(expr, atom);
// a list contains an atom if one element contains it
// we test for lists now because lists are also procedures
// first take care of the empty list:
19 ## HasExpressionSome?([], atom_, looklist_) <-- False;
20 ## HasExpressionSome?(expr_List?, atom_, looklist_) <-- HasExpressionSome?(First(expr), atom, looklist) |? HasExpressionSome?(Rest(expr), atom, looklist);
// a procedure contains an atom if one of its arguments contains it
// first deal with procedures that do not belong to the list: return False since we have already checked it at #15
25 ## HasExpressionSome?(expr_Procedure?, atom_, looklist_)::(!? Contains?(looklist, ToAtom(Type(expr)))) <-- False;
// a procedure contains an atom if one of its arguments contains it
30 ## HasExpressionSome?(expr_Procedure?, atom_, looklist_) <-- HasExpressionSome?(Rest(ProcedureToList(expr)), atom, looklist);

%/mathpiper



%mathpiper_docs,name="HasExpressionSome?",categories="Programming Procedures;Predicates"
*CMD HasExpressionSome? --- check for expression containing a subexpression
*STD
*CALL
        HasExpressionSome?(expr, x, list)

*PARMS

{expr} -- an expression

{x} -- a subexpression to be found

{list} -- list of procedure atoms to be considered "transparent"

*DESC

The command {HasExpressionSome?} does the same as {HasExpression?}, except 
it only looks at arguments of a given {list} of procedures. All other procedures 
become "opaque" (as if they do not contain anything).

Note that since the operators "{+}" and "{-}" are prefix as well as infix 
operators, it is currently required to use {ToAtom("+")} to obtain the unevaluated atom "{+}".

*E.G.

In> HasExpressionSome?([a+b*2,c/d],c/d,[List])
Result: True;

In> HasExpressionSome?([a+b*2,c/d],c,[List])
Result: False;

*SEE HasExpression?, HasExpressionArithmetic?, ProcedureList, VarList, HasProcedure?
%/mathpiper_docs