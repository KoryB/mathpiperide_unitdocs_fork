%mathpiper,def="HasProcedureArithmetic?"

/// Analyse arithmetic expressions

HasProcedureArithmetic?(expr, atom) := HasProcedureSome?(expr, atom, ["+", "-", "*", "/"]);

%/mathpiper



%mathpiper_docs,name="HasProcedureArithmetic?",categories="Programming Procedures;Predicates"
*CMD HasProcedureArithmetic? --- check for expression containing a procedure
*STD
*CALL
        HasProcedureArithmetic?(expr, func)

*PARMS

{expr} -- an expression

{func} -- a procedure atom to be found

*DESC

{HasProcedureArithmetic?} is defined through {HasProcedureSome?} to look 
only at arithmetic operations {+}, {-}, {*}, {/}.

Note that since the operators "{+}" and "{-}" are prefix as well as infix 
operators, it is currently required to use {ToAtom("+")} to obtain the 
unevaluated atom "{+}".

*E.G.

In> HasProcedureArithmetic?(x+y*Cos(Ln(x)/x), Cos)
Result: True;

In> HasProcedureArithmetic?(x+y*Cos(Ln(x)/x), Ln)
Result: False;

*SEE HasProcedure?, HasProcedureSome?, ProcedureList, VarList, HasExpression?
%/mathpiper_docs






%mathpiper,name="HasProcedureArithmetic?",subtype="automatic_test"

Verify(HasProcedureArithmetic?(_a*_b+1,"+"),True);
Verify(HasProcedureArithmetic?(_a+Sqrt(_b*_c),"*"),False);
Verify(HasProcedureArithmetic?(_a+Sqrt(_b*_c),'Sqrt),True);

RulebaseHoldArguments("f",["a"]);
Verify(HasProcedureArithmetic?(_a*_b+f([_b,_c]),'List),False);
Retract("f",All);

%/mathpiper