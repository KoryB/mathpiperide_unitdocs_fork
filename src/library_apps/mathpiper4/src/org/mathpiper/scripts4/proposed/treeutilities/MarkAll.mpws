%mathpiper,def="MarkAll;MarkAllAtoms"

MarkAllAtoms(expression, color) :=
{   
    Local(index, newList);
    
    If(Atom?(expression))
    {
        MetaSet(MetaSet(expression, "HighlightColor", color), "HighlightNodeShape", "RECTANGLE");
    }
    Else
    {    
        newList := [];
        
        For(index := 0, index <=? Length(expression), index++)
        {
            newList := Append(newList, MarkAllAtoms(expression[index], color));
        }
        
        ListToProcedure(newList);
    }
}

// Highlight all nodes in an expression.
MarkAll(expression, position, pattern, color) :=
{
    Local(list, result);
    
    list := [
        ["function",
            Lambda([trackingList,positionString,node], 
            {
                node := MarkAllAtoms(node, color);
            })
        ]
    ];
    
    TreeProcess(expression, pattern, list, Position:position);
}

%/mathpiper





%mathpiper_docs,name="MarkAll",categories="Programming Procedures;Expression Trees"
*CMD MarkAll --- highlights a subtree of an expression tree at the given position if it matches the given pattern

*CALL
    MarkAll(expression, position, pattern, color)

*PARMS

{expression} -- expression tree to be highlighted

{position} -- position of the subtree

{pattern} -- pattern to match against subtrees

{color} -- the highlighting color

*DESC

Highlights a subtree of an expression tree at the
given position if it matches the given pattern.
Highlighting metadata is applied to the
dominant node and all nodes that are underneath it.
This is useful for expression viewers other than
the tree viewer.

*E.G.
In> result := MarkAll('(1+2+3), "1", a_ + b_, "BLUE")
Result: (1 + 2) + 3

In> MetaEntries(result[1][0])
Result: ["HighlightNodeShape":"RECTANGLE","HighlightColor":"BLUE"]

*SEE HighlightPattern, Mark
%/mathpiper_docs





%mathpiper,name="MarkAll",subtype="automatic_test"

{
    Local(result);
    
    result := MarkAll('(1+2+3), "1", a_ + b_, "BLUE");
    
    Verify(MetaGet(result[1][0], "HighlightColor"), "BLUE");
    
    Verify(MetaGet(result[1][1], "HighlightColor"), "BLUE");
    
    Verify(MetaGet(result[1][2], "HighlightColor"), "BLUE");
}

%/mathpiper