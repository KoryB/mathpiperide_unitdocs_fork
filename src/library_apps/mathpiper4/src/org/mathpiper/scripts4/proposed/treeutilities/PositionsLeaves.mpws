%mathpiper,def="PositionsLeaves"

PositionsLeaves(expression) :=
{
    Local(stack, positions, subexpressionAndPosition, subexpression, index, argumentsCount);
    
    stack := [[expression, []]];
    
    positions := [];
    
    While(Length(stack) !=? 0)
    {
        subexpressionAndPosition := PopFront(stack);
        
        subexpression := First(subexpressionAndPosition);                
        
        If(!? Atom?(subexpression) &? (argumentsCount := ArgumentsCount(subexpression)) >? 0)
        {
            index := 1;
            
            While(index <=? argumentsCount)
            {
                Append!(stack, [subexpression[index], Append!(FlatCopy(subexpressionAndPosition[2]), index)]);
                
                index++;
            }
        }
        Else
        {
            Append!(positions, ListToString(subexpressionAndPosition[2], ","));
        }
        
        positions;
    }
}

%/mathpiper





%mathpiper_docs,name="PositionsLeaves",categories="Programming Procedures;Expression Trees"
*CMD PositionsLeaves --- return the positions of all the leaves in an expression tree

*CALL
    PositionsLeaves(expression)

*PARMS

{expression} -- an expression

*DESC

Return the positions of all the leaves in an expression tree.

*E.G.
In> PositionsLeaves('(2 + (_b*_c) / _b - 3))
Result: ["2","1,1","1,2,2","1,2,1,1","1,2,1,2"]
%/mathpiper_docs





%mathpiper,name="PositionsLeaves",subtype="automatic_test"

Verify(PositionsLeaves('(2 + (_b*_c) / _b - 3)), ["2","1,1","1,2,2","1,2,1,1","1,2,1,2"]);

%/mathpiper
