%mathpiper,def="PositionGet"

PositionGet(expression, position) :=
{
    Local(positionList, lastPosition, );
    
    If(String?(position))
    {
        positionList := PositionStringToList(position);
    }
    Else If(List?(position))
    {
        positionList := position;
    }
    Else
    {
        Check(False, "The second argument must be a string or a list.");
    }
    
    If(Length(positionList) >? 0)
    {
        ForEach(argumentPosition, positionList)
        {
            expression := Nth(expression, argumentPosition);
        }
        
        expression;
    }
    Else
    {
        expression;
    }
}

%/mathpiper





%mathpiper_docs,name="PositionGet",categories="Programming Procedures;Expression Trees"
*CMD PositionGet --- return the subexpression at the given position in the given expression

*CALL
    PositionGet(expression, position)

*PARMS

{expression} -- an expression

{predicate} -- an expression tree position in string or list form

*DESC

return the subexpression at the given position in the given expression.

*E.G.
In> PositionGet('(2 + (_b*_c) / _b - 3), "1,2")
Result: (_b*_c)/_b

In> PositionGet('(2 + (_b*_c) / _b - 3), [1,2])
Result: (_b*_c)/_b

*SEE SubstitutePosition
%/mathpiper_docs





%mathpiper,name="PositionGet",subtype="automatic_test"

Verify(PositionGet('(2 + (_b*_c) / _b - 3), "1,2"), (_b*_c)/_b);
Verify(PositionGet('(2 + (_b*_c) / _b - 3), [1,2]), (_b*_c)/_b);

%/mathpiper
