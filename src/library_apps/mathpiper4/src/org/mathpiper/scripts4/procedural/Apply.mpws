%mathpiper,def="Apply"

10 ## Apply(applyoper_,applyargs_):: (Or?(String?(applyoper), List?(applyoper))) <-- ApplyFast(applyoper,applyargs);
20 ## Apply(applyoper_Atom?,applyargs_) <-- ApplyFast(ToString(applyoper),applyargs);

30 ## Apply(Lambda(args_,body_),applyargs_) <-- `ApplyFast(Hold([@args,@body]),applyargs);
UnFence("Apply",2);

%/mathpiper



%mathpiper_docs,name="Apply",categories="Programming Procedures;Functional Operators"
*CMD Apply --- apply a procedure to arguments
*STD
*CALL
        Apply(fn, arglist)

*PARMS

{fn} -- procedure to apply

{arglist} -- list of arguments

*DESC

This procedure applies the procedure "fn" to the arguments in
"arglist" and returns the result. The first parameter "fn" can
either be a string containing the name of a procedure or a pure
procedure. Pure procedures, modeled after lambda-expressions, have the
form "{varlist,body}", where "varlist" is the list of formal
parameters. Upon application, the formal parameters are assigned the
values in "arglist" (the second parameter of {Apply}) and the "body" is evaluated.

Another way to define a pure procedure is with the Lambda construct.
Here, instead of passing in "{varlist,body}", one can pass in
"Lambda(varlist,body)". Lambda has the advantage that its arguments
are not evaluated (using lists can have undesirable effects because
lists are evaluated). Lambda can be used everywhere a pure procedure
is expected, in principle, because the procedure Apply is the only procedure
dealing with pure procedures. So all places where a pure procedure can
be passed in will also accept Lambda.

An shorthand for {Apply} is provided by the {@} operator.

*E.G.
In> Apply("+", [5,9]);
Result: 14;

In> Apply('[[x,y], x-y^2], [Cos(_a), Sin(_a)]);
Result: y('[[x,y], x-y^2], [Cos(_a), Sin(_a)]);
Result: Cos(_a)-Sin(_a)^2;

In>  Apply(Lambda([x,y], x-y^2), [Cos(_a), Sin(_a)]);
Result: Cos(_a)-Sin(_a)^2

In>  Lambda([x,y], x-y^2) @ [Cos(_a), Sin(_a)]
Result: Cos(_a)-Sin(_a)^2

*SEE Map, MapSingle, @, Lambda
%/mathpiper_docs




%mathpiper,name="Apply",subtype="automatic_test"

Verify(Apply("+",[2,3]),5);
{
  Local(x,y);
  Verify(Apply('[[x,y],x+y],[2,3]),5);
  Verify(Apply(Lambda([x,y],x+y),[2,3]),5);

  /* Basically the next line is to check that [[x],Length(x)]
   * behaves in an undesirable way (Length being evaluated
   * prematurely), so that the next test can then check that
   * Lambda solves the problem.
   */
  Verify(Apply('[[x],Length(x)],["aaa"]),3);
  Verify(Apply(Lambda([x],Length(x)),["aaa"]),3);

  Verify(_x,_x);
  Verify(_y,_y);

  Testing("ThreadingListables");
  x:=[_bb,_cc,_dd];
  Verify(Sqrt(_aa*x),[Sqrt(_aa*_bb),Sqrt(_aa*_cc),Sqrt(_aa*_dd)]);
}

%/mathpiper





%mathpiper,name="Apply",subtype="manual_test"
//todo:tk:switching to manual_test for the minimal version of the scripts.
LocalSymbols(f,x,n)
{
  RulebaseHoldArguments("f",["n"]);

  f(n_) <-- Apply("Differentiate",[x,n, x^n]);

  Verify(f(10),(10!));

  Retract("f",All);
}

%/mathpiper