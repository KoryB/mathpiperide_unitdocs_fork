%mathpiper,def="++"

Procedure("++",["aVar"])
{
    Local(oldVar);
    
    oldVar := Eval(aVar);
    
    MacroAssign(aVar, AddN(Eval(aVar), 1));
   
    oldVar;
}

UnFence("++",1);

HoldArgument("++","aVar");

%/mathpiper




%mathpiper_docs,name="++",categories="Operators"
*CMD ++ --- increment variable
*STD
*CALL
        var++

*PARMS

{var} -- variable to increment

*DESC

The variable with name "var" is incremented, i.e. the number 1 is
added to it. The expression {x++} is equivalent to
the assignment {x := x + 1}.

*E.G.
In> x := 5;
Result: 5;

In> x++;
Result: 6;

In> x;
Result: 6;

*SEE --, :=
%/mathpiper_docs