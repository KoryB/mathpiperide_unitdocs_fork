%mathpiper,def="ArgumentsCount"

Procedure("ArgumentsCount",["aLeft"]) 
{
    Check(!? Atom?(aLeft), "The argument must not be an atom.");
    
    Length(ProcedureToList(aLeft))-1;
}

%/mathpiper




%mathpiper_docs,name="ArgumentsCount",categories="Programming Procedures;Miscellaneous"
*CMD ArgumentsCount --- return number of top-level arguments
*STD
*CALL
        ArgumentsCount(expr)

*PARMS

{expr} -- expression to examine

*DESC

This procedure evaluates to the number of top-level arguments of the
expression "expr". The argument "expr" may not be an atom, since
that would lead to an error.

*E.G.
In> ArgumentsCount(f(_a,3,Pi)) //If f is a 'known' procedure
Result: 3;

In> ArgumentsCount(Sin(_x));
Result: 1;

In> ArgumentsCount(_a*(_b+_c));
Result: 2;

*SEE Type, Length
%/mathpiper_docs
