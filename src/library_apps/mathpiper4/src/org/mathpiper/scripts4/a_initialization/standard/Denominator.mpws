%mathpiper,def="Denominator"

1 ## Denominator(x_ / y_)      <-- y;
1 ## Denominator(-x_/ y_)      <-- y;
2 ## Denominator(x_Number?)   <-- 1;

%/mathpiper

 


%mathpiper_docs,name="Denominator",categories="Mathematics Procedures;Numbers (Operations)"
*CMD Denominator --- denominator of an expression
*STD
*CALL
        Denominator(expr)

*PARMS

{expr} -- expression to determine denominator of

*DESC

This procedure determines the denominator of the rational expression
"expr" and returns it. As a special case, if its argument is numeric
but not rational, it returns {1}. If "expr" is
neither rational nor numeric, the procedure returns unevaluated.

*E.G.

In> Denominator(2/7)
Result: 7;

In> Denominator(_a / _x^2)
Result: _x^2;

In> Denominator(-_a / _x^2)
Result: _x^2

In> Denominator(5)
Result: 1;

*File: ../src/org/mathpiper/scripts4/a_initialization/standard/Denominator.mpw

*SEE Numerator, Rational?, Number?
%/mathpiper_docs



