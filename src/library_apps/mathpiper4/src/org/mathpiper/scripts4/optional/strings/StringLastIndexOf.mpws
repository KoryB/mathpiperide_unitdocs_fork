%mathpiper,def="StringLastIndexOf"

StringLastIndexOf(string, substring) :=
{
    Local(javaString, index);
    javaString := JavaNew("java.lang.String", string);
    index := JavaAccess(javaString,"lastIndexOf", substring) + 1;
    Decide(index =? 0, -1, index);
}


StringLastIndexOf(string, substring, fromIndex) :=
{
    Local(javaString, index);
    javaString := JavaNew("java.lang.String", string);
    index := JavaAccess(javaString,"lastIndexOf", substring, fromIndex - 1) + 1;
    Decide(index =? 0, -1, index);
}

%/mathpiper







%mathpiper_docs,name="StringLastIndexOf",categories="Programming Procedures;Strings",access="experimental"
*CMD StringLastIndexOf --- locate the last index of a substring within a given string. 
*CALL

StringLastIndexOf(string, substring)
StringLastIndexOf(string, substring, fromIndex)

*PARMS
{string} -- a string of characters

{substring} -- a character or string of characters

{fromIndex} -- the index to start the backwards search from


*DESC
This procedure returns the index of the last
occurrence of the given substring.

If a {fromIndex} is given, the backwards search
begins at this index.

If the given substring cannot be found, the
procedure will return -1.

*E.G.
In> StringLastIndexOf("Hello", "l")
Result: 4

In> StringLastIndexOf("abcdcfg", "c", 4)
Result: 3

In> StringLastIndexOf("Hello", "G")
Result: -1
%/mathpiper_docs





%mathpiper,name="StringLastIndexOf",subtype="automatic_test"

Verify(StringLastIndexOf("Hello", "l"), 4);

Verify(StringLastIndexOf("abcdcfg", "c", 4), 3);

%/mathpiper
