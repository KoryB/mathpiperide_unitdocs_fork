%mathpiper,def="***;Factorialpartial"

/// partial factorial
n1_RationalOrNumber? *** n2_RationalOrNumber? <--
{
        Check(n2-n1 <=? 65535, "Partial factorial: Error: the range " + ( PipeToString() Write(n2-n1) ) + " is too large, you may want to avoid exact calculation");
        Decide(n2-n1<?0,
                1,
                Factorialpartial(n1, n2)
        );
}

/// recursive routine to evaluate "partial factorial" a*(a+1)*...*b
// TODO lets document why the >>1 as used here is allowed (rounding down? What is the idea behind this algorithm?)
2 ## Factorialpartial(a_, b_)::(b-a>=?4) <-- Factorialpartial(a, a+((b-a)>>1)) * Factorialpartial(a+((b-a)>>1)+1, b);
3 ## Factorialpartial(a_, b_)::(b-a>=?3) <-- a*(a+1)*(a+2)*(a+3);
4 ## Factorialpartial(a_, b_)::(b-a>=?2) <-- a*(a+1)*(a+2);
5 ## Factorialpartial(a_, b_)::(b-a>=?1) <-- a*(a+1);
6 ## Factorialpartial(a_, b_)::(b-a>=?0) <-- a;
%/mathpiper




%mathpiper_docs,name="***",categories="Operators"
*CMD ***  --- partial factorial operator

*CALL
        a *** b

*PARMS
{a}, {b} -- numbers

*DESC
The "partial factorial" procedure {a *** b} calculates the product $a*(a+1)*...$ 
which is terminated at the least integer not greater than $b$. The arguments $a$ 
and $b$ do not have to be integers; for integer arguments, {a *** b} = $b! / (a-1)!$. 
This procedure is sometimes a lot faster than evaluating the two factorials, 
especially if $a$ and $b$ are close together. If $a>b$ the procedure returns $1$.

The factorial procedures terminate and print an error message if the arguments are too large (currently the limit 
is $n < 65535$) because exact factorials of such large numbers are computationally expensive and most probably 
not useful. One can call {InternalLnGammaNum()} to evaluate logarithms of such factorials to desired precision.

*E.G.
In> 1/3 *** 10;
Result: 17041024000/59049;

*SEE BinomialCoefficient, Product, Gamma, Subfactorial
%/mathpiper_docs