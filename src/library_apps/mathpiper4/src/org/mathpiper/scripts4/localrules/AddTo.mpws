%mathpiper,def="AddTo"

// (a or b) and (c or d) -> (a and c) or (a and d) or (b and c) or (b and d)
20 ## (list_List? AddTo rest_) <--
{
  Local(res);
  res:=[];
  ForEach(item,list)
  {
    res := Concat(res,item AddTo rest);
  }
  res;
}
30 ## (aitem_ AddTo list_List?) <--
{
  MapSingle([[orig],aitem &? orig],list);
}
40 ## (aitem_ AddTo b_) <-- aitem &? b;

%/mathpiper



%mathpiper_docs,name="AddTo",categories="Mathematics Procedures;Solvers (Symbolic)"
*CMD AddTo --- add an equation to a set of equations or set of set of equations
*STD
*CALL
        eq1 AddTo eq2

*PARMS

{eq} - (set of) set of equations

*DESC

Given two (sets of) sets of equations, the command AddTo combines
multiple sets of equations into one. 

A list {a,b} means that a is a solution, OR b is a solution.
AddTo then acts as a AND operation:

        (a or b) and (c or d) => 
        (a or b) Addto (c or d) => 
        (a and c) or (a and d) or (b and c)
          or (b and d)

This procedure is useful for adding an identity to an already
existing set of equations. Suppose a solve command returned
{a>=?0 &? x==a,a<?0 &? x== -a} from an expression x==Abs(a),
then a new identity a==2 could be added as follows:

In> a==2 AddTo {a>=?0 &? x==a,a<?0 &? x== -a}
Result: {a==2 &? a>=?0 &? x==a,a==2 &? a<?0 &? x== -a}

Passing this set of set of identities back to solve, solve
should recognize that the second one is not a possibility
any more, since a==2 &? a<?0 can never be true at the same time.

This operator can help the user to program in the style of functional 
programming languages such as Miranda or Haskell.

*E.G.

In> [A==2,c==d] AddTo [b==3 &? d==2]
Result: [A==2 &? b==3 &? d==2,c==d &? b==3 &? d==2];

In> [A==2,c==d] AddTo [b==3, d==2]
Result: [A==2 &? b==3,A==2 &? d==2,c==d &? b==3,c==d &? d==2];

*SEE Where, Solve
%/mathpiper_docs