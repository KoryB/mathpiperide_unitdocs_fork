%mathpiper,def="LogicTest;TrueFalse"

/* LogicTest compares the truth tables of two expressions. */
LocalSymbols(TrueFalse)
{
  10 ## TrueFalse(var_Atom?, expr_) <-- `[(@expr) Where (@var)==False,(@expr) Where (@var)==True];
  20 ## TrueFalse([], expr_) <-- `(@expr);
  30 ## TrueFalse(var_List?, expr_) <--
  `{ 
    Local(t,h);
    Assign(h,First(@var));
    Assign(t,Rest(@var));
    TrueFalse(h,TrueFalse(t,@expr));
  }
  
  HoldArgument("TrueFalse","var");
  HoldArgument("TrueFalse","expr");

  Macro("LogicTest",[ "vars", "expr1", "expr2"]) Verify(TrueFalse((@vars),(@expr1)), TrueFalse((@vars),(@expr2)));
}

%/mathpiper





%mathpiper_docs,name="LogicTest",categories="Programming Procedures;Testing"
*CMD LogicTest --- verifying equivalence of two expressions
*STD
*CALL
        LogicTest(variables,expr1,expr2)

*PARMS

{variables} -- list of variables

{exprN} -- Some boolean expression

*DESC

The command {LogicTest} can be used to verify that an
expression is <I>equivalent</I> to  a correct answer after evaluation.
It returns {True} or {False}.


*E.G.

In> LogicTest([A,B,C],Not?((!? A) &? (!? B)),A |? B)
Result: True

In> LogicTest([A,B,C],Not?((!? A) &? (!? B)),A |? C)
        ******************
        CommandLine: 1

        $TrueFalse4([A,B,C],Not?(!? A &? !? B))
         evaluates to
        [[[False,False],[True,True]],[[True,True],[True,True]]]
         which differs from
        [[[False,True],[False,True]],[[True,True],[True,True]]]
        ******************
Result: False

*SEE KnownFailure, Verify, TestMathPiper, LogicVerify

%/mathpiper_docs

*SEE Simplify, CanProve




%mathpiper,name="LogicTest",subtype="automatic_test"

LogicTest([_A],_A &? _A,_A);
LogicTest([_A],_A &? True, _A);
LogicTest([_A],_A &? False,False);
LogicTest([_A],_A |? True, True);
LogicTest([_A],_A |? False,_A);
LogicTest([_A],_A |? !? _A,True);
LogicTest([_A],_A &? !? _A,False);
LogicTest([_A,_B],(_A &? _B) |? (_A &? _B), _A &? _B);
LogicTest([_A,_B],_A |? (_A &? _B), _A &? (_A |? _B));
LogicTest([_A,_B],(_A &? _B) &? _A,(_A &? _B) &? _A);
LogicTest([_A,_B],!? (_A &? _B) &? _A,(!? _A |? !? _B) &? _A);
LogicTest([_A,_B],(_A |? _B) &? !? _A,_B &? !? _A);
LogicTest([_A,_B,_C],(_A |? _B) &? (!? _A |? _C), (_A |? _B) &? (_C |? !? _A));
LogicTest([_A,_B,_C],(_B |? _A) &? (!? _A |? _C), (_A |? _B) &? (_C |? !? _A));
LogicTest([_A,_B,_C], _A &? (_A |? _B |? _C), _A);
LogicTest([_A,_B,_C], _A &? (!? _A |? _B |? _C),_A &? (_B |? _C));

%/mathpiper