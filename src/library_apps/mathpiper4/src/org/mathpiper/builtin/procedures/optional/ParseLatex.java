/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.optional;

import java.util.HashMap;
import java.util.Map;


import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.ui.gui.worksheets.latexparser.ConsStack;
import org.mathpiper.ui.gui.worksheets.latexparser.TexParser;
import org.scilab.forge.mp.jlatexmath.DefaultTeXFont;
import org.scilab.forge.mp.jlatexmath.cyrillic.CyrillicRegistration;
import org.scilab.forge.mp.jlatexmath.greek.GreekRegistration;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;

/**
 *
 *
 */
public class ParseLatex extends BuiltinProcedure {

    private Map defaultOptions;
    
    public void plugIn(Environment aEnvironment)  throws Throwable
    {
	this.functionName = "ParseLatex";
	
        aEnvironment.getBuiltinFunctions().put("ParseLatex", new BuiltinProcedureEvaluator(this, 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));

        defaultOptions = new HashMap();
        defaultOptions.put("Scale", 40.0);
        defaultOptions.put("Resizable", false);


        DefaultTeXFont.registerAlphabet(new CyrillicRegistration());
	DefaultTeXFont.registerAlphabet(new GreekRegistration());


    }//end method.

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {
	
        Cons arguments = getArgument(aEnvironment, aStackTop, 1);

        if(! Utility.isSublist(arguments)) LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");

        arguments = (Cons) arguments.car(); //Go to sub list.

        arguments = arguments.cdr(); //Strip List tag.

        //if(! Utility.isList(arguments)) LispError.throwError(aEnvironment, aStackTop, LispError.NOT_A_LIST, "");

        Object latexStringObject = arguments.car();
        
        if(! (latexStringObject instanceof String)) LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");

        String latexString = (String) latexStringObject;
        
        
        Cons options = arguments.cdr();

        Map userOptions = Utility.optionsListToJavaMap(aEnvironment, aStackTop, options, defaultOptions);
        

        latexString = Utility.stripEndQuotesIfPresent(latexString);

        latexString = Utility.stripEndDollarSigns(latexString);
        
        TexParser parser = new TexParser(new ConsStack(), true);
        
        Cons expression = (Cons) parser.parse(latexString);

        setTopOfStack(aEnvironment, aStackTop, expression);


    }//end method.


}//end class.





/*
%mathpiper_docs,name="ParseLatex",categories="Programming Procedures;Input/Output;Built In"
*CMD ParseLatex --- parse LaTeX strings

*CALL
    ParseLatex(string)

*PARMS
{string} -- a string that contains a LaTeX expression

*DESC
Parses a string that contains a LaTeX expresion.
 
*E.G.
In> ParseLatex("2 \\times 2")
Result: 2*2

%/mathpiper_docs



%mathpiper,name="ParseLatex",subtype="automatic_test"

Verify(ParseLatex("2^3^4*6"), '( 2^3^4*6 ));
KnownFailure( Verify(ParseLatex("2^3^4^5*6"), '( 2^3^4^5*6 )) );
Verify(ParseLatex("(1-7n)"), '( (1-7*n) ));
Verify(ParseLatex("1 + 2"), '(1+2));
Verify(ParseLatex("1 + 2 + 3"), '( (1 + 2) + 3));
Verify(ParseLatex("1 + -2"), '( 1 + (-2) ));
Verify(ParseLatex("5n + 34= - 2( 1- 7n )"), '( 5*n + 34== - 2*( 1- 7*n ) ));
Verify(ParseLatex("3 = -2(1+2)"), '( 3 == -2*(1+2) ));
Verify(ParseLatex("5n + 34 = ( 1- 7n )"), '( 5*n + 34 == ( 1- 7*n ) ));
Verify(ParseLatex("{1-7n}"), '( 1-7*n ));
Verify(ParseLatex("\\sqrt{12 * 3}"), '( Sqrt(12 * 3) ));
Verify(ParseLatex("2x"), '( 2*x ));
Verify(ParseLatex("1*2*3*4"), '( 1*2*3*4 ));
Verify(ParseLatex("abcd"), '( a*b*c*d ));

%/mathpiper

*/



