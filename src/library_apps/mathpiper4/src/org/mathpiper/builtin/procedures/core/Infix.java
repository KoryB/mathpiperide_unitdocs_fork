/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.Environment;

/**
 *
 *  
 */
public class Infix extends BuiltinProcedure
{

    private Infix()
    {
    }

    public Infix(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Utility.multiFix(aEnvironment, aStackTop, aEnvironment.iInfixOperators);
    }
}



/*
%mathpiper_docs,name="Infix",categories="Programming Procedures;Built In"
*CMD Infix --- define procedure syntax (infix operator)
*CORE
*CALL
	Infix("op")
	Infix("op", precedence)

*PARMS

{"op"} -- string, the name of a procedure

{precedence} -- nonnegative integer (evaluated)

*DESC

Declares a special syntax for the procedure to be parsed as an  infix operator.

"Infix" procedures must have two arguments and are syntactically placed between their arguments.
Names of infix procedures can be arbitrary, although for reasons of readability they are usually made of non-alphabetic characters.

Function name can be any string but meaningful usage and readability would
require it to be either made up entirely of letters or entirely of non-letter
characters (such as "+", ":" etc.).
Precedence is optional (will be set to 0 by default).

*E.G.
In> Infix("###", 5)
Result: True;

In> '(_a ### _b)
Result: _a ### _b

*SEE Bodied?, PrecedenceGet, Bodied, Postfix, Prefix
%/mathpiper_docs
*/