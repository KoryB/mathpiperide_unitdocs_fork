/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mathpiper.builtin.procedures.core;


import java.util.Map;
import org.mathpiper.builtin.BigNumber;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.BuiltinObjectCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.NumberCons;



public class MetaGet extends BuiltinProcedure
{

    private MetaGet()
    {
    }

    public MetaGet(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {

        Cons object = getArgument(aEnvironment, aStackTop, 1);

        Cons key = getArgument(aEnvironment, aStackTop, 2);

        LispError.checkIsString(aEnvironment, aStackTop, key, 2);


        Map metadataMap = object.getMetadataMap();

        if (metadataMap == null) {
            setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(aEnvironment.getPrecision(), "None"));

            return;
        }//end if.


        Object value = metadataMap.get((String) key.car());
        
        if (value == null) {
            setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(aEnvironment.getPrecision(), "None"));
            return;
        } 
        

        Cons valueCons = null;
            
        if(value instanceof Cons)
        {
            valueCons = (Cons) value;
        }
        else if (value instanceof Number)
        {
            valueCons = new NumberCons(new BigNumber(value.toString(), 10, 10));
        }
        else if (value instanceof String)
        {
            valueCons = AtomCons.getInstance(10, (String) value);
        }
        else 
        {
            valueCons = BuiltinObjectCons.getInstance(aEnvironment, aStackTop, new JavaObject(value));
        }
        
        setTopOfStack(aEnvironment, aStackTop, valueCons);
            



    }//end method.


}//end class.



/*
%mathpiper_docs,name="MetaGet",categories="Programming Procedures;Built In"
*CMD MetaGet --- returns the metadata for a value or an unbound variable
*CORE
*CALL
MetaGet(value_or_unbound_variable, key_string)

*PARMS

{value_or_unbound_variable} -- a value or an unbound variable

{key_string} -- a string which is the key for the given value


*DESC

Returns the metadata for a value or an unbound variables.  The metadata is
held in an associative list.



*SEE MetaSet, MetaClear, MetaKeys, MetaValues, Unassign
%/mathpiper_docs
 */
