/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.Array;
import org.mathpiper.builtin.BuiltinContainer;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;

import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;

/**
 *
 *  
 */
public class Length extends BuiltinProcedure
{

    private Length()
    {
    }

    public Length(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Object argument = getArgument(aEnvironment, aStackTop, 1).car();
        

        if (argument instanceof Cons)
        {
            int num = Utility.listLength(aEnvironment, aStackTop, (((Cons)argument).cdr()));
            setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(aEnvironment.getPrecision(), "" + num));
            return;
        }//end if.
        
        
        
        if (argument instanceof BuiltinContainer)
        {
            BuiltinContainer gen = (BuiltinContainer) getArgument(aEnvironment, aStackTop, 1).car();
            if (gen.typeName().equals("\"Array\""))
            {
                int size = ((Array) gen).size();
                setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(aEnvironment.getPrecision(), "" + size));
                return;
            }
            
            LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, argument);
        }//end if.



        if(! (argument instanceof String)) 
        {
            LispError.throwError(aEnvironment, aStackTop, "The argument to Length must be a list or a string.");
        }
        
        String string =  (String) argument;
        if (Utility.isString(string))
        {
            int num = string.length() - 2;
            setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(aEnvironment.getPrecision(), "" + num));
            return;
        }//end if.
        
        LispError.throwError(aEnvironment, aStackTop, "The argument to Length must be a list or a string.");

    }//end method..

}//end class.



/*
%mathpiper_docs,name="Length",categories="Programming Procedures;Lists (Operations);Built In"
*CMD Length --- the length of a list or string
*CORE
*CALL
	Length(object)

*PARMS

{object} -- a list, array or string

*DESC

Length returns the length of a list.
This function also works on strings and arrays.

*E.G.

In> Length([_a,_b,_c])
Result: 3;

In> Length("abcdef");
Result: 6;

*SEE First, Rest, Nth, Count
%/mathpiper_docs
*/
