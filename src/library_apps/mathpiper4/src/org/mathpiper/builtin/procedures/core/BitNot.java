/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BigNumber;
import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.lisp.Environment;

/**
 *
 *  
 */
public class BitNot extends BuiltinProcedure
{

    private BitNot()
    {
    }

    public BitNot(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        BigNumber x = org.mathpiper.lisp.Utility.getNumber(aEnvironment, aStackTop, 1);
        BigNumber z = new BigNumber(aEnvironment.getPrecision());
        z.bitNot(x, aEnvironment, aStackTop);
        setTopOfStack(aEnvironment, aStackTop, new org.mathpiper.lisp.cons.NumberCons(z));
    }
}//end class.



/*
%mathpiper_docs,name="BitNot",categories="Programming Procedures;Built In"
*CMD BitNot --- bitwise 'not' operation
*CORE
*CALL
	BitNot(n)


*DESC
This function returns the bitwise "not"
of a number.

*SEE BitOr, BitXor, BitAnd

%/mathpiper_docs
*/
