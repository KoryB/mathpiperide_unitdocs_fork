/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}


package org.mathpiper.builtin.procedures.optional;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;

/**
 *
 *
 */
public class TextToSpeech extends BuiltinProcedure
{


    public void plugIn(Environment aEnvironment) throws Throwable {
        this.functionName = "TextToSpeech";
        aEnvironment.getBuiltinFunctions().put(this.functionName, new BuiltinProcedureEvaluator(this, 3, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
    }//end method.


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        // First argument.
        if (getArgument(aEnvironment, aStackTop, 1) == null) {
            LispError.checkArgument(aEnvironment, aStackTop, 1);
        }
        Object argument = getArgument(aEnvironment, aStackTop, 1).car();
        if (!(argument instanceof String)) {
            LispError.raiseError("The first argument to " + functionName + " must be a string.", aStackTop, aEnvironment);
        }
        String text = (String) argument;
        if (text == null) {
            LispError.checkArgument(aEnvironment, aStackTop, 1);
        }
        text = Utility.stripEndQuotesIfPresent(text);
        
        
        
        // Second argument.
        if (getArgument(aEnvironment, aStackTop, 2) == null) {
            LispError.checkArgument(aEnvironment, aStackTop, 2);
        }
        argument = getArgument(aEnvironment, aStackTop, 2).car();
        if (!(argument instanceof String)) {
            LispError.raiseError("The second argument to " + functionName + " must be a string.", aStackTop, aEnvironment);
        }
        String inetAddress = (String) argument;
        if (text == null) {
            LispError.checkArgument(aEnvironment, aStackTop, 2);
        }
        inetAddress = Utility.stripEndQuotesIfPresent(inetAddress);
        
        
        
        // Third argument.
        if (getArgument(aEnvironment, aStackTop, 3) == null) {
            LispError.checkArgument(aEnvironment, aStackTop, 3);
        }
        argument = getArgument(aEnvironment, aStackTop, 3).car();
        if (!(argument instanceof String)) {
            LispError.raiseError("The third argument to " + functionName + " must be a string.", aStackTop, aEnvironment);
        }
        String soundFilePath = (String) argument;
        if (text == null) {
            LispError.checkArgument(aEnvironment, aStackTop, 3);
        }
        soundFilePath = Utility.stripEndQuotesIfPresent(soundFilePath);
        
        
        
        
        BufferedReader in;
        PrintWriter out;

        Socket socket;
        final int FILE_SIZE = 1000000;

        //try {

            InetAddress iAddress = InetAddress.getByName(inetAddress);

            socket = new Socket(iAddress, 8080);


            out = new PrintWriter(socket.getOutputStream());

            out.print(text + "\n");
            out.print("\n");
            out.flush();

            int bytesRead;
            int current = 0;
            FileOutputStream fos = null;
            BufferedOutputStream bos = null;
            
            // receive file
            byte[] mybytearray = new byte[FILE_SIZE];
            InputStream is = socket.getInputStream();
            fos = new FileOutputStream(soundFilePath);
            bos = new BufferedOutputStream(fos);
            bytesRead = is.read(mybytearray, 0, mybytearray.length);
            current = bytesRead;

            do {
                bytesRead
                        = is.read(mybytearray, current, (mybytearray.length - current));
                if (bytesRead >= 0) {
                    current += bytesRead;
                }
            } while (bytesRead > -1);

            bos.write(mybytearray, 0, current);
            bos.flush();

            bos.close();
            is.close();

            socket.close();
            
            setTopOfStack(aEnvironment, aStackTop, Utility.getTrueAtom(aEnvironment));

        /*}//end try
        catch (UnknownHostException uhe) {
            uhe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }//end catch  */     
        
    }
    

    
}//end class.



/*
%mathpiper_docs,name="TextToSpeech",categories="Programming Procedures;Input/Output;Built In",access="experimental"
*CMD TextToSpeech --- converts text to an audio file
*CORE
*CALL
	TextToSpeech(text, ipAddress, soundFilePath)

*PARMS

{text} -- a string that contains text
{filePath} -- a path to a sound file
{ipAddress} -- a string that contains an IP address


*DESC

This procedure uses a server to convert text to an audio file.

%/mathpiper_docs
*/
