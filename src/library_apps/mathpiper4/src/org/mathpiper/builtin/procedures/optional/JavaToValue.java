/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.optional;

import org.mathpiper.builtin.BigNumber;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.BuiltinObjectCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.SublistCons;

/**
 *
 *
 */
public class JavaToValue extends BuiltinProcedure {

    public void plugIn(Environment aEnvironment) throws Throwable {
        this.functionName = "JavaToValue";
        aEnvironment.getBuiltinFunctions().put(this.functionName, new BuiltinProcedureEvaluator(this, 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
    }//end method.

    //private StandardFileOutputStream out = new StandardFileOutputStream(System.out);
    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {

        Object argument = getArgument(aEnvironment, aStackTop, 1).car();

        if(argument instanceof AtomCons && ((String)((AtomCons) argument).car()).equals("List"))
        { // Handle as flatlist for now.
            
            Cons inCons = (Cons) argument;
            
            inCons = inCons.cdr();
            
            Cons outHead = aEnvironment.iListAtom.copy(false);

            Cons outCons = outHead;

            while (inCons != null) {

                Object object = inCons.car();
                
                if(object instanceof JavaObject)
                {
                    JavaObject javaObject = (JavaObject) object;
                    object = javaObject.getObject();
                }
                
                if (object instanceof Number) {
                    
                    BigNumber number = new BigNumber(aEnvironment.getPrecision());
                    
                    number.setTo(object.toString(), aEnvironment.getPrecision(), 10);

                    outCons.setCdr(new org.mathpiper.lisp.cons.NumberCons(number));
                } else {
                    // todo:tk:handler other data types here.
                }

                outCons = outCons.cdr();

                inCons = inCons.cdr();

            }//end while.

            setTopOfStack(aEnvironment, aStackTop, SublistCons.getInstance(outHead));
            
            return;
        }
        else if (argument instanceof JavaObject) {
            String atomValue = "";

            JavaObject javaObject = (JavaObject) argument;

            Object object = javaObject.getObject();

            if (object != null) {

                if (object instanceof java.lang.Boolean) {
                    if (((Boolean) object).booleanValue() == true) {
                        atomValue = "True";
                    } else {
                        atomValue = "False";
                    }
                } else if (object instanceof String[]) {

                    String[] stringArray = (String[]) object;

                    Cons listAtomCons = aEnvironment.iListAtom.copy(false);

                    Cons sublistCons = SublistCons.getInstance(listAtomCons);

                    Cons cons = listAtomCons;

                    for (String javaString : stringArray) {
                        Cons atomCons = AtomCons.getInstance(aEnvironment.getPrecision(), Utility.toMathPiperString(aEnvironment, aStackTop, javaString));

                        cons.setCdr(atomCons);

                        cons = cons.cdr();
                    }//end for.

                    setTopOfStack(aEnvironment, aStackTop, sublistCons);

                    return;

                } else if (object instanceof Integer) {
                    atomValue = javaObject.getObject().toString();
                }
                else{
                    atomValue = Utility.toMathPiperString(aEnvironment, aStackTop, (String) javaObject.getObject().toString());
                }

                setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(aEnvironment.getPrecision(), atomValue));

                return;
            }
        } else {
            LispError.raiseError("The argument must be a JavaObject.", aStackTop, aEnvironment);
        }


    }//end method.
}//end class.





/*
%mathpiper_docs,name="JavaToValue",categories="Programming Procedures;Built In;Native Objects",access="experimental"
*CMD JavaToValue --- converts a Java object into a MathPiper data structure
*CALL
    JavaToValue(javaObject)

*PARMS
{javaObject} -- a Java object

*DESC
This function is used to convert a Java object into a MathPiper data structure.  It is typically 
used with JavaCall.

*E.G.
In> javaString := JavaNew("java.lang.String", "Hello")
Result: java.lang.String

In> JavaToValue(javaString)
Result: Hello

*SEE JavaCall, JavaAccess, JavaNew
%/mathpiper_docs
*/
