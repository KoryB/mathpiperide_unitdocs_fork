/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import org.mathpiper.exceptions.EvaluationException;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;

import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;

/**
 *
 *  
 */
public class ExceptionThrow extends BuiltinProcedure
{

    private ExceptionThrow()
    {
    }

    public ExceptionThrow(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Cons typeCons;
        typeCons = getArgument(aEnvironment, aStackTop, 1);
        LispError.checkIsString(aEnvironment, aStackTop, typeCons, 1);
        String typeString = Utility.toNormalString(aEnvironment, aStackTop, (String) typeCons.car());
        
        Cons message = getArgument(aEnvironment, aStackTop, 2);
        LispError.checkIsString(aEnvironment, aStackTop, message, 2);
        String errorMessage = " Error: " + Utility.toNormalString(aEnvironment, aStackTop, (String) message.car());

        throw new EvaluationException(typeString, errorMessage, "", -1, -1, -1, "Null", "Null");

    }
}



/*
%mathpiper_docs,name="ExceptionThrow",categories="Programming Procedures;Error Reporting;Built In"
*CMD ExceptionThrow --- throw an exception of a given type
*CORE
*CALL
	ExceptionThrow("type", "exceptionMessage")

*PARMS

{"type"} -- a string that contains the type of the expression

{"exceptionMessage"} -- string which holds the exception message

*DESC
The current operation is stopped and an exception will be thrown.

Exceptions that are thrown by this function can be caught by the {ExceptionCatch} function.



*E.G.

In> ExceptionThrowThrow("EXAMPLE", "This is an example exception.")
Result: Exception
Exception: The argument must be an integer.


*SEE ExceptionCatch, ExceptionGet

%/mathpiper_docs
*/