package org.mathpiper.lisp.astprocessors;

import java.util.HashMap;
import java.util.List;


import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.Cons;


public class MetaToObjectSubstitute
        implements ASTProcessor {

    Environment iEnvironment;


    public MetaToObjectSubstitute(Environment aEnvironment) {
        iEnvironment = aEnvironment;

    }


    public Cons patternMatches(Environment aEnvironment, int aStackTop, Cons aElement, List<Integer> positionList)
            throws Throwable {
	
	if(aElement.car() instanceof String && ! ((String) aElement.car()).startsWith("\""))
	{
	    String name = ((String) aElement.car()).replace("_", "");
            
            name = name.replace("$", "");
	    
            Cons newCons = AtomCons.getInstance(aEnvironment.getPrecision(), name);
            
            if(aElement.getMetadataMap() != null)
            {
                newCons.setMetadataMap(new HashMap<String,Object>(aElement.getMetadataMap()));
            }
            
	    return(newCons);
	}

        return null;
    }

};
