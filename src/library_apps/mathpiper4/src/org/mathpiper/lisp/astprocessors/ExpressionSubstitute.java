package org.mathpiper.lisp.astprocessors;

import java.util.List;

import org.mathpiper.lisp.Utility;

import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.cons.Cons;

/** Substing one expression for another. The simplest form
 * of substitution
 */
public class ExpressionSubstitute
        implements ASTProcessor {

    Environment iEnvironment;
    Cons iToMatch;
    Cons iToReplaceWith;


    public ExpressionSubstitute(Environment aEnvironment, Cons aToMatch, Cons aToReplaceWith) {
        iEnvironment = aEnvironment;
        iToMatch = aToMatch;
        iToReplaceWith = aToReplaceWith;
    }


    public Cons patternMatches(Environment aEnvironment, int aStackTop, Cons aElement, List<Integer> positionList)
            throws Throwable {

        if (Utility.equals(iEnvironment, aStackTop, aElement, iToMatch)) {
            return iToReplaceWith.copy(false);

        }

        return null;
    }

};
