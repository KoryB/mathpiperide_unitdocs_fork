/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.lisp.rulebases;

import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.SublistCons;

public class ListedRulebase extends SingleArityRulebase {

    public ListedRulebase(Environment aEnvironment, int aStackTop, Cons aParameters, String functionName) throws Throwable {
        super(aEnvironment, aStackTop, aParameters, functionName);
    }


    @Override
    public boolean isArity(int aArity) {
        return (arity() <= aArity);
    }


    @Override
    public Cons evaluate(Environment aEnvironment, int aStackTop, Cons aArguments) throws Throwable {
        Cons aResult;

        Cons newArgs = null;

        Cons consTraverser = aArguments;

        Cons ptr = null;
        
        ptr = consTraverser.copy(false);
        
        newArgs = ptr;

        int arity = arity();
        
        if(arity == 0)
        {
            consTraverser = consTraverser.cdr();
            
            while(consTraverser != null)
            {
                Cons nextCons = consTraverser.copy(false);
                ptr.setCdr(nextCons);
                ptr = nextCons;
                consTraverser = consTraverser.cdr();
            }
            return SublistCons.getInstance(newArgs);
        }
        else
        {
            int i = 0;

            while (i < arity && consTraverser != null) {

                if(i != 0)
                {
                    Cons nextCons = consTraverser.copy(false);
                    ptr.setCdr(nextCons);
                    ptr = nextCons;
                }

                i++;

                consTraverser = consTraverser.cdr();
            }

            if (consTraverser.cdr() == null) {
                // Only the nonlisted parameters have values assigned to them.
                Cons nextCons = consTraverser.copy(false);
                ptr.setCdr(nextCons);
                ptr = nextCons;
                i++;
                consTraverser = consTraverser.cdr();
                if(consTraverser != null) LispError.lispAssert(aEnvironment, aStackTop);
            } else {
                Cons head = aEnvironment.iListAtom.copy(false);
                head.setCdr(consTraverser);
                Cons nextCons = SublistCons.getInstance(head);
                ptr.setCdr(nextCons);
                ptr = nextCons;
            }
        
        }
        
        aResult = super.evaluate(aEnvironment, aStackTop, newArgs);
        return aResult;
    }

}
