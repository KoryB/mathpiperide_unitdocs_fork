/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *///}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.lisp;

import java.util.Map;

import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.exceptions.EvaluationException;

import org.mathpiper.lisp.rulebases.SingleArityRulebase;
import org.mathpiper.lisp.tokenizers.MathPiperTokenizer;

/**
 * The basic evaluator for Lisp expressions.
 * 
 */
public class LispExpressionEvaluator extends Evaluator {

    /**
     * <p>
     * First, the evaluation depth is checked. An error is raised if the maximum
     * evaluation depth is exceeded. The next step is the actual evaluation.
     * aExpression is a Cons, so we can distinguish three cases:
     * </p>
     * <ol>
     * <li value="1">
     * <p>
     * If aExpression is a string starting with " , it is simply copied in
     * aResult. If it starts with another character (this includes the case
     * where it represents a number), the environment is checked to see whether
     * a variable with this name exists. If it does, its value is copied in
     * aResult, otherwise aExpression is copied.
     * </p>
     * 
     * <li value="2">
     * <p>
     * If aExpression is a list, the head of the list is examined. If the head
     * is not a string. ApplyFast() is called. If the head is a string, it is
     * checked against the core commands (if there is a check, the corresponding
     * evaluator is called). Then it is checked against the list of user
     * procedures with getRulebase(). Again, the corresponding evaluator is called
     * if there is a check. If all fails, ReturnUnEvaluated() is called.
     * </p>
     * 
     * <li value="3">
     * <p>
     * Otherwise (i.e. if aExpression is a getJavaObject object), it is copied
     * in aResult.
     * </p>
     * </ol>
     * 
     * <p>
     * Note: The result of this operation must be a unique (copied) element! Eg.
     * its Next might be set...
     * </p>
     * 
     * @param aEnvironment
     *            the Lisp environment, in which the evaluation should take
     *            place
     * @param aResult
     *            the result of the evaluation
     * @param aExpression
     *            the expression to evaluate
     * @throws java.lang.Exception
     */
    public Cons evaluate(Environment aEnvironment, int aStackTop, Cons aExpression)
	    throws Throwable {

	if (aExpression == null) {
	    LispError.lispAssert(aEnvironment, aStackTop);
	}

        if(aEnvironment.iEvalDepth++ > aEnvironment.iMaxEvalDepthReached)
        {
            aEnvironment.iMaxEvalDepthReached = aEnvironment.iEvalDepth;
        }
        
	if (aEnvironment.iEvalDepth >= aEnvironment.iMaxEvalDepth) {

	    if (aEnvironment.iEvalDepth >= aEnvironment.iMaxEvalDepth)
		LispError.throwError(aEnvironment, aStackTop, LispError.MAXIMUM_RECURSE_DEPTH_REACHED, "Maximum recursed depth set to " + aEnvironment.iMaxEvalDepth + ".");
	    // }
	}

	if (Environment.haltEvaluation == true) {
	    Environment.haltEvaluation = false;

            throw new EvaluationException("HALT", Environment.haltEvaluationMessage, aEnvironment.getCurrentInput().iStatus.getSourceName(),  -1, -1, -1, null, null);
	}

	// evaluate an atom: find the bound value (treat it as a variable)
	if (aExpression.car() instanceof String) {

	    String atomName = (String) aExpression.car();

	    if (atomName.charAt(0) == '\"') {
		// Handle string atoms.
		aEnvironment.iEvalDepth--;
		return aExpression.copy(false);
	    }

	    if (Character.isDigit(atomName.charAt(0))) {
		// Handle number atoms.
		aEnvironment.iEvalDepth--;
		return aExpression.copy(false);
	    }

	    Cons val = aEnvironment.getLocalOrGlobalVariable(aStackTop, atomName);
	    if (val != null) {
		aEnvironment.iEvalDepth--;
                
                if(atomName.contains("_"))
                {
                    return aExpression.copy(false);
                }
                else
                {
                    return val.copy(false);
                }
	    }

	    /*
	     * Map metaDataMap = aExpression.getCons().getMetadataMap(); int
	     * lineNumber = aEnvironment.iCurrentInput.iStatus.getLineNumber();
	     * int startIndex = -1; int endIndex =
	     * aEnvironment.iCurrentInput.iStatus.getLineIndex(); if
	     * (metaDataMap != null) { lineNumber = (Integer)
	     * metaDataMap.get("\"lineNumber\""); startIndex = (Integer)
	     * metaDataMap.get("\"startIndex\""); endIndex = (Integer)
	     * metaDataMap.get("\"endIndex\""); }
	     * LispError.raiseError("Problem with atom ***(" + atomName +
	     * ")***, <wrong code: " + Utility.printLispExpression(-1,
	     * aExpression, aEnvironment, 50) + ">,", "[Internal]", lineNumber,
	     * startIndex, endIndex, aStackTop, aEnvironment);
	     */

	    // Handle unbound variables.
	    

	    if(atomName.charAt(0) == '.' || atomName.charAt(0) == '-') //todo:tk:why is .1 being treated differently than 0.1?
	    {
                aEnvironment.iEvalDepth--;
		return aExpression.copy(false);
	    }
	    
	    
	    
	    boolean foundNonSymbol = false;
	    for(char c : atomName.toCharArray())
	    {
		if(!MathPiperTokenizer.isSymbolic(c))
		{
		    foundNonSymbol = true;
		    break;
		}
	    }
	    if(foundNonSymbol == false)
	    {
		LispError.throwError(aEnvironment, aStackTop, "The name <" + atomName + "> is an operator that cannot be evaluated without operands.", aExpression.getMetadataMap());
	    }
	    
	    
	    
	    BuiltinProcedureEvaluator builtinInProcedureEvaluator = (BuiltinProcedureEvaluator) aEnvironment.getBuiltinFunctions().get(atomName);
	    if (builtinInProcedureEvaluator != null) 
            {
		LispError.throwError(aEnvironment, aStackTop, "The name <" + atomName + "> is a procedure that cannot be evaluated without arguments.", aExpression.getMetadataMap());
            }
	    
	    Utility.loadLibraryFunction(atomName, aEnvironment, aStackTop);
	    
	    if(aEnvironment.getMultipleArityRulebase(aStackTop, atomName, false) != null)
	    {
		LispError.throwError(aEnvironment, aStackTop, "The name <" + atomName + "> is a procedure that cannot be evaluated without arguments.", aExpression.getMetadataMap());
	    }
	    
            if(atomName.contains("_"))
            {
                aEnvironment.iEvalDepth--;
                return aExpression.copy(false);
            }
	    
	    LispError.throwError(aEnvironment, aStackTop, "The variable <" + atomName + "> does not have a value assigned to it.", aExpression.getMetadataMap());

	}

	if (aExpression.car() instanceof Cons) {
	    Cons procedureAndArgumentsList = (Cons) aExpression.car();
	    Cons head = procedureAndArgumentsList;
	    if (head != null) {
		


		String procedureName;

		if (head.car() instanceof String) {

		    procedureName = (String) head.car();
		    
		    /*
		     //Note:tk:this code prints the locations of operators that are being executed in the user's code.
		    Map map = head.getMetadataMap();

		    if(map != null)
		    {
			System.out.println(functionName + " " + map.get("\"lineNumber\"") + ", " + map.get("\"startIndex\"") + ", " + map.get("\"endIndex\"") + ",  " + Utility.printMathPiperExpression(-1, aExpression, aEnvironment, 0));
		    }
		    */
		    
                    try {
                        // Built-in procedure handler.
                        BuiltinProcedureEvaluator builtinInProcedureEvaluator = (BuiltinProcedureEvaluator) aEnvironment.getBuiltinFunctions().get(procedureName);
                        if (builtinInProcedureEvaluator != null) {

                            Cons result = builtinInProcedureEvaluator.evaluate(aEnvironment, aStackTop, procedureAndArgumentsList);
                            aEnvironment.iEvalDepth--;
                            return(result);
                        }

                        // User function handler.
                        SingleArityRulebase userProcedure;
                        
                        userProcedure = getUserProcedure(aEnvironment, aStackTop, procedureAndArgumentsList);
                        
                        if (userProcedure != null) {
                            Cons result = userProcedure.evaluate(aEnvironment, aStackTop, procedureAndArgumentsList);
                            aEnvironment.iEvalDepth--;
                            return result;
                        }
                        else if(procedureName.startsWith("\"")) // Return procedures that have a string as a procedure name unevaluated.
                        {
                            aEnvironment.iEvalDepth--;
                            return aExpression.copy(false);
                        }
                        else if(procedureName.startsWith("_") || procedureName.endsWith("$")) // Return metalevel symbols unevaluated.
                        {
                            aEnvironment.iEvalDepth--;
                            return aExpression.copy(false);
                        }
                        else
                        {
                            LispError.throwError(aEnvironment, aStackTop, "The " + (Utility.listLength(aEnvironment, aStackTop, procedureAndArgumentsList) - 1) + " parameter version of procedure <" + procedureName + "> is not defined (MAKE SURE THE PROCEDURE'S NAME IS SPELLED CORRECTLY and the correct number of arguments are being passed to it.)", head.getMetadataMap());
                        }

                    } catch (Exception e) {
                        Map<String, Object> map = Utility.findMetaData(aEnvironment, aStackTop, aExpression);

                        if (map != null && map.get("\"lineNumber\"") != null) {
                            if (e instanceof EvaluationException) {
                                EvaluationException ee = (EvaluationException) e;

                                if (!ee.isAlreadyHandled()) {
                                    EvaluationException ee2 = null;
                                    
                                    if(ee.getStartIndex() != -1)
                                    {
                                        ee2 = new EvaluationException(ee.getType(), ee.getMessage(), ee.getFileName(), ee.getLineNumber(), ee.getStartIndex(), ee.getEndIndex(), null, ee.getSourceName());
                                    }
                                    else
                                    {
                                        ee2 = new EvaluationException(ee.getType(), ee.getMessage(), ee.getFileName(), (Integer) map.get("\"lineNumber\""), (Integer) (Integer) map.get("\"startIndex\""), (Integer) map.get("\"endIndex\""), null, (String) map.get("\"sourceName\""));
                                    }
                                    
                                    ee2.setIsAlreadyHandled(true);
                                    
                                    throw ee2;                                }
                            } else {
                                // e.printStackTrace(); //todo:tk:uncomment for debugging.
                                throw new EvaluationException("Evaluate", e.getClass().getName() + ": " + e.getMessage() + ".", aEnvironment.getCurrentInput().iStatus.getSourceName(), (Integer) map.get("\"lineNumber\""), (Integer) map.get("\"startIndex\""), (Integer) map.get("\"endIndex\""), null, (String) map.get("\"sourceName\""));
                            }
                        }
                        // e.printStackTrace(); //todo:tk:uncomment for debugging.
                        throw e;
                    }

		    if (procedureName.equals("FreeOf?")) {
                        // todo:tk:the purpose of this code needs to be determined.
			Cons result = Utility.returnUnEvaluated(aStackTop, procedureAndArgumentsList, aEnvironment);
                        aEnvironment.iEvalDepth--;
                        return result; 
		    }

		    int lineNumber = aEnvironment.getCurrentInput().iStatus.getLineNumber();
		    int startIndex = -1;
		    int endIndex = aEnvironment.getCurrentInput().iStatus.getLineIndex();
                    
                    String metaDataSourceName = null;
                    Map metaDataMap = procedureAndArgumentsList.getMetadataMap();
		    if (metaDataMap != null) {
			lineNumber = (Integer) metaDataMap.get("\"lineNumber\"");
			startIndex = (Integer) metaDataMap.get("\"startIndex\"");
			endIndex = (Integer) metaDataMap.get("\"endIndex\"");
                        metaDataSourceName = (String) metaDataMap.get("\"sourceName\"");
		    }
                    
                    String sourceName = metaDataSourceName != null? metaDataSourceName: aEnvironment.getCurrentInput().iStatus.getSourceName();

		    // LispError.raiseError("Problem with procedure ***(" + procedureName + ")***, <wrong code: " + Utility.printLispExpression(-1, procedureAndArgumentsList, aEnvironment, 50) + ">, <the " + (Utility.listLength(aEnvironment, aStackTop, procedureAndArgumentsList) - 1) + " parameter version of this procedure is not defined (MAKE SURE THE PROCEDURE IS SPELLED CORRECTLY).>", lineNumber, startIndex, endIndex, aStackTop, aEnvironment);

                    throw new EvaluationException("Evaluate", "Problem with procedure ***(" + procedureName + ")***, <wrong code: " + Utility.printLispExpression(-1, procedureAndArgumentsList, aEnvironment, 50) + ">, <the " + (Utility.listLength(aEnvironment, aStackTop, procedureAndArgumentsList) - 1) + " parameter version of this procedure is not defined (MAKE SURE THE PROCEDURE IS SPELLED CORRECTLY).>", aEnvironment.getCurrentInput().iStatus.getSourceName(), lineNumber, startIndex, endIndex, procedureName, sourceName);

		} else {
		    // Pure procedure handler.  todo:tk:determine when this code is executed.
		    Cons operator = procedureAndArgumentsList;
		    Cons args2 = procedureAndArgumentsList.cdr();

		    Cons result = Utility.applyPure(aStackTop, operator, args2, aEnvironment);
                    aEnvironment.iEvalDepth--;
                    return result;
		}
	    }
	}

	aEnvironment.iEvalDepth--;
	return aExpression.copy(false);
    }

    SingleArityRulebase getUserProcedure(Environment aEnvironment, int aStackTop, Cons subList)
	    throws Throwable {
	Cons head = subList;

	if (!(head.car() instanceof String))
	    LispError.throwError(aEnvironment, aStackTop, "No procedure name specified.");

	String procedureName = (String) head.car();

	SingleArityRulebase userProcedure = (SingleArityRulebase) aEnvironment.getRulebase(aStackTop, subList);

	// if (userProcedure == null && procedureName.equals("f")) {
	// int xx = 1;
	// }

	if (userProcedure != null) {
	    return userProcedure;

	} else {

	    // System.out.println(procedureName);

	    if (Utility.loadLibraryFunction(procedureName, aEnvironment, aStackTop) == false) {
                
		return null;
            }

	    userProcedure = (SingleArityRulebase) aEnvironment.getRulebase(aStackTop, subList);
	}

	return userProcedure;

    }// end method.
    /*
     * void TracedStackEvaluator::PushFrame() { UserStackInformaif (tion *op = NEW
     * UserStackInformation; objs.Append(op); }
     * 
     * void TracedStackEvaluator::PopFrame() { LISPASSERT (objs.Size() > 0); if
     * (objs[objs.Size()-1]) { delete objs[objs.Size()-1]; objs[objs.Size()-1] =
     * null; } objs.Delete(objs.Size()-1); }
     * 
     * void TracedStackEvaluator::ResetStack() { while (objs.Size()>0) {
     * PopFrame(); } }
     * 
     * UserStackInformation& TracedStackEvaluator::StackInformation() { return
     * *(objs[objs.Size()-1]); }
     * 
     * TracedStackEvaluator::~TracedStackEvaluator() { ResetStack(); }
     * 
     * void TracedStackEvaluator::ShowStack(Environment aEnvironment,
     * LispOutput& aOutput) { LispLocalEvaluator local(aEnvironment,NEW
     * BasicEvaluator);
     * 
     * LispInt i; LispInt from=0; LispInt upto = objs.Size();
     * 
     * for (i=from;i<upto;i++) { LispChar str[20]; #ifdef YACAS_DEBUG
     * aEnvironment.write(objs[i].iFileName); aEnvironment.write("(");
     * InternalIntToAscii(str,objs[i].iLine); aEnvironment.write(str);
     * aEnvironment.write(") : "); aEnvironment.write("Debug> "); #endif
     * InternalIntToAscii(str,i); aEnvironment.write(str);
     * aEnvironment.write(": ");
     * aEnvironment.CurrentPrinter().Print(objs[i].iOperator,
     * *aEnvironment.CurrentOutput(),aEnvironment);
     * 
     * LispInt internal; internal = (null !=
     * aEnvironment.CoreCommands().LookUp(objs[i].iOperator.String())); if
     * (internal) { aEnvironment.write(" (Internal function) "); } else { if
     * (objs[i].iRulePrecedence>=0) { aEnvironment.write(" (Rule # ");
     * InternalIntToAscii(str,objs[i].iRulePrecedence); aEnvironment.write(str);
     * if (objs[i].iSide) aEnvironment.write(" in body) "); else
     * aEnvironment.write(" in pattern) "); } else
     * aEnvironment.write(" (User function) "); } if (!!objs[i].iExpression) {
     * aEnvironment.write("\n      "); if
     * (aEnvironment.iEvalDepth>(aEnvironment.iMaxEvalDepth-10)) { LispString
     * expr; PrintExpression(expr, objs[i].iExpression,aEnvironment,60);
     * aEnvironment.write(expr.c_str()); } else { LispPtr getSubList =
     * objs[i].iExpression.SubList(); if (!!getSubList && !!getSubList) {
     * LispString expr; LispPtr out(objs[i].iExpression); PrintExpression(expr,
     * out,aEnvironment,60); aEnvironment.write(expr.c_str()); } } }
     * aEnvironment.write("\n"); } }
     * 
     * void TracedStackEvaluator::Eval(Environment aEnvironment, ConsPointer
     * aResult, ConsPointer aExpression) { if
     * (aEnvironment.iEvalDepth>=aEnvironment.iMaxEvalDepth) {
     * ShowStack(aEnvironment, *aEnvironment.CurrentOutput());
     * CHK2(aEnvironment.iEvalDepth<aEnvironment.iMaxEvalDepth,
     * MAXIMUM_RECURSE_DEPTH_REACHED); }
     * 
     * LispPtr getSubList = aExpression.SubList(); LispString * str = null; if
     * (getSubList) { Cons head = getSubList; if (head) { str = head.String();
     * if (str) { PushFrame(); UserStackInformation& st = StackInformation();
     * st.iOperator = (LispAtom::New(aEnvironment,str.c_str())); st.iExpression
     * = (aExpression); #ifdef YACAS_DEBUG if (aExpression.iFileName) {
     * st.iFileName = aExpression.iFileName; st.iLine = aExpression.iLine; }
     * #endif } } } BasicEvaluator::Eval(aEnvironment, aResult, aExpression); if
     * (str) { PopFrame(); } }
     * 
     * void TracedEvaluator::Eval(Environment aEnvironment, ConsPointer aResult,
     * ConsPointer aExpression) { if(!aEnvironment.iDebugger)
     * RaiseError("Internal error: debugging failing");
     * if(aEnvironment.iDebugger.Stopped()) RaiseError("");
     * 
     * REENTER: errorStr.ResizeTo(1); errorStr[0] = '\0';
     * LispTrap(aEnvironment.iDebugger.Enter(aEnvironment,
     * aExpression),errorOutput,aEnvironment);
     * if(aEnvironment.iDebugger.Stopped()) RaiseError(""); if (errorStr[0]) {
     * aEnvironment.write(errorStr.c_str()); aEnvironment.iEvalDepth=0; goto
     * REENTER; }
     * 
     * errorStr.ResizeTo(1); errorStr[0] = '\0';
     * LispTrap(BasicEvaluator::Eval(aEnvironment, aResult,
     * aExpression),errorOutput,aEnvironment);
     * 
     * if (errorStr[0]) { aEnvironment.write(errorStr.c_str());
     * aEnvironment.iEvalDepth=0; aEnvironment.iDebugger.Error(aEnvironment);
     * goto REENTER; }
     * 
     * if(aEnvironment.iDebugger.Stopped()) RaiseError("");
     * 
     * aEnvironment.iDebugger.Leave(aEnvironment, aResult, aExpression);
     * if(aEnvironment.iDebugger.Stopped()) RaiseError(""); }
     * 
     * YacasDebuggerBase::~YacasDebuggerBase() { }
     * 
     * void DefaultDebugger::Start() { }
     * 
     * void DefaultDebugger::Finish() { }
     * 
     * void DefaultDebugger::Enter(Environment aEnvironment, ConsPointer
     * aExpression) { LispLocalEvaluator local(aEnvironment,NEW BasicEvaluator);
     * iTopExpr = (aExpression.Copy()); LispPtr result;
     * defaultEval.Eval(aEnvironment, result, iEnter); }
     * 
     * void DefaultDebugger::Leave(Environment aEnvironment, ConsPointer
     * aResult, ConsPointer aExpression) { LispLocalEvaluator
     * local(aEnvironment,NEW BasicEvaluator); LispPtr result; iTopExpr =
     * (aExpression.Copy()); iTopResult = (aResult);
     * defaultEval.Eval(aEnvironment, result, iLeave); }
     * 
     * LispBoolean DefaultDebugger::Stopped() { return iStopped; }
     * 
     * void DefaultDebugger::Error(Environment aEnvironment) {
     * LispLocalEvaluator local(aEnvironment,NEW BasicEvaluator); LispPtr
     * result; defaultEval.Eval(aEnvironment, result, iException); }
     */
}// end class.

