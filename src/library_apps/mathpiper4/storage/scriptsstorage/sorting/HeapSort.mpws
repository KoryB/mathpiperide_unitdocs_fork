%mathpiper,def="HeapSort"

//Retract("HeapSort",*);
//Retract("HeapSort1",*);


//*------ Because HeapSort is a Recursive function, and acts on its 
//*       original data, we have created a two-stage proces, so that the
//*       input list is not changed.

HeapSort(L_List?, _compare) <-- 
{
   Local(list);
   list := FlatCopy(L);
   HeapSort1(list, ArrayCreate(Length(list), 0), 1, Length(list), compare);
};

// this will sort "list" and mangle "tmplist"
1 # HeapSort1(_L, _tmplist, _first, _last, _compare) _ (last - first <=? 2) <-- SmallSort(L, first, last, compare);

2 # HeapSort1(_L, _tmplist, _first, _last, _compare) <--
{       // See: J. W. J. Williams, Algorithm 232 (Heapsort), Com. of ACM, vol. 7, no. 6, p. 347 (1964)
        // sort two halves recursively, then merge two halves
        // cannot merge in-place efficiently, so need a second list
        Local(mid, ileft, iright, pleft);
        //list := FlatCopy(L);
        mid  := first+((last-first)>>1);
        HeapSort1(L, tmplist, first, mid, compare);
        HeapSort1(L, tmplist, mid+1, last, compare);
        // copy the lower part to temporary array
        For(ileft := first,  ileft <=? mid, ileft++)
                tmplist[ileft] := L[ileft];
        For(
                {ileft := first; pleft := first; iright := mid+1;},
                ileft <=? mid,        // if the left half is finished, we don't have to do any more work
                pleft++        // one element is stored at each iteration
        )        // merge two halves
                // elements before pleft have been stored
                // the smallest element of the right half is at iright
                // the smallest element of the left half is at ileft, access through tmplist
        Decide(        // we copy an element from ileft either if it is smaller or if the right half is finished; it is unnecessary to copy the remainder of the right half since the right half stays in the "list"
                iright>?last Or? Apply(compare,[tmplist[ileft],L[iright]]),
                {        // take element from ileft
                        L[pleft] := tmplist[ileft];
                        ileft++;
                },
                {        // take element from iright
                        L[pleft] := L[iright];
                        iright++;
                }
        );
        L;
};

%/mathpiper



%mathpiper_docs,name="HeapSort",categories="Programming Functions;Sorting"
*CMD HeapSort --- sort a list
*STD
*CALL
        HeapSort(list, compare)

*PARMS

{list} -- list to sort

{compare} -- function used to compare elements of {list}

*DESC

This command returns {list} after it is sorted using {compare} to
compare elements. The procedure {compare} should accept two arguments,
which will be elements of {list}, and compare them. It should return
{True} if in the sorted list the second argument
should come after the first one, and {False}
otherwise.  The procedure {compare} can either be a string which
contains the name of a function or a pure function.

The procedure {HeapSort} uses a recursive algorithm "heapsort" and is much
faster for large lists. The sorting time is proportional to $n*Ln(n)$ where $n$
is the length of the list.

The original {list} is left unmodified.

*E.G.
In> HeapSort([4,7,23,53,-2,1], ">?")
Result: [53,23,7,4,1,-2]

In> HeapSort([3,5,2],Lambda([x,y],x<?y))
Result: [2,3,5]

*SEE HeapSortIndexed, BubbleSort, BubbleSortListed, Lambda
%/mathpiper_docs