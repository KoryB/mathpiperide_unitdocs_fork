%mathpiper,def="SumTaylorNum;SumTaylorNum0;SumTaylorNum1"

/// coded by Serge Winitzki. See essays documentation for algorithms.

//////////////////////////////////////////////////
/// Numerical method: Taylor series, rectangular summation
//////////////////////////////////////////////////

/// Fast summation of Taylor series using a rectangular scheme.
/// SumTaylorNum(x, nthtermfunc, nterms) = Sum(k, 0, nterms, nthtermfunc(k)*x^k)
/// Note that sufficient precision must be preset to avoid roundoff errors (these methods do not modify precision).
/// The only reason to try making these functions HoldArgument is to make sure that the closures nthtermfunc and nexttermfactor are passed intact. But its probably not desired in most cases because a closure might contain parameters that should be evaluated.

/// The short form is used when only the nth term is known but no simple relation between a term and the next term.
/// The long form is used when there is a simple relation between consecutive terms. In that case, the n'th term function is not needed, only the 0th term value.

/// SumTaylorNum0 is summing the terms with direct methods (Horner's scheme or simple summation). SumTaylorNum1 is for the rectangular method.

/// nthtermfunc and nexttermfunc must be functions applicable to one argument.

/// interface
SumTaylorNum0(_x, _nthtermfunc, _nterms) <-- SumTaylorNum0(x, nthtermfunc, [], nterms);

SumTaylorNum1(_x, _nthtermfunc, _nterms) <-- SumTaylorNum1(x, nthtermfunc, [], nterms);

/// interface
SumTaylorNum(_x, _nthtermfunc, _nterms) <--
Decide(
        nterms >=? 30,        // threshold for calculation with nexttermfactor
        // use the rectangular algorithm for large enough number of terms
        SumTaylorNum1(x, nthtermfunc, nterms),
        SumTaylorNum0(x, nthtermfunc, nterms)
);

SumTaylorNum(_x, _nthtermfunc, _nexttermfactor, _nterms) <--
Decide(
        nterms >=? 5,        // threshold for calculation with nexttermfactor
        SumTaylorNum1(x, nthtermfunc, nexttermfactor, nterms),
        SumTaylorNum0(x, nthtermfunc, nexttermfactor, nterms)
);
//HoldArgumentNumber(SumTaylorNum, 3, 2);

/// straightforward algorithms for a small number of terms
1# SumTaylorNum0(_x, _nthtermfunc, [], _nterms) <--
{
        Local(sum, k);
  NM({
    // use Horner scheme starting from the last term
    x:=Eval(x);
    sum := 0;
    For(k:=nterms, k>=?0, k--)
      sum := AddN(sum*x, nthtermfunc @ k);
  });
        sum;
};

//HoldArgumentNumber(SumTaylorNum0, 3, 2);

2# SumTaylorNum0(_x, _nthtermfunc, _nexttermfactor, _nterms) <--
{
        Local(sum, k, term, delta);
  NM({
    x:=Eval(x);        // x must be floating-point
    Decide(Constant?(nthtermfunc),
      term := nthtermfunc,
      term := (nthtermfunc @ [0]),
    );
    sum := term;        // sum must be floating-point
  });
  NonNM({
    delta := 1;
    For(k:=1, k<=?nterms And? delta !=? 0, k++)
    {
      term := MultiplyNum(term, nexttermfactor @ [k], x);        // want to keep exact fractions here, but the result is floating-point
      delta := sum;
      sum := sum + term;        // term must be floating-point
      delta := Abs(sum-delta);        // check for underflow
    };
  });
        sum;
};

/// interface
SumTaylorNum0(_x, _nthtermfunc, _nterms) <-- SumTaylorNum0(x, nthtermfunc, [], nterms);

//HoldArgumentNumber(SumTaylorNum0, 4, 2);
//HoldArgumentNumber(SumTaylorNum0, 4, 3);

/// this is to be used when a simple relation between a term and the next term is known.
/// nexttermfactor must be a function applicable to one argument, so that if term = nthtermfunc(k-1), then nthtermfunc(k) = term / nexttermfactor(k). (This is optimized for Taylor series of elementary functions.) In this case, nthtermfunc is either a number, value of the 0th term, or a function.
/// A special case: when nexttermfactor is an empty list; then we act as if there is no nexttermfactor available.
/// In this case, nthtermfunc must be a function applicable to one argument.
/// Need IntLog(nterms, 10) + 1 guard digits due to accumulated roundoff error.
SumTaylorNum1(x, nthtermfunc, nexttermfactor, nterms) :=
{
        // need Sqrt(nterms/2) units of storage (rows) and Sqrt(nterms*2) columns. Let's underestimate the storage.
        Local(sum, rows, cols, rowstmp, lastpower, i, j, xpower, termtmp);
  NM({ // want to keep exact fractions
    x:=Eval(x);        // x must be floating-point
    rows := IntNthRoot(nterms+1, 2);
    cols := Quotient(nterms+rows, rows);        // now: rows*cols >=? nterms+1
    Check(rows>?1 And? cols>?1, "Argument", "SumTaylorNum1: Internal error: number of Taylor sum terms must be at least 4");
    rowstmp := ArrayCreate(rows, 0);
    xpower := x ^ rows;        // do not use PowerN b/c x might be complex
    // initialize partial sums (array rowstmp) - the 0th column (i:=0)
    // prepare termtmp for the first element
    // if we are using nexttermfactor, then termtmp is x^(rows*i)*a[rows*i]
    // if we are not using it, then termtmp is x^(rows*i)
    Decide(
      nexttermfactor =? [],
      termtmp := 1,
  //                termtmp := (nthtermfunc @ 0)        // floating-point
      Decide(Constant?(nthtermfunc),
        termtmp := nthtermfunc,
        termtmp := (nthtermfunc @ [0]),
      )
    );
  });
  NonNM({ // want to keep exact fractions below
    // do horizontal summation using termtmp to get the first element
    For(i:=0, i<?cols, i++)
    {
      // add i'th term to each row
      For(j:=0, j<?rows And? (i<?cols-1 Or? i*rows+j<=?nterms), j++)        // do this unless we are beyond the last term in the last column
      {
        // if we are using nexttermfactor, then termtmp is x^(rows*i)*a[rows*i]
        // if we are not using it, then termtmp is x^(rows*i)
        Decide(
          nexttermfactor =? [],        // no nexttermfactor, so nthtermfunc must be given
          {
            rowstmp[j+1] := rowstmp[j+1] + MultiplyNum(termtmp, nthtermfunc @ [i*rows+j]);
          },
          {
            rowstmp[j+1] := rowstmp[j+1] + termtmp;        // floating-point
            termtmp := MultiplyNum(termtmp, nexttermfactor @ [i*rows+j+1]);        // arguments may be rational but the result is floating-point
          }
        );
      };
      // update termtmp for the next column
      termtmp := termtmp*xpower;        // both floating-point
    };
    // do vertical summation using Horner's scheme
    // now xpower = x^cols
    For({j:=rows; sum:=0;}, j>?0, j--)
      sum := sum*x + rowstmp[j];
  });
        sum;
};

//HoldArgumentNumber(SumTaylorNum, 4, 2);
//HoldArgumentNumber(SumTaylorNum, 4, 3);

/*
Examples:

In> SumTaylorNum(1,[[k], 1/k!],[[k], 1/k], 10 )
Result: 2.7182818006;

In> SumTaylorNum(1,[[k],1/k!], 10 )
Result: 2.7182818007;
*/

%/mathpiper



%mathpiper_docs,name="SumTaylorNum",categories="Mathematics Functions;Series"
*CMD SumTaylorNum --- optimized numerical evaluation of Taylor series
*STD
*CALL
        SumTaylorNum(x, NthTerm, order)
        SumTaylorNum(x, NthTerm, TermFactor, order)
        SumTaylorNum(x, ZerothTerm, TermFactor, order)

*PARMS

{NthTerm} -- a function specifying $n$-th coefficient of the series

{ZerothTerm} -- value of the $0$-th coefficient of the series

{x} -- number, value of the expansion variable

{TermFactor} -- a function specifying the ratio of $n$-th term to the previous one

{order} -- power of $x$ in the last term

*DESC

[SumTaylorNum] computes a Taylor series $Sum(k,0,n,a[k]*x^k)$
numerically. This function allows very efficient computations of
functions given by Taylor series, although some tweaking of the
parameters is required for good results.

The coefficients $a[k]$ of the Taylor series are given as functions of one integer variable ($k$). It is convenient to pass them to {SumTaylorNum} as closures.
For example, if a function {a(k)} is defined, then
        SumTaylorNum(x, [[k], a(k)], n)
computes the series $Sum(k, 0, n, a(k)*x^k)$.

Often a simple relation between successive coefficients $a[k-1]$,
$a[k]$ of the series is available; usually they are related by a
rational factor. In this case, the second form of {SumTaylorNum} should
be used because it will compute the series faster. The procedure
{TermFactor} applied to an integer $k>=1$ must return the ratio
$a[k]$/$a[k-1]$. (If possible, the function [TermFactor] should return
a rational number and not a floating-point number.) The procedure
{NthTerm} may also be given, but the current implementation only calls
{NthTerm(0)} and obtains all other coefficients by using {TermFactor}.
Instead of the function {NthTerm}, a number giving the $0$-th term can be given.

The algorithm is described Elsewhere in the documentation.
The number of terms {order}+1
must be specified and a sufficiently high precision must be preset in
advance to achieve the desired accuracy.
(The procedure {SumTaylorNum} does not change the current precision.)

*E.G.
To compute 20 digits of $Exp(1)$ using the Taylor series, one needs 21
digits of working precision and 21 terms of the series. 

In> BuiltinPrecisionSet(21)
Result: True;

In> SumTaylorNum(1, [[k],1/k!], 21)
Result: 2.718281828459045235351;

In> SumTaylorNum(1, 1, [[k],1/k], 21)
Result: 2.71828182845904523535;

In> SumTaylorNum(1, [[k],1/k!], [[k],1/k], 21)
Result: 2.71828182845904523535;

In> RoundTo(NM(Ln(%)),20)
Result: 1;


*SEE Taylor
%/mathpiper_docs





%mathpiper,name="SumTaylorNum",subtype="automatic_test"

BuiltinPrecisionSet(22);
NumericEqual(RoundTo(SumTaylorNum(1, [[k],1/k!], [[k],1/k], 21),21), 2.718281828459045235359,21);
NumericEqual(RoundTo(SumTaylorNum(1, [[k],1/k!], 21),21), 2.718281828459045235359,21);

%/mathpiper