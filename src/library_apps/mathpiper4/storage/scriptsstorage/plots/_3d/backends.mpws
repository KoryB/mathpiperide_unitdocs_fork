%mathpiper,def="Plot3DSoutputs;Plot3DSdata"

//////////////////////////////////////////////////
/// Backends for 3D plotting
//////////////////////////////////////////////////

/// List of all defined backends and their symbolic labels.
/// Add any new backends here
Plot3DSoutputs() := [
        ["default", "data"],
        ["data", "Plot3DSdata"],
];

/*
        How backends work:
        Plot3DS<backend>(values, optionshash)
        optionshash is a hash that contains all plotting options:
        ["xrange"] - a list of [x1, x2], ["xname"] - name of the variable to plot, same for "yrange";
        ["zname"] - array of string representations of the function(s), and perhaps other options relevant to the particular backend.
        [values] is a list of lists of triples of the form [[[x1, y1, z1], [x2, y2, z2], ...], [[x1, y1, t1], [x2, y2, t2], ...], ...] corresponding to the functions z(x,y), t(x,y), ... to be plotted. The points x[i], y[i] are not necessarily the same for all functions.
        The backend should prepare the graph of the function(s). The "datafile" backend Plot3DSdatafile(values, optionshash) may be used to output all data to file(s), in which case the file name should be given by the value optionshash["filename"]. Multiple files are created with names obtained by appending numbers to the filename.
        Note that the "data" backend does not do anything and simply returns the data.
        The backend Plot3DSdatafile takes care not to write "Infinity" or "Undefined" data points (it just ignores them). Custom backends should either use Plot3DSdatafile to prepare a file, or take care of this themselves.
*/

/// trivial backend: return data list (do not confuse with Plot3DSgetdata() defined in the main code which is the middle-level plotting routine)
Plot3DSdata(values_List?, _optionshash) <-- values;



%/mathpiper