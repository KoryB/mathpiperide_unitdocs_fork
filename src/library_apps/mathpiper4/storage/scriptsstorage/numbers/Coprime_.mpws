%mathpiper,def="Coprime?"

5  # Coprime?(list_List?)                     <-- (Lcm(list) =? Product(list));
10 # Coprime?(n_Integer?,m_Integer?)        <-- (Gcd(n,m) =? 1);

%/mathpiper



%mathpiper_docs,name="Coprime?",categories="Programming Functions;Predicates"
*CMD Coprime? --- test if integers are coprime 
*STD
*CALL
        Coprime?(m,n)
        Coprime?(list)
*PARMS

{m},{n} -- positive integers

{list}  -- list of positive integers

*DESC

This function returns {True} if the given pair or list of integers are coprime,
also called relatively prime. A pair or list of numbers are coprime if they 
share no common factors.

*E.G.

In> Coprime?([3,4,5,8])
Result: False;

In> Coprime?(15,17)
Result: True;

*SEE Prime
%/mathpiper_docs





%mathpiper,name="Coprime?",subtype="automatic_test"

Verify( Coprime?(11,13), True );
Verify( Coprime?(1 .. 10), False );
Verify( Coprime?([9,40]), True );

%/mathpiper