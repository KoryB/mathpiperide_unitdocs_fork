%mathpiper,def="^"

/* Faster version of raising power to 0.5 */
50 # _x ^ (1/2) <-- Sqrt(x);
50 # (x_PositiveNumber? ^ (1/2))_Integer?(SqrtN(x)) <-- SqrtN(x);
58 # 1 ^ n_Infinity? <-- Undefined;
59 # _x ^ 1 <-- x;
59 # 1 ^ _n <-- 1;
59 # x_Zero? ^ y_Zero? <-- Undefined;
60 # (x_Zero? ^ n_RationalOrNumber?)_(n>?0) <-- 0;
60 # (x_Zero? ^ n_RationalOrNumber?)_(n<?0) <-- Infinity;
// This is to fix:
// In> 0.0000^2
// Result: 0.0000^2;
// In> 0.0^2/2
// Result: 0.0^2/2;
//60 # (x_Number? ^ n_RationalOrNumber?)_(x+1=1) <-- 0;

59 # _x ^ Undefined <-- Undefined;
59 # Undefined ^ _x <-- Undefined;

/* Regular raising to the power. */
61 # Infinity ^ (y_NegativeNumber?) <-- 0;
61 # (-Infinity) ^ (y_NegativeNumber?) <-- 0;
//61 # x_PositiveNumber? ^ y_PositiveNumber? <-- PowerN(x,y);
//61 # x_PositiveNumber? ^ y_NegativeNumber? <-- (1/PowerN(x,-y));
// integer powers are very fast
61 # x_PositiveNumber? ^ y_PositiveInteger? <-- MathIntPower(x,y);
61 # x_PositiveNumber? ^ y_NegativeInteger? <-- 1/MathIntPower(x,-y);
65 # (x_PositiveNumber? ^ y_Number?)_NumericMode?() <-- Exp(y*Ln(x));

90 # (-_x)^m_Even? <-- x^m;
91 # (x_Constant? ^ (m_Odd? / p_Odd?))_(NegativeNumber?(Re(NM(Eval(x))))) <--
     -((-x)^(m/p));
92 # (x_NegativeNumber? ^ y_Number?)_NumericMode?() <-- Exp(y*Ln(x));


70  # (_x ^ m_RationalOrNumber?) ^ n_RationalOrNumber? <-- x^(n*m);

80 # (x_Number?/y_Number?) ^ n_PositiveInteger? <-- x^n/y^n;
80 # (x_Number?/y_Number?) ^ n_NegativeInteger? <-- y^(-n)/x^(-n);
80 # x_NegativeNumber? ^ n_Even? <-- (-x)^n;
80 # x_NegativeNumber? ^ n_Odd? <-- -((-x)^n);


100  # ((x_Number?)^(n_Integer?/(_m)))_(n>?1) <-- MathIntPower(x,n)^(1/m);

100 # Sqrt(_n)^(m_Even?) <-- n^(m/2);

100 # Abs(_a)^n_Even? <-- a^n;
100 # Abs(_a)^n_Odd? <-- Sign(a)*a^n;


200 # x_Matrix? ^ n_PositiveInteger? <-- x*(x^(n-1));
204 # (xlist_List? ^ nlist_List?)_(Length(xlist)=?Length(nlist)) <--
         Map("^",[xlist,nlist]);
205 # (xlist_List? ^ n_Constant?)_(Not?(List?(n))) <--
         Map([[xx],xx^n],[xlist]);
206 # (_x ^ n_List?)_(Not?(List?(x))) <-- Map([[xx],x^xx],[n]);
249 # x_Infinity? ^ 0 <-- Undefined;
250 # Infinity ^ (_n) <-- Infinity;
250 # Infinity ^ (_x_Complex?) <-- Infinity;
250 # ((-Infinity) ^ (n_Number?))_(Even?(n)) <-- Infinity;
250 # ((-Infinity) ^ (n_Number?))_(Odd?(n)) <-- -Infinity;

250 # (x_Number? ^ Infinity)_(x>? -1 And? x <? 1) <-- 0;
250 # (x_Number? ^ Infinity)_(x>? 1) <-- Infinity;

// these Magnitude(x)s should probably be changed to Abs(x)s

250 # (x_Complex? ^ Infinity)_(Magnitude(x) >? 1) <-- Infinity;
250 # (x_Complex? ^ Infinity)_(Magnitude(x) <? -1) <-- -Infinity;
250 # (x_Complex? ^ Infinity)_(Magnitude(x) >? -1 And? Magnitude(x) <? 1) <-- 0;

250 # (x_Number? ^ -Infinity)_(x>? -1 And? x <? 1) <-- Infinity;
250 # (x_Number? ^ -Infinity)_(x<? -1) <-- 0;
250 # (x_Number? ^ -Infinity)_(x>? 1) <-- 0;

255 # (x_Complex? ^ Infinity)_(Abs(x) =? 1) <-- Undefined;
255 # (x_Complex? ^ -Infinity)_(Abs(x) =? 1) <-- Undefined;



400 # _x ^ 0 <-- 1;

%/mathpiper


%mathpiper_docs,name="^",categories="Operators"
*CMD ^ --- arithmetic power
*STD
*CALL

        x^y
Precedence:
*EVAL PrecedenceGet("^")

*PARMS

{x} and {y} -- objects for which arithmetic operations are defined

*DESC

These are the basic arithmetic operations. They can work on integers,
rational numbers, complex numbers, vectors, matrices and lists.

These operators are implemented in the standard math library (as opposed
to being built-in). This means that they can be extended by the user.

*E.G.

In> 2^3
Result: 8;
%/mathpiper_docs





%mathpiper,name="^",subtype="automatic_test"

Verify(1^Infinity,Undefined);

// Matrix operations failed: a^2 performed the squaring on each element.
Verify([[1,2],[3,4]]^2,[[7,10],[15,22]]);

// Check that raising powers still works on lists/vectors (dotproduct?) correctly.
Verify([2,3]^2,[4,9]);

Verify(0.0000^(24),0);

// expansion of negative powers of fractions
Verify( (-1/2)^(-10), 1024);

Verify( I^(Infinity), Undefined );
Verify( I^(-Infinity), Undefined );

Verify( 2^(-10), 1/1024 );

%/mathpiper